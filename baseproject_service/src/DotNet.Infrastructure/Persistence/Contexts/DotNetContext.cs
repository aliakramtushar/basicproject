﻿using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.Entities;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.CDA;
using System;

namespace DotNet.Infrastructure.Persistence.Contexts
{
    public class DotNetContext : DbContext
    {
        public DotNetContext(DbContextOptions<DotNetContext> options) : base(options)
        {
            //Database. SetInitializer<DotNetContext>(null);
        }

        public DbSet<Users> Users { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<GlobalSetting> GlobalSettings { get; set; }
        public DbSet<NotificationArea> NotificationAreas { get; set; }
        public DbSet<SMSNotification> SMSNotifications { get; set; }
        public DbSet<PermissionUserRoleMap> PermissionUserRoleMaps { get; set; }
        public DbSet<UserDPZMap> UserDPZMaps { get; set; }
        public DbSet<Country> Countrys { get; set; }
        public DbSet<CountryDivisionMap> CountryDivisionMaps { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<DivisionDistrictMap> DivisionDistrictMaps { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<DistrictUpazilaCityCorporationMap> DistrictUpazilaCityCorporationMaps { get; set; }
        public DbSet<UpazilaCityCorporation> UpazilaCityCorporations { get; set; }
        public DbSet<UpazilaCityCorporationThanaMap> UpazilaCityCorporationThanaMaps { get; set; }
        public DbSet<Thana> Thanas { get; set; }
        public DbSet<Mouza> Mouzas { get; set; }
        public DbSet<DPZ> DPZs { get; set; }
        public DbSet<ThanaUnionWardMap> ThanaUnionWardMaps { get; set; }
        public DbSet<UnionWard> UnionWards { get; set; }
        public DbSet<UnionWardVillageAreaMap> UnionWardVillageAreaMaps { get; set; }
        public DbSet<VillageArea> VillageAreas { get; set; }
        public DbSet<VillageAreaParaMap> VillageAreaParaMaps { get; set; }
        public DbSet<Para> Paras { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<OrganizationCountryMap> OrganizationCountryMaps { get; set; }
        public DbSet<OrganizationDivisionMap> OrganizationDivisionMaps { get; set; }
        public DbSet<OrganizationDistrictMap> OrganizationDistrictMaps { get; set; }
        public DbSet<OrganizationUpazilaCityCorporationMap> OrganizationUpazilaCityCorporationMaps { get; set; }
        public DbSet<OrganizationThanaMap> OrganizationThanaMaps { get; set; }
        public DbSet<OrganizationUnionWardMap> OrganizationUnionWardMaps { get; set; }
        public DbSet<OrganizationVillageAreaMap> OrganizationVillageAreaMaps { get; set; }
        public DbSet<OrganizationParaMap> OrganizationParaMaps { get; set; }
        public DbSet<Designation> Designations { get; set; }
<<<<<<< HEAD
        public DbSet<Attachments> Attachments { get; set; }


        #region CDA
        public DbSet<ApplicationMigration> ApplicationMigrations { get; set; }
        public DbSet<ApplicationFileMaster> ApplicationFileMasters { get; set; }
        public DbSet<ApplicationFileUserMap> ApplicationFileUserMaps { get; set; }
        public DbSet<InspectionDetailsBCCase> InspectionDetailsBCCases { get; set; }
        #endregion

=======
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
    }
}