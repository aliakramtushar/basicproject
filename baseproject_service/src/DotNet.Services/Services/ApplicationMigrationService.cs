﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.CDA;

namespace DotNet.Services.Services
{
    //GenericRepository<Users>,
    public class ApplicationMigrationService : IApplicationMigrationService
    {
        private readonly IApplicationMigrationRepository _ApplicationMigrationRepository;
        private readonly IThanaRepository _thanaRepository;
        private readonly IMouzaRepository _mouzaRepository;



        ResponseMessage rm = new ResponseMessage();
        public ApplicationMigrationService(
            IApplicationMigrationRepository ApplicationMigrationRepository,
            IThanaRepository thanaRepository,
            IMouzaRepository mouzaRepository
            )// : base(dotnetContext)
        {
            _ApplicationMigrationRepository = ApplicationMigrationRepository;
            _thanaRepository = thanaRepository;
            _mouzaRepository = mouzaRepository;
        }

        public async Task<IEnumerable<VMApplicationMigration>> GetAll(int type)
        {
            return await _ApplicationMigrationRepository.GetAll(type);
        }
        public async Task<ApplicationMigration> GetByID(int id)
        {
            var response = await _ApplicationMigrationRepository.GetByID(id);
            return response;
        }
        public async Task<ApplicationMigration> Add(ApplicationMigration ApplicationMigration)
        {
            var data = await _ApplicationMigrationRepository.Add(ApplicationMigration);
            return data;
        }
        public async Task<ApplicationMigration> Update(ApplicationMigration ApplicationMigration)
        {
            return await _ApplicationMigrationRepository.Update(ApplicationMigration);
        }
        public async Task<bool> Delete(int id)
        {
            var response = await _ApplicationMigrationRepository.Delete(id);
            return response;
        }
        public async Task<ResponseMessage> GetInitialData(int id)
        {
            try
            {
                var lstThana = await _thanaRepository.GetListByOrganizationID(id);
                var lstMouza = await _mouzaRepository.GetAll();

                var array = Enum.GetValues(typeof(ApplicationType)).Cast<ApplicationType>();
                var lstApplicationType = array.Select(a => new
                {
                    ApplicationTypeID = Convert.ToInt32(a),
                    ApplicationTypeName = a.ToString()
                }).ToList();

                rm.StatusCode = ReturnStatus.Success;
                rm.ResponseObj = new
                {
                    lstThana = lstThana,
                    lstMouza = lstMouza,
                    lstApplicationType = lstApplicationType
                };
            }
            catch (Exception ex)
            {
                rm.Message = ex.Message;
                rm.StatusCode = ReturnStatus.Failed;
            }
            return rm;
        }
        public async Task<ResponseMessage> ExcelUpload(VMAttachments attachment)
        {
            return await _ApplicationMigrationRepository.ExcelUpload(attachment);
        }

        
    }
}
