﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.CDA;

namespace DotNet.Services.Services
{
    //GenericRepository<Users>,
    public class ApplicationFileUserMapService : IApplicationFileUserMapService
    {
        private readonly IApplicationFileUserMapRepository _ApplicationFileUserMapRepository;


        ResponseMessage rm = new ResponseMessage();
        public ApplicationFileUserMapService(
            IApplicationFileUserMapRepository ApplicationFileUserMapRepository
            )// : base(dotnetContext)
        {
            _ApplicationFileUserMapRepository = ApplicationFileUserMapRepository;
        }

        public async Task<IEnumerable<VMApplicationFileUserMap>> GetAll()
        {
            return await _ApplicationFileUserMapRepository.GetAll();
        }
        public async Task<ApplicationFileUserMap> GetByID(int id)
        {
            var response = await _ApplicationFileUserMapRepository.GetByID(id);
            return response;
        }
        public async Task<ApplicationFileUserMap> Add(ApplicationFileUserMap ApplicationFileUserMap)
        {
            var data = await _ApplicationFileUserMapRepository.Add(ApplicationFileUserMap);
            return data;
        }
        public async Task<ApplicationFileUserMap> Update(ApplicationFileUserMap ApplicationFileUserMap)
        {
            return await _ApplicationFileUserMapRepository.Update(ApplicationFileUserMap);
        }
        public async Task<bool> Delete(int id)
        {
            var response = await _ApplicationFileUserMapRepository.Delete(id);
            return response;
        }
        public async Task<IEnumerable<VMApplicationFileUserMap>> GetAssingedUserListByMasterFileID(int fileID)
        {
            var response = await _ApplicationFileUserMapRepository.GetAssingedUserListByMasterFileID(fileID);
            return response;
        }
    }
}
