﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Infrastructure.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using DotNet.Services.Repositories.Common;
using DotNet.ApplicationCore.DTOs.VM;

namespace DotNet.Services.Services.Common
{
    //GenericRepository<Users>,
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ICommonRepository _commonRepository;
        private readonly INotificationAreaRepository _notificationAreaRepository;
        private readonly IOrganizationRepository _organizaionRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly IDesignationRepository _designationRepository;
        private readonly IDepartmentRepository _departmentRepository;


        ResponseMessage rm = new ResponseMessage();
        public UserService(
            IUserRepository userRepository,
            ICommonRepository commonRepository,
            INotificationAreaRepository notificationAreaRepository,
            IOrganizationRepository organizaionRepository,
            IUserRoleRepository userRoleRepository,
            IDesignationRepository designationRepository,
            IDepartmentRepository departmentRepository
            )// : base(dotnetContext)
        {
            _userRepository = userRepository;
            _commonRepository = commonRepository;
            _notificationAreaRepository = notificationAreaRepository;
            _organizaionRepository = organizaionRepository;
            _userRoleRepository = userRoleRepository;
            _designationRepository = designationRepository;
            _departmentRepository = departmentRepository;

        }

        public ResponseMessage UserAuthentication(AuthUser user)
        {
            try
            {
                AuthUser authUser = _userRepository.UserAuthentication(user);
                if (authUser.UserAutoID > 0)
                {
                    rm.StatusCode = ReturnStatus.Success;
                    rm.ResponseObj = authUser;
                }
                else
                {
                    rm.Message = "Invalid UserID or Password";
                    rm.StatusCode = ReturnStatus.Failed;
                }
            }
            catch (Exception ex)
            {
                rm.Message = ex.Message;
                rm.StatusCode = ReturnStatus.Failed;
            }
            return rm;
        }
        public async Task<IEnumerable<Users>> GetAll()
        {
            return await _userRepository.GetAll();
        }
        public async Task<Users> GetByID(int id)
        {
            var response = await _userRepository.GetByID(id);
            return response;
        }
        public async Task<Users> Add(Users user)
        {
            var data = await _userRepository.Add(user);
            return data;
        }
        public async Task<Users> Update(Users user)
        {
            var result = await _userRepository.Update(user);
            var notificationArea = await _notificationAreaRepository.GetByID((int)NotificationAreaEnum.UserLogin);
            await _commonRepository.SendSMS(user.UserAutoID, user.MobileNo, "your number changed", notificationArea, "");
            return result;
        }
        public async Task<bool> Delete(int id)
        {
            var response = await _userRepository.Delete(id);
            return response;
        }
        public async Task<IEnumerable<VMUsers>> GetAllByOrganizationID()
        {
            return await _userRepository.GetAllByOrganizationID();
        }
        public async Task<IEnumerable<VMUsers>> GetAllByDepartmentID(int id)
        {
            return await _userRepository.GetAllByDepartmentID(id);
        }
        public async Task<ResponseMessage> GetInitialData()
        {
            try
            {
                var lstOrganization = await _organizaionRepository.GetAll();
                var lstUserRole = await _userRoleRepository.GetAll();
                var lstDesignation = await _designationRepository.GetAll();
                var lstDepartment= await _departmentRepository.GetAll();


                rm.StatusCode = ReturnStatus.Success;
                rm.ResponseObj = new
                {
                    lstOrganization = lstOrganization,
                    lstUserRole = lstUserRole,
                    lstDesignation = lstDesignation.Where(x => x.IsActive == true).ToList(),
                    lstDepartment = lstDepartment.Where(x => x.IsActive == true).ToList(),
                };
            }
            catch (Exception ex)
            {
                rm.Message = ex.Message;
                rm.StatusCode = ReturnStatus.Failed;
            }
            return rm;
        }
    }
}
