﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;

namespace DotNet.Services.Services.Common
{
    //GenericRepository<Users>,
    public class DesignationService : IDesignationService
    {
        private readonly IDesignationRepository _DesignationRepository;

        ResponseMessage rm = new ResponseMessage();
        public DesignationService(
            IDesignationRepository DesignationRepository
            )// : base(dotnetContext)
        {
            _DesignationRepository = DesignationRepository;
        }

        public async Task<IEnumerable<Designation>> GetAll()
        {
            return await _DesignationRepository.GetAll();
        }
        public async Task<Designation> GetByID(int id)
        {
            var response = await _DesignationRepository.GetByID(id);
            return response;
        }
        public async Task<Designation> Add(Designation Designation)
        {
            var data = await _DesignationRepository.Add(Designation);
            return data;
        }
        public async Task<Designation> Update(Designation Designation)
        {
            return await _DesignationRepository.Update(Designation);
        }
        public async Task<bool> Delete(int id)
        {
            var response = await _DesignationRepository.Delete(id);
            return response;
        }
    }
}
