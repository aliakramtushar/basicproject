﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.Services.Services.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;

namespace DotNet.Services.Services.Common.AdministrativeUnit
{
    public class MouzaService : IMouzaService
    {
        private readonly IMouzaRepository _MouzaRepository;

        ResponseMessage rm = new ResponseMessage();
        public MouzaService(
            IMouzaRepository MouzaRepository
            )// : base(dotnetContext)
        {
            _MouzaRepository = MouzaRepository;
        }

        public async Task<IEnumerable<VMMouza>> GetAll()
        {
            return await _MouzaRepository.GetAll();
        }
        public async Task<VMMouza> GetByID(int id)
        {
            var response = await _MouzaRepository.GetByID(id);
            return response;
        }
        public async Task<VMMouza> Add(VMMouza Mouza)
        {
            var data = await _MouzaRepository.Add(Mouza);
            return data;
        }
        public async Task<VMMouza> Update(VMMouza Mouza)
        {
            return await _MouzaRepository.Update(Mouza);
        }
        public async Task<bool> Delete(int id)
        {
            var response = await _MouzaRepository.Delete(id);
            return response;
        }
        public async Task<IEnumerable<VMMouza>> Search(QueryObject queryObject)
        {
            var data = await _MouzaRepository.Search(queryObject);
            return data;
        }
    }
}
