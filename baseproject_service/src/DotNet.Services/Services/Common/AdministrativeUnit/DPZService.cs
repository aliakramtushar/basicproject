﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.Services.Services.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.DTOs.VM;

namespace DotNet.Services.Services.Common.AdministrativeUnit
{
    public class DPZService : IDPZService
    {
        private readonly IDPZRepository _DPZRepository;

        ResponseMessage rm = new ResponseMessage();
        public DPZService(
            IDPZRepository DPZRepository
            )// : base(dotnetContext)
        {
            _DPZRepository = DPZRepository;
        }

        public async Task<IEnumerable<VMDPZ>> GetAll()
        {
            return await _DPZRepository.GetAll();
        }
        public async Task<VMDPZ> GetByID(int id)
        {
            var response = await _DPZRepository.GetByID(id);
            return response;
        }
        public async Task<VMDPZ> Add(VMDPZ DPZ)
        {
            var data = await _DPZRepository.Add(DPZ);
            return data;
        }
        public async Task<VMDPZ> Update(VMDPZ DPZ)
        {
            return await _DPZRepository.Update(DPZ);
        }
        public async Task<bool> Delete(int id)
        {
            var response = await _DPZRepository.Delete(id);
            return response;
        }
        public async Task<IEnumerable<VMDPZ>> Search(QueryObject queryObject)
        {
            var data = await _DPZRepository.Search(queryObject);
            return data;
        }
        //public async Task<IEnumerable<VMUserDPZMap>> UserDpzMap(List<VMUserDPZMap> objList)
        //{
        //    var data = await _DPZRepository.UserDpzMap(objList);
        //    return data;
        //}
    }
}
