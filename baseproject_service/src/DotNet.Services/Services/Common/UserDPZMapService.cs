﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.Services.Services.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.Services.Repositories.Interfaces.Common;

namespace DotNet.Services.Services.Common
{
    public class UserDPZMapService : IUserDPZMapService
    {
        private readonly IUserDPZMapRepository _UserDPZMapRepository;

        ResponseMessage rm = new ResponseMessage();
        public UserDPZMapService(
            IUserDPZMapRepository UserDPZMapRepository
            )// : base(dotnetContext)
        {
            _UserDPZMapRepository = UserDPZMapRepository;
        }

        public async Task<IEnumerable<VMUserDPZMap>> GetAll()
        {
            return await _UserDPZMapRepository.GetAll();
        }
        public async Task<VMUserDPZMap> GetByID(int id)
        {
            var response = await _UserDPZMapRepository.GetByID(id);
            return response;
        }
        public async Task<VMUserDPZMap> Add(VMUserDPZMap UserDPZMap)
        {
            var data = await _UserDPZMapRepository.Add(UserDPZMap);
            return data;
        }
        public async Task<VMUserDPZMap> Update(VMUserDPZMap UserDPZMap)
        {
            return await _UserDPZMapRepository.Update(UserDPZMap);
        }
        public async Task<bool> Delete(int id)
        {
            var response = await _UserDPZMapRepository.Delete(id);
            return response;
        }
    }
}
