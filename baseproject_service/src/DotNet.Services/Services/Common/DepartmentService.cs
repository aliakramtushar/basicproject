﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.ApplicationCore.DTOs.Common;
using Newtonsoft.Json;

namespace DotNet.Services.Services.Common
{
    //GenericRepository<Users>,
    public class DepartmentService : IDepartmentService
    {
        private readonly IDepartmentRepository _DepartmentRepository;

        ResponseMessage rm = new ResponseMessage();
        public DepartmentService(
            IDepartmentRepository DepartmentRepository
            )// : base(dotnetContext)
        {
            _DepartmentRepository = DepartmentRepository;
        }

        public async Task<IEnumerable<Department>> GetAll()
        {
            return await _DepartmentRepository.GetAll();
        }
        public async Task<Department> GetByID(int id)
        {
            var response = await _DepartmentRepository.GetByID(id);
            return response;
        }
        public async Task<Department> Add(Department Department)
        {
            var data = await _DepartmentRepository.Add(Department);
            return data;
        }
        public async Task<Department> Update(Department Department)
        {
            return await _DepartmentRepository.Update(Department);
        }
        public async Task<bool> Delete(int id)
        {
            var response = await _DepartmentRepository.Delete(id);
            return response;
        }
        public List<DepartmentDTO> MakeListWithChild(List<Department> oList)
        {
            List<DepartmentDTO> lstDepartmentDTO = new List<DepartmentDTO>();
            DepartmentDTO DepartmentDTO = new DepartmentDTO();

            if (oList != null && oList.Count > 0)
            {
                foreach(Department Department in oList)
                {
                    var data = Newtonsoft.Json.JsonConvert.SerializeObject(Department);
                    DepartmentDTO = JsonConvert.DeserializeObject<DepartmentDTO>(data);
                    bool hasChild = oList.Any(x => x.ParentDepartmentID == Department.DepartmentID);
                    if (hasChild)
                    {
                        DepartmentDTO.HasChild= true;
                    }
                    else
                    {
                        DepartmentDTO.HasChild = false;
                    }
                    lstDepartmentDTO.Add(DepartmentDTO);
                }
            }
            return lstDepartmentDTO;
        }
        public async Task<IEnumerable<Department>> UpdateOrder(List<Department> oList)
        {
            return await _DepartmentRepository.UpdateOrder(oList);
        }
<<<<<<< HEAD
        public async Task<IEnumerable<Department>> GetListByOrganizationID(int id)
        {
            var response = await _DepartmentRepository.GetListByOrganizationID(id);
            return response;
        }
=======
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
    }
}
