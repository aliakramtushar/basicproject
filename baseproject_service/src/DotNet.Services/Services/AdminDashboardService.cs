﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;

namespace DotNet.Services.Services
{
    //GenericRepository<Users>,
    public class AdminDashboardService : IAdminDashboardService
    {
        private readonly IAdminDashboardRepository _AdminDashboardRepository;
        private readonly IInspectionDetailsBCCaseRepository _InspectionDetailsBCCaseRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMouzaRepository _mouzaRepository;
        private readonly IThanaRepository _thanaRepository;


        ResponseMessage rm = new ResponseMessage();
        public AdminDashboardService(
            IAdminDashboardRepository AdminDashboardRepository,
            IInspectionDetailsBCCaseRepository InspectionDetailsBCCaseRepository,
            IUserRepository userRepository,
            IMouzaRepository mouzaRepository,
            IThanaRepository thanaRepository

            )// : base(dotnetContext)
        {
            _AdminDashboardRepository = AdminDashboardRepository;
            _thanaRepository = thanaRepository;
            _InspectionDetailsBCCaseRepository = InspectionDetailsBCCaseRepository;
            _userRepository = userRepository;
            _mouzaRepository = mouzaRepository;
            _thanaRepository = thanaRepository;
        }

        public async Task<VMAdminDashboard> GetAdminDashboard()
        {
            var response = await _AdminDashboardRepository.GetAdminDashboard();
            return response;
        }
        public async Task<List<VMAdminDashboardFileUserWise>> GetFileListByFileType(int type)
        {
            var response = await _AdminDashboardRepository.GetFileListByFileType(type);
            return response;
        }
        public async Task<List<VMAdminDashboardFileListUserWise>> GetFileListByTypeVisitedUser(QueryObject queryObject)
        {
            var response = await _AdminDashboardRepository.GetFileListByTypeVisitedUser(queryObject);
            return response;
        }
    }
}
