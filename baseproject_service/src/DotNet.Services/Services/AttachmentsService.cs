﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;

namespace DotNet.Services.Services
{
    //GenericRepository<Users>,
    public class AttachmentsService : IAttachmentsService
    {
        private readonly IAttachmentsRepository _AttachmentsRepository;
        private readonly IInspectionDetailsBCCaseRepository _InspectionDetailsBCCaseRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMouzaRepository _mouzaRepository;
        private readonly IThanaRepository _thanaRepository;
        private readonly IApplicationFileMasterRepository _applicationFileMasterRepository;


        ResponseMessage rm = new ResponseMessage();
        public AttachmentsService(
            IAttachmentsRepository AttachmentsRepository,
            IInspectionDetailsBCCaseRepository InspectionDetailsBCCaseRepository,
            IUserRepository userRepository,
            IMouzaRepository mouzaRepository,
            IThanaRepository thanaRepository,
            IApplicationFileMasterRepository applicationFileMasterRepository

            )// : base(dotnetContext)
        {
            _AttachmentsRepository = AttachmentsRepository;
            _thanaRepository = thanaRepository;
            _InspectionDetailsBCCaseRepository = InspectionDetailsBCCaseRepository;
            _userRepository = userRepository;
            _mouzaRepository = mouzaRepository;
            _thanaRepository = thanaRepository;
            _applicationFileMasterRepository = applicationFileMasterRepository;
        }


        public async Task<ResponseMessage> GetAttachmentListByFileID(int id)
        {
            var response = await _AttachmentsRepository.GetAttachmentListByFileID(id);
            return response;
        }
    }
}
