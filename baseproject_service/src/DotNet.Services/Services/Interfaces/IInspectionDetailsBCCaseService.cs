﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Services.Infrastructure;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Services.Services.Interfaces
{
    public interface IInspectionDetailsBCCaseService 
    {
        Task<IEnumerable<VMInspectionDetailsBCCase>> GetAll();
        Task<InspectionDetailsBCCase> GetByID(int id);
        Task<ResponseMessage> Add(InspectionDetailsBCCase entity);
        Task<ResponseMessage> Update(InspectionDetailsBCCase entity);
        Task<bool> Delete(int id);    
    }
}
