﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Services.Infrastructure;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Services.Services.Interfaces
{
    public interface IApplicationMigrationService 
    {
        Task<IEnumerable<VMApplicationMigration>> GetAll(int type);
        Task<ApplicationMigration> GetByID(int id);
        Task<ApplicationMigration> Add(ApplicationMigration entity);
        Task<ApplicationMigration> Update(ApplicationMigration entity);
        Task<bool> Delete(int id);
        Task<ResponseMessage> GetInitialData(int id);
        Task<ResponseMessage> ExcelUpload(VMAttachments attachment);
    }
}
