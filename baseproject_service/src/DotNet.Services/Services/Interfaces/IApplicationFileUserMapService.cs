﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Services.Infrastructure;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Services.Services.Interfaces
{
    public interface IApplicationFileUserMapService 
    {
        Task<IEnumerable<VMApplicationFileUserMap>> GetAll();
        Task<ApplicationFileUserMap> GetByID(int id);
        Task<ApplicationFileUserMap> Add(ApplicationFileUserMap entity);
        Task<ApplicationFileUserMap> Update(ApplicationFileUserMap entity);
        Task<bool> Delete(int id);
        Task<IEnumerable<VMApplicationFileUserMap>> GetAssingedUserListByMasterFileID(int fileID);

    }
}
