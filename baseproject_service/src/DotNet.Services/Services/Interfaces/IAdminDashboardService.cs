﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Services.Infrastructure;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Services.Services.Interfaces
{
    public interface IAdminDashboardService
    {
        Task<VMAdminDashboard> GetAdminDashboard();
        Task<List<VMAdminDashboardFileUserWise>> GetFileListByFileType(int type);
        Task<List<VMAdminDashboardFileListUserWise>> GetFileListByTypeVisitedUser(QueryObject queryObject);
    }
}
