﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;

namespace DotNet.Services.Services.Interfaces.Common
{
    public interface IInspectionMonitoringService //: ICommonInterface<UserRole>
    {
        Task<IEnumerable<VMApplicationFileMaster>> Search(QueryObject queryObject);
        Task<ResponseMessage> GetInitialData();
    }
}
