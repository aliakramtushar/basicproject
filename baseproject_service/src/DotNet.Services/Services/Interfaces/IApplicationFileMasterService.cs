﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Services.Infrastructure;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;

namespace DotNet.Services.Services.Interfaces
{
    public interface IApplicationFileMasterService 
    {
        Task<IEnumerable<VMApplicationFileMaster>> GetAll();
        Task<VMApplicationFileMaster> GetByID(int id);
        Task<ApplicationFileMaster> Add(ApplicationFileMaster entity);
        Task<ApplicationFileMaster> Update(ApplicationFileMaster entity);
        Task<bool> Delete(int id);    
        Task<ResponseMessage> GetInitialData(int id);
        Task<ResponseMessage> MigrateData(List<ApplicationMigration> lstData);
        Task<ResponseMessage> GetListByAssingedPerson(QueryObject oQueryObject);
        Task<ResponseMessage> GetFileByIDWithInspectionDetails(int id);
        Task<ResponseMessage> UpdateApplicationFileGeofence(QueryObject entity);
        Task<ResponseMessage> SaveApplicationFileAttachments(IFormCollection request, int applicationFileMasterID, int attachmentType);


    }
}
