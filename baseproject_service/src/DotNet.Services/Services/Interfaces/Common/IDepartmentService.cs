﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Services.Services.Interfaces.Common
{
    public interface IDepartmentService //: ICommonInterface<Department>
    {
        Task<IEnumerable<Department>> GetAll();
        Task<Department> GetByID(int id);
        Task<Department> Add(Department entity);
        Task<Department> Update(Department entity);
        Task<bool> Delete(int id);
        List<DepartmentDTO> MakeListWithChild(List<Department> oList);
        Task<IEnumerable<Department>> UpdateOrder(List<Department> oList);
<<<<<<< HEAD
        Task<IEnumerable<Department>> GetListByOrganizationID(int id);
=======
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
    }
}
