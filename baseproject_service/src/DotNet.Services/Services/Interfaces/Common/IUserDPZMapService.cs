﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Services.Services.Interfaces.Common
{
    public interface IUserDPZMapService //: ICommonInterface<UserDPZMap>
    {
        Task<IEnumerable<VMUserDPZMap>> GetAll();
        Task<VMUserDPZMap> GetByID(int id);
        Task<VMUserDPZMap> Add(VMUserDPZMap entity);
        Task<VMUserDPZMap> Update(VMUserDPZMap entity);
        Task<bool> Delete(int id);
    }
}
