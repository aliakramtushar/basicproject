﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Services.Services.Interfaces.Common.AdministrativeUnit
{
    public interface IMouzaService //: ICommonInterface<Mouza>
    {
        Task<IEnumerable<VMMouza>> GetAll();
        Task<VMMouza> GetByID(int id);
        Task<VMMouza> Add(VMMouza entity);
        Task<VMMouza> Update(VMMouza entity);
        Task<bool> Delete(int id);
        Task<IEnumerable<VMMouza>> Search(QueryObject queryObject);
    }
}
