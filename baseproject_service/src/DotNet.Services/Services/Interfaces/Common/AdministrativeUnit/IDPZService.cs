﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Services.Services.Interfaces.Common.AdministrativeUnit
{
    public interface IDPZService //: ICommonInterface<DPZ>
    {
        Task<IEnumerable<VMDPZ>> GetAll();
        Task<VMDPZ> GetByID(int id);
        Task<VMDPZ> Add(VMDPZ entity);
        Task<VMDPZ> Update(VMDPZ entity);
        Task<bool> Delete(int id);
        Task<IEnumerable<VMDPZ>> Search(QueryObject queryObject);
        //Task<IEnumerable<VMUserDPZMap>> UserDpzMap(List<VMUserDPZMap> objList);

    }
}
