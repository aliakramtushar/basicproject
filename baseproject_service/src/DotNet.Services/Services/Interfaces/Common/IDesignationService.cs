﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.Entities;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNet.Services.Services.Interfaces.Common
{
    public interface IDesignationService 
    {
        Task<IEnumerable<Designation>> GetAll();
        Task<Designation> GetByID(int id);
        Task<Designation> Add(Designation entity);
        Task<Designation> Update(Designation entity);
        Task<bool> Delete(int id);
    }
}
