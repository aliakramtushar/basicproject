﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;

namespace DotNet.Services.Services
{
    //GenericRepository<Users>,
    public class ApplicationFileMasterService : IApplicationFileMasterService
    {
        private readonly IApplicationFileMasterRepository _ApplicationFileMasterRepository;
        private readonly IInspectionDetailsBCCaseRepository _InspectionDetailsBCCaseRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMouzaRepository _mouzaRepository;
        private readonly IThanaRepository _thanaRepository;


        ResponseMessage rm = new ResponseMessage();
        public ApplicationFileMasterService(
            IApplicationFileMasterRepository ApplicationFileMasterRepository,
            IInspectionDetailsBCCaseRepository InspectionDetailsBCCaseRepository,
            IUserRepository userRepository,
            IMouzaRepository mouzaRepository,
            IThanaRepository thanaRepository

            )// : base(dotnetContext)
        {
            _ApplicationFileMasterRepository = ApplicationFileMasterRepository;
            _thanaRepository = thanaRepository;
            _InspectionDetailsBCCaseRepository = InspectionDetailsBCCaseRepository;
            _userRepository = userRepository;
            _mouzaRepository = mouzaRepository;
            _thanaRepository = thanaRepository;
        }

        public async Task<IEnumerable<VMApplicationFileMaster>> GetAll()
        {
            return await _ApplicationFileMasterRepository.GetAll();
        }
        public async Task<VMApplicationFileMaster> GetByID(int id)
        {
            var response = await _ApplicationFileMasterRepository.GetByID(id);
            return response;
        }
        public async Task<ApplicationFileMaster> Add(ApplicationFileMaster ApplicationFileMaster)
        {
            var data = await _ApplicationFileMasterRepository.Add(ApplicationFileMaster);
            return data;
        }
        public async Task<ApplicationFileMaster> Update(ApplicationFileMaster ApplicationFileMaster)
        {
            return await _ApplicationFileMasterRepository.Update(ApplicationFileMaster);
        }
        public async Task<bool> Delete(int id)
        {
            var response = await _ApplicationFileMasterRepository.Delete(id);
            return response;
        }
        public async Task<ResponseMessage> GetInitialData(int id)
        {
            try
            {
                var lstThana = await _thanaRepository.GetListByOrganizationID(id);
                var lstInspectionDetailsBCCase = await _InspectionDetailsBCCaseRepository.GetAll();
                var lstUser = await _userRepository.GetAllByLoginUserDepartmentID();
                var lstMouza = await _mouzaRepository.GetAll();

                var array = Enum.GetValues(typeof(ApplicationType)).Cast<ApplicationType>();
                var lstApplicationType = array.Select(a => new
                {
                    ApplicationTypeID = Convert.ToInt32(a),
                    ApplicationTypeName = a.ToString()
                }).ToList();

                rm.StatusCode = ReturnStatus.Success;
                rm.ResponseObj = new
                {
                    lstThana = lstThana,
                    lstInspectionDetailsBCCase = lstInspectionDetailsBCCase,
                    lstUser = lstUser,
                    lstMouza = lstMouza,
                    lstApplicationType = lstApplicationType
                };
            }
            catch (Exception ex)
            {
                rm.Message = ex.Message;
                rm.StatusCode = ReturnStatus.Failed;
            }
            return rm;
        }
        public async Task<ResponseMessage> MigrateData(List<ApplicationMigration> lstData)
        {
            var response = await _ApplicationFileMasterRepository.MigrateData(lstData);
            return response;
        }
        public async Task<ResponseMessage> GetListByAssingedPerson(QueryObject oQueryObject)
        {
            var response = await _ApplicationFileMasterRepository.GetListByAssingedPerson(oQueryObject);
            return response;
        }
        public async Task<ResponseMessage> GetFileByIDWithInspectionDetails(int id)
        {
            VMApplicationFileMaster oApplicationFileMaster = new VMApplicationFileMaster();
            InspectionDetailsBCCase oInspectionDetailsBCCase = new InspectionDetailsBCCase();
            try
            {
                oApplicationFileMaster = await GetByID(id);
                if (oApplicationFileMaster != null && oApplicationFileMaster.ApplicationFileMasterID > 0)
                {
                    //if (oApplicationFileMaster.ApplicationType == ApplicationType.BCCase)
                    //{
                    oInspectionDetailsBCCase = await _InspectionDetailsBCCaseRepository.GetByFileMasterID(oApplicationFileMaster.ApplicationFileMasterID);
                    //}
                }

                rm.StatusCode = ReturnStatus.Success;
                rm.ResponseObj = new
                {
                    ApplicationFileMaster = oApplicationFileMaster,
                    details = oInspectionDetailsBCCase,
                };
            }
            catch (Exception ex)
            {
                rm.Message = ex.Message;
                rm.StatusCode = ReturnStatus.Failed;
            }
            return rm;
        }
        public async Task<ResponseMessage> UpdateApplicationFileGeofence(QueryObject oQueryObject)
        {
            var response = await _ApplicationFileMasterRepository.UpdateApplicationFileGeofence(oQueryObject);
            return response;
        }
        public async Task<ResponseMessage> SaveApplicationFileAttachments(IFormCollection request, int applicationFileMasterID, int attachmentType)
        {
            var response = await _ApplicationFileMasterRepository.SaveApplicationFileAttachments(request, applicationFileMasterID, attachmentType);
            return response;
        }


    }
}
