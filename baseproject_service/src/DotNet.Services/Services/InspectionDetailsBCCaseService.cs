﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.CDA;

namespace DotNet.Services.Services
{
    //GenericRepository<Users>,
    public class InspectionDetailsBCCaseService : IInspectionDetailsBCCaseService
    {
        private readonly IInspectionDetailsBCCaseRepository _InspectionDetailsBCCaseRepository;
        private readonly IThanaRepository _thanaRepository;
        private readonly IMouzaRepository _mouzaRepository;
        private readonly IUserRepository _userRepository;


        ResponseMessage rm = new ResponseMessage();
        public InspectionDetailsBCCaseService(
            IInspectionDetailsBCCaseRepository InspectionDetailsBCCaseRepository,
            IThanaRepository thanaRepository,
            IMouzaRepository mouzaRepository,
            IUserRepository userRepository
            )// : base(dotnetContext)
        {
            _InspectionDetailsBCCaseRepository = InspectionDetailsBCCaseRepository;
            _thanaRepository = thanaRepository;
            _mouzaRepository = mouzaRepository;
            _userRepository = userRepository;
        }

        public async Task<IEnumerable<VMInspectionDetailsBCCase>> GetAll()
        {
            return await _InspectionDetailsBCCaseRepository.GetAll();
        }
        public async Task<InspectionDetailsBCCase> GetByID(int id)
        {
            var response = await _InspectionDetailsBCCaseRepository.GetByID(id);
            return response;
        }
        public async Task<ResponseMessage> Add(InspectionDetailsBCCase InspectionDetailsBCCase)
        {
            var data = await _InspectionDetailsBCCaseRepository.Add(InspectionDetailsBCCase);
            return data;
        }
        public async Task<ResponseMessage> Update(InspectionDetailsBCCase InspectionDetailsBCCase)
        {
            return await _InspectionDetailsBCCaseRepository.Update(InspectionDetailsBCCase);
        }
        public async Task<bool> Delete(int id)
        {
            var response = await _InspectionDetailsBCCaseRepository.Delete(id);
            return response;
        }
    }
}
