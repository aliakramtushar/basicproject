﻿using DotNet.ApplicationCore.DTOs;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.Services.Repositories.Interfaces;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Common;
using DotNet.Infrastructure.Persistence.Contexts;
using DotNet.Services.Repositories.Infrastructure;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.ApplicationCore.DTOs.Common;

namespace DotNet.Services.Services.Common
{
    public class InspectionMonitoringService : IInspectionMonitoringService
    {
        private readonly IInspectionMonitoringRepository _inspectionMonitoringRepository;
        private readonly IUserRepository _userRepository;
        private readonly IOrganizationRepository _organizationRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IDPZRepository _dpzRepository;
        private readonly IThanaRepository _thanaRepository;
        private readonly IMouzaRepository _mouzaRepository;

        ResponseMessage rm = new ResponseMessage();
        public InspectionMonitoringService(
            IInspectionMonitoringRepository inspectionMonitoringRepository,
            IUserRepository userRepository,
            IOrganizationRepository organizationRepository,
            IDepartmentRepository departmentRepository,
            IDPZRepository dPZRepository,
            IThanaRepository thanaRepository,
            IMouzaRepository mouzaRepository
            )// : base(dotnetContext)
        {
            _inspectionMonitoringRepository = inspectionMonitoringRepository;
            _userRepository = userRepository;
            _organizationRepository = organizationRepository;
            _departmentRepository = departmentRepository;
            _dpzRepository = dPZRepository;
            _thanaRepository = thanaRepository;
            _mouzaRepository = mouzaRepository;
        }

        public async Task<IEnumerable<VMApplicationFileMaster>> Search(QueryObject queryObject)
        {
            var data = await _inspectionMonitoringRepository.Search(queryObject);
            return data;
        }
        public async Task<ResponseMessage> GetInitialData()
        {
            try
            {
                var lstOrganizations = await _organizationRepository.GetAll();
                var lstDepartments = await _departmentRepository.GetAll();
                var lstDPZs = await _dpzRepository.GetAll();
                var lstThanas = await _thanaRepository.GetAll();
                var lstMouzas = await _mouzaRepository.GetAll();
                var lstUsers = await _userRepository.GetAll();

                var array = Enum.GetValues(typeof(ApplicationType)).Cast<ApplicationType>();
                var lstApplicationTypes = array.Select(a => new
                {
                    ApplicationTypeID = Convert.ToInt32(a),
                    ApplicationTypeName = a.ToString()
                }).ToList();


                rm.StatusCode = ReturnStatus.Success;
                rm.ResponseObj = new
                {
                    lstOrganizations = lstOrganizations,
                    lstDepartments = lstDepartments,
                    lstDPZs = lstDPZs,
                    lstThanas = lstThanas,
                    lstMouzas = lstMouzas,
                    lstUsers = lstUsers,
                    lstApplicationTypes = lstApplicationTypes
                };
            }
            catch (Exception ex)
            {
                rm.Message = ex.Message;
                rm.StatusCode = ReturnStatus.Failed;
            }
            return rm;
        }

    }
}
