﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using System.Net.Mail;
using OfficeOpenXml;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using System.Globalization;
using Microsoft.AspNetCore.Http.Internal;
using System.Drawing;
using DotNet.ApplicationCore.DTOs.VM;
using Microsoft.Extensions.Configuration;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.CDA;
using System.Xml.Linq;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using DotNet.Infrastructure.Persistence;
using System.Data.SqlClient;
using System.Data.Entity;
using System.ComponentModel;


namespace DotNet.Services.Repositories
{
    public class AdminDashboardRepository : IAdminDashboardRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;


        public AdminDashboardRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper,
            IConfiguration configuration
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
            _configuration = configuration;
        }
        public async Task<VMAdminDashboard> GetAdminDashboard()
        {
            //var result = _context.Countrys.FromSqlRaw("SELECT * FROM com.Countrys").ToList();
            VMAdminDashboard adminDashboard = new VMAdminDashboard();
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            int organizationID = await _httpContextAccessor.HttpContext.User.GetOrginzationIdFromClaimIdentity();

            var lstFileMaster = _context.ApplicationFileMasters.ToList();
            adminDashboard.AdminDashboardFileTypeWiseList =
                (List<VMAdminDashboardFileTypeWise>)lstFileMaster.GroupBy(n => n.ApplicationType)
                         .Select(n => new VMAdminDashboardFileTypeWise
                         {
                             ApplicationType = n.Key,
                             TotalFile = n.Count(),
                             Visited = n.Where(x => x.IsVisited == true).ToList().Count()
                         })
                         .OrderBy(n => n.ApplicationTypeName).ToList();
            return await Task.FromResult(adminDashboard);
        }
        public async Task<List<VMAdminDashboardFileUserWise>> GetFileListByFileType(int type)
        {
            int organizationID = await _httpContextAccessor.HttpContext.User.GetOrginzationIdFromClaimIdentity();
            List<VMAdminDashboardFileUserWise> oList = new List<VMAdminDashboardFileUserWise>();
            var lstUser = _context.Users.Where(x => x.OrganizationID == organizationID).ToList();
            var lstDepartment = _context.Departments.ToList();
            List<ApplicationFileMaster> lstFileMaster = _context.ApplicationFileMasters.ToList();
            if (type > 0)
            {
                lstFileMaster = lstFileMaster.Where(x => (int)x.ApplicationType == type).ToList();
            }
            IEnumerable<string> fileIDs = lstFileMaster.Select(x => x.ApplicationFileMasterID.ToString()).ToList();
            var lstMap = _context.ApplicationFileUserMaps.Where(x => fileIDs.Contains(x.ApplicationFileMasterID.ToString())).ToList();


            foreach (Users user in lstUser)
            {
                VMAdminDashboardFileUserWise data = new VMAdminDashboardFileUserWise();
                data.UserID = user.UserAutoID;
                data.UserFullName = user.UserFullName;
                var dept = lstDepartment.SingleOrDefault(x => x.DepartmentID == user.DepartmentID);
                if (dept != null)
                {
                    data.DepartmentID = dept.DepartmentID;
                    data.DepartmentName = dept.DepartmentName;
                }
                var totalList = lstMap.Where(x => x.UserID == user.UserAutoID).ToList();
                data.TotalFile = totalList.Count;

                IEnumerable<string> userFileIDs = totalList.Select(x => x.ApplicationFileMasterID.ToString()).ToList();
                var visitedList = lstFileMaster.Where(x => userFileIDs.Contains(x.ApplicationFileMasterID.ToString()) && x.IsVisited == true).ToList();

                data.Visited = visitedList.Count;
                oList.Add(data);
            }


            return await Task.FromResult(oList);
        }
        public async Task<List<VMAdminDashboardFileListUserWise>> GetFileListByTypeVisitedUser(QueryObject queryObject)
        {
            int organizationID = await _httpContextAccessor.HttpContext.User.GetOrginzationIdFromClaimIdentity();
            List<VMAdminDashboardFileListUserWise> oList = new List<VMAdminDashboardFileListUserWise>();
            List<ApplicationFileMaster> lstFileMaster = _context.ApplicationFileMasters.ToList();
            if (queryObject.ApplicationType > 0)
            {
                lstFileMaster = _context.ApplicationFileMasters.Where(x => (int)x.ApplicationType == queryObject.ApplicationType).ToList();
            }

            if (queryObject.IsVisited == true)
            {
                lstFileMaster = lstFileMaster.Where(x => x.IsVisited == true).ToList();
            }
            if (queryObject.IsVisited == false)
            {
                lstFileMaster = lstFileMaster.Where(x => x.IsVisited == false).ToList();
            }

            IEnumerable<string> fileIDs = lstFileMaster.Select(x => x.ApplicationFileMasterID.ToString()).ToList();
            var lstMap = _context.ApplicationFileUserMaps.Where(x => fileIDs.Contains(x.ApplicationFileMasterID.ToString()) && x.UserID == queryObject.UserID).ToList();

            var query =
                from file in lstFileMaster
                join map in lstMap on file.ApplicationFileMasterID equals map.ApplicationFileMasterID
                join thana in _context.Thanas.ToList() on file.ThanaID equals thana.ThanaID
                join mouza in _context.Mouzas.ToList() on file.MouzaID equals mouza.MouzaID

                select new VMAdminDashboardFileListUserWise
                {
                    RefNo = file.RefNo,
                    ApplicantName = file.ApplicantName,
                    ThanaID = file.ThanaID,
                    ThanaName = thana.ThanaName,
                    MouzaID = file.MouzaID,
                    MouzaName = mouza.MouzaName,
                    IsVisited = file.IsVisited,
                    AssignDate = map.AssignDate,
                    TargetDate = map.TargetDate,
                    VisitDate = map.CreatedDate
                };
            oList = query.ToList();
            return await Task.FromResult(oList);
        }
    }
}

