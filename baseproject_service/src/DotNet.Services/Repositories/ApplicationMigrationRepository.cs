﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using System.Net.Mail;
using OfficeOpenXml;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using System.Globalization;
using Microsoft.AspNetCore.Http.Internal;
using System.Drawing;
using DotNet.ApplicationCore.DTOs.VM;
using Microsoft.Extensions.Configuration;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.CDA;

namespace DotNet.Services.Repositories
{
    public class ApplicationMigrationRepository : IApplicationMigrationRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public ApplicationMigrationRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper,
            IConfiguration configuration
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
            _configuration = configuration;
        }
        public async Task<IEnumerable<VMApplicationMigration>> GetAll(int type)
        {
            List<ApplicationMigration> dataList = new List<ApplicationMigration>();
            if (type == 0)
            {
                dataList = _context.ApplicationMigrations.Where(x => x.ApplicationFileMasterID == 0).ToList();
            }
            else if (type == 1)
            {
                dataList = _context.ApplicationMigrations.Where(x => x.ApplicationFileMasterID > 0).ToList();
            }
            else
            {
                dataList = _context.ApplicationMigrations.ToList();
            }
            var sql =
                    (from appMigration in dataList

                     join thana in _context.Thanas on appMigration.ThanaID equals thana.ThanaID into lstThana
                     from objthana in lstThana.DefaultIfEmpty()

                     join mouza in _context.Mouzas on appMigration.MouzaID equals mouza.MouzaID into lstMouza
                     from objMouza in lstMouza.DefaultIfEmpty()

                     select new VMApplicationMigration
                     {
                         ApplicationMigrationID = appMigration.ApplicationMigrationID,
                         ApplicationFileMasterID = appMigration.ApplicationFileMasterID,
                         RefNo = appMigration.RefNo,
                         ApplicantName = appMigration.ApplicantName,
                         ApprovalDate = appMigration.ApprovalDate,
                         ApplicationType = (ApplicationType)appMigration.ApplicationType,
                         RSNo = appMigration.RSNo == null ? "" : appMigration.RSNo,
                         BSNo = appMigration.BSNo == null ? "" : appMigration.BSNo,
                         ThanaID = appMigration.ThanaID,
                         MouzaID = appMigration.MouzaID,
                         Road = appMigration.Road == null ? "" : appMigration.Road,
                         CreatedBy = appMigration.CreatedBy,
                         CreatedDate = appMigration.CreatedDate,
                         UpdatedBy = appMigration.UpdatedBy,
                         UpdatedDate = appMigration.UpdatedDate,
                         ThanaName = (objthana == null) ? "" : objthana.ThanaName,
                         MouzaName = (objMouza == null) ? "" : objMouza.MouzaName
                     });
            List<VMApplicationMigration> retData = sql.ToList();
            return await Task.FromResult(retData);
        }
        public async Task<ApplicationMigration> GetByID(int id)
        {
            var result = _context.ApplicationMigrations.SingleOrDefault(x => x.ApplicationMigrationID == id);
            return await Task.FromResult(result);
        }
        public async Task<ApplicationMigration> Add(ApplicationMigration ApplicationMigration)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            _context.ApplicationMigrations.Add(ApplicationMigration);
            _context.SaveChanges();

            return await GetByID(ApplicationMigration.ApplicationMigrationID);
        }
        public async Task<ApplicationMigration> Update(ApplicationMigration applicationMigration)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var data = await GetByID(applicationMigration.ApplicationMigrationID);
            if (data == null)
            {
                throw new Exception();
            }
            if (data.ApplicationFileMasterID > 0)
            {
                throw new Exception("You cant edit this");
            }

            data.RefNo = applicationMigration.RefNo;
            data.ApplicantName = applicationMigration.ApplicantName;
            data.ApprovalDate = applicationMigration.ApprovalDate;
            data.ApplicationType = applicationMigration.ApplicationType;
            data.RSNo = applicationMigration.RSNo;
            data.BSNo = applicationMigration.BSNo;
            data.ThanaID = applicationMigration.ThanaID;
            data.MouzaID = applicationMigration.MouzaID;
            data.Road = applicationMigration.Road;
            data.UpdatedBy = userId;
            data.UpdatedDate = DateTime.Now;

            _context.ApplicationMigrations.Attach(data);
            _context.Entry(data).State = EntityState.Modified;
            _context.SaveChanges();
            return data;
        }
        public async Task<bool> Delete(int ApplicationMigrationID)
        {
            var data = await GetByID(ApplicationMigrationID);
            if (data != null)
            {
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
        //public async Task<string> ExcelUpload(Attachments attachment)
        //{
        //    var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
        //    string fileName = "";
        //    string fileFormat = ".xlsx";
        //    string fileSavePath = "";
        //    var filePath = "";

        //    if (!string.IsNullOrEmpty(attachment.FileContent) && attachment.FileContent.Contains("data"))
        //    {
        //        fileName = attachment.AttachmentName + fileFormat;
        //        filePath = Path.Combine(fileSavePath, fileName + fileFormat);
        //        var fileLocation = new FileInfo(filePath);
        //        if (File.Exists(filePath))
        //        {
        //            File.Delete(filePath);
        //        }
        //        fileSavePath = Path.Combine(fileSavePath, fileName);
        //        string cleandata = attachment.FileContent.Split(',')[1];
        //        byte[] data = System.Convert.FromBase64String(cleandata);
        //        File.WriteAllBytes(fileSavePath, data);

        //        List<ApplicationMigration> oData = new List<ApplicationMigration>();
        //        oData = SaveMigrationData(attachment, filePath);
        //        if(oData != null && oData.Count > 0)
        //        {
        //            _context.ApplicationMigrations.AddRange(oData);
        //            _context.SaveChanges();
        //        }
        //    }
        //    return "";
        //}
        public async Task<ResponseMessage> ExcelUpload(VMAttachments attachment)
        {
            ResponseMessage responseMessage = new ResponseMessage();
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var fileSavePath = _configuration.GetSection("FilePath:fileSavePath");
            var fileGetPath = _configuration.GetSection("FilePath:fileGetPath");

            string fileName = "";
            string fileFormat = ".xlsx";
            if (!Directory.Exists(fileSavePath.ToString()))
            {
                Directory.CreateDirectory(fileSavePath.ToString());
            }
            if (!string.IsNullOrEmpty(attachment.FileContent) && attachment.FileContent.Contains("data"))
            {
                try
                {
                    fileName = attachment.AttachmentName + fileFormat;
                    var filePath = Path.Combine(fileSavePath.ToString(), fileName + fileFormat);
                    var fileLocation = new FileInfo(filePath);
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }
                    string cleandata = attachment.FileContent.Split(',')[1];
                    byte[] data = System.Convert.FromBase64String(cleandata);
                    File.WriteAllBytes(fileSavePath.ToString() + "/" + fileName, data);

                    List<ApplicationMigration> oData = new List<ApplicationMigration>();
                    oData = SaveMigrationData(fileGetPath.ToString() + "\\" + fileName, userId, attachment.AttachementTypeID);
                    if (oData != null && oData.Count > 0)
                    {
                        _context.ApplicationMigrations.AddRange(oData);
                        _context.SaveChanges();
                    }
                    responseMessage.Message = "Success";
                    responseMessage.StatusCode = ReturnStatus.Success;
                    return responseMessage;
                }
                catch (Exception ex)
                {
                    responseMessage.Message = ex.Message;
                    responseMessage.StatusCode = ReturnStatus.Failed;
                    return responseMessage;
                }
            }
            return responseMessage;
        }
        public List<ApplicationMigration> SaveMigrationData(string fileNameWithPath, int userID, int attachementTypeID)
        {

            var file = new FileInfo(fileNameWithPath);

            List<Thana> lstThana = _context.Thanas.ToList();
            List<Mouza> lstMouza = _context.Mouzas.ToList();
            List<ApplicationMigration> retDataList = new List<ApplicationMigration>();
            using (ExcelPackage package = new ExcelPackage(file))
            {
                var workSheetData = package.Workbook.Worksheets.First();
                int totalRowCount = workSheetData.Dimension.Rows;
                var maxData = _context.GlobalSettings.SingleOrDefault(x => x.GlobalSettingID == (int)GlobalSettingsEnum.Application_Migration_Max_Data_Upload);
                int maxDataRow = (int)maxData.Value;

                int colNo = 1;
                IDictionary<int, string> lstCol = new Dictionary<int, string>();
                lstCol.Add(colNo++, "RefNo");
                lstCol.Add(colNo++, "ApplicantName");
                lstCol.Add(colNo++, "ApprovalDate");
                lstCol.Add(colNo++, "RSNo");
                lstCol.Add(colNo++, "BSNo");
                lstCol.Add(colNo++, "Thana");
                lstCol.Add(colNo++, "Mouza");
                lstCol.Add(colNo++, "Road");

                for (int rowIndex = 2; rowIndex <= totalRowCount; rowIndex++)
                {
                    int colIndex = 2;
                    ApplicationMigration applicationMigration = new ApplicationMigration();

                    applicationMigration.RefNo = workSheetData.Cells[rowIndex, colIndex].Value == null ? null : workSheetData.Cells[rowIndex, colIndex++].Value.ToString();
                    applicationMigration.ApplicantName = workSheetData.Cells[rowIndex, colIndex].Value == null ? null : workSheetData.Cells[rowIndex, colIndex++].Value.ToString();

                    #region Date
                    DateTime approvalDate;
                    double dApprovalDate;
                    var approvalDateVal = workSheetData.Cells[rowIndex, colIndex].Value;
                    if (approvalDateVal != null)
                    {
                        var strIssueDate = workSheetData.Cells[rowIndex, colIndex].Value.ToString();
                        if (double.TryParse(strIssueDate, out dApprovalDate))
                        {
                            applicationMigration.ApprovalDate = DateTime.FromOADate(dApprovalDate);
                        }
                        else if (DateTime.TryParseExact(strIssueDate, "dd-mm-yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out approvalDate))
                        {
                            applicationMigration.ApprovalDate = Convert.ToDateTime(approvalDate);
                        }
                        else if (DateTime.TryParse(strIssueDate, out approvalDate))
                        {
                            applicationMigration.ApprovalDate = approvalDate;
                        }
                        //else
                        //{
                        //    "";
                        //}
                    }
                    colIndex++;
                    #endregion


                    if (attachementTypeID > 0)
                    {
                        applicationMigration.ApplicationType = (ApplicationType)attachementTypeID;
                    }

                    applicationMigration.RSNo = workSheetData.Cells[rowIndex, colIndex].Value == null ? null : workSheetData.Cells[rowIndex, colIndex++].Value.ToString(); colIndex++;
                    applicationMigration.BSNo = workSheetData.Cells[rowIndex, colIndex].Value == null ? null : workSheetData.Cells[rowIndex, colIndex++].Value.ToString();

                    #region thana & mouza
                    var thana = workSheetData.Cells[rowIndex, colIndex].Value == null ? null : workSheetData.Cells[rowIndex, colIndex++].Value;
                    if (thana != null)
                    {
                        var selectedThana = lstThana.SingleOrDefault(x => x.ThanaNameBangla.Trim() == thana.ToString().Trim());
                        colIndex++;
                        if (selectedThana != null)
                        {
                            applicationMigration.ThanaID = selectedThana.ThanaID;

                            #region mouza
                            var mouza = workSheetData.Cells[rowIndex, colIndex].Value == null ? null : workSheetData.Cells[rowIndex, colIndex++].Value;
                            if (mouza != null)
                            {
                                var selectedMouza = lstMouza.SingleOrDefault(x => x.MouzaNameBangla == mouza.ToString() && x.ThanaID == selectedThana.ThanaID);
                                if (selectedMouza != null)
                                {
                                    applicationMigration.MouzaID = selectedMouza.MouzaID;
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                    applicationMigration.ApplicationFileMasterID = 0;
                    applicationMigration.Road = workSheetData.Cells[rowIndex, colIndex].Value == null ? null : workSheetData.Cells[rowIndex, colIndex++].Value.ToString();
                    applicationMigration.CreatedBy = userID;
                    applicationMigration.CreatedDate = DateTime.Now;
                    applicationMigration.UpdatedBy = userID;
                    applicationMigration.ApprovalDate = DateTime.Now;
                    applicationMigration.UpdatedDate = DateTime.Now;

                    retDataList.Add(applicationMigration);
                }
            }
            return retDataList;
        }
    }
}

