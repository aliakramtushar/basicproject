﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using System.Net.Mail;
using OfficeOpenXml;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using System.Globalization;
using Microsoft.AspNetCore.Http.Internal;
using System.Drawing;
using DotNet.ApplicationCore.DTOs.VM;
using Microsoft.Extensions.Configuration;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.CDA;
using System.Xml.Linq;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;

namespace DotNet.Services.Repositories
{
    public class ApplicationFileMasterRepository : IApplicationFileMasterRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;


        public ApplicationFileMasterRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper,
            IConfiguration configuration
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
            _configuration = configuration;
        }
        public async Task<IEnumerable<VMApplicationFileMaster>> GetAll()
        {
            var sql =
                    from fileMaster in _context.ApplicationFileMasters.ToList()

                    join thana in _context.Thanas on fileMaster.ThanaID equals thana.ThanaID into lstThana
                    from objthana in lstThana.DefaultIfEmpty()

                    join mouza in _context.Mouzas on fileMaster.MouzaID equals mouza.MouzaID into lstMouza
                    from objMouza in lstMouza.DefaultIfEmpty()

                    join map in _context.ApplicationFileUserMaps on fileMaster.ApplicationFileMasterID equals map.ApplicationFileMasterID into gj_map
                    from sub_map in gj_map.DefaultIfEmpty()

                    select new VMApplicationFileMaster
                    {
                        ApplicationFileMasterID = fileMaster.ApplicationFileMasterID,
                        RefNo = fileMaster.RefNo,
                        ApplicantName = fileMaster.ApplicantName,
                        ApprovalDate = fileMaster.ApprovalDate,
                        ApplicationType = (ApplicationType)fileMaster.ApplicationType,
                        RSNo = fileMaster.RSNo == null ? "" : fileMaster.RSNo,
                        BSNo = fileMaster.BSNo == null ? "" : fileMaster.BSNo,
                        ThanaID = fileMaster.ThanaID,
                        MouzaID = fileMaster.MouzaID,
                        Road = fileMaster.Road == null ? "" : fileMaster.Road,
                        IsVisited = fileMaster.IsVisited,
                        Latitude = fileMaster.Latitude,
                        Longitude = fileMaster.Longitude,
                        CreatedBy = fileMaster.CreatedBy,
                        CreatedDate = fileMaster.CreatedDate,
                        UpdatedBy = fileMaster.UpdatedBy,
                        UpdatedDate = fileMaster.UpdatedDate,
                        ThanaName = (objthana == null) ? "" : objthana.ThanaName,
                        ThanaNameBangla = (objthana == null) ? "" : objthana.ThanaNameBangla,
                        MouzaName = (objMouza == null) ? "" : objMouza.MouzaName,
                        MouzaNameBangla = (objMouza == null) ? "" : objMouza.MouzaNameBangla,
                        UserID = (sub_map == null) ? 0 : sub_map.UserID,
                        //UserFullName = (user == null) ? "" : user.UserFullName,
                    };
            List<VMApplicationFileMaster> retData = sql.ToList();
            foreach (VMApplicationFileMaster item in retData)
            {
                if (item.UserID > 0)
                {
                    var user = _context.Users.SingleOrDefault(x => x.UserAutoID == item.UserID);
                    if (user != null)
                    {
                        item.UserFullName = user.UserFullName;

                    }
                };
            }
            return await Task.FromResult(retData);
        }
        public async Task<VMApplicationFileMaster> GetByID(int id)
        {
            //var result = _context.ApplicationFileMasters.SingleOrDefault(x => x.ApplicationFileMasterID == id);
            var sql =
                    from fileMaster in _context.ApplicationFileMasters.Where(x => x.ApplicationFileMasterID == id).ToList()

                    join thana in _context.Thanas on fileMaster.ThanaID equals thana.ThanaID into lstThana
                    from objthana in lstThana.DefaultIfEmpty()

                    join mouza in _context.Mouzas on fileMaster.MouzaID equals mouza.MouzaID into lstMouza
                    from objMouza in lstMouza.DefaultIfEmpty()

                    join map in _context.ApplicationFileUserMaps on fileMaster.ApplicationFileMasterID equals map.ApplicationFileMasterID into gj_map
                    from sub_map in gj_map.DefaultIfEmpty()

                    select new VMApplicationFileMaster
                    {
                        ApplicationFileMasterID = fileMaster.ApplicationFileMasterID,
                        RefNo = fileMaster.RefNo,
                        ApplicantName = fileMaster.ApplicantName,
                        ApprovalDate = fileMaster.ApprovalDate,
                        ApplicationType = (ApplicationType)fileMaster.ApplicationType,
                        RSNo = fileMaster.RSNo == null ? "" : fileMaster.RSNo,
                        BSNo = fileMaster.BSNo == null ? "" : fileMaster.BSNo,
                        ThanaID = fileMaster.ThanaID,
                        MouzaID = fileMaster.MouzaID,
                        Road = fileMaster.Road == null ? "" : fileMaster.Road,
                        IsVisited = fileMaster.IsVisited,
                        Latitude = fileMaster.Latitude,
                        Longitude = fileMaster.Longitude,
                        CreatedBy = fileMaster.CreatedBy,
                        CreatedDate = fileMaster.CreatedDate,
                        UpdatedBy = fileMaster.UpdatedBy,
                        UpdatedDate = fileMaster.UpdatedDate,
                        ThanaName = (objthana == null) ? "" : objthana.ThanaName,
                        ThanaNameBangla = (objthana == null) ? "" : objthana.ThanaNameBangla,
                        MouzaName = (objMouza == null) ? "" : objMouza.MouzaName,
                        MouzaNameBangla = (objMouza == null) ? "" : objMouza.MouzaNameBangla,
                        UserID = (sub_map == null) ? 0 : sub_map.UserID,
                    };
            VMApplicationFileMaster retData = sql.ToList().SingleOrDefault();
            if (retData.UserID > 0)
            {
                var user = _context.Users.SingleOrDefault(x => x.UserAutoID == retData.UserID);
                if (user != null)
                {
                    retData.UserFullName = user.UserFullName;
                }
            };
            return await Task.FromResult(retData);
        }
        public async Task<ApplicationFileMaster> Add(ApplicationFileMaster applicationFileMaster)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            _context.ApplicationFileMasters.Add(applicationFileMaster);
            _context.SaveChanges();
            return applicationFileMaster;
        }
        public async Task<ApplicationFileMaster> Update(ApplicationFileMaster oApplicationFileMaster)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var data = _context.ApplicationFileMasters.SingleOrDefault(x => x.ApplicationFileMasterID == oApplicationFileMaster.ApplicationFileMasterID);
            if (data == null)
            {
                throw new Exception();
            }
            data.RefNo = oApplicationFileMaster.RefNo;
            data.ApplicantName = oApplicationFileMaster.ApplicantName;
            data.ApprovalDate = oApplicationFileMaster.ApprovalDate;
            data.ApplicationType = oApplicationFileMaster.ApplicationType;
            data.RSNo = oApplicationFileMaster.RSNo;
            data.BSNo = oApplicationFileMaster.BSNo;
            data.ThanaID = oApplicationFileMaster.ThanaID;
            data.MouzaID = oApplicationFileMaster.MouzaID;
            data.DPZID = oApplicationFileMaster.DPZID;
            data.Road = oApplicationFileMaster.Road;
            data.IsVisited = oApplicationFileMaster.IsVisited;
            //data.Latitude = oApplicationFileMaster.;
            //data.Longitude = oApplicationFileMaster.;
            data.UpdatedBy = userId;
            data.UpdatedDate = DateTime.Now;

            _context.ApplicationFileMasters.Attach(data);
            _context.Entry(data).State = EntityState.Modified;
            _context.SaveChanges();
            return data;
        }
        public async Task<bool> Delete(int ApplicationFileMasterID)
        {
            var data = await GetByID(ApplicationFileMasterID);
            if (data != null)
            {
                if (data.IsVisited)
                {
                    throw new Exception("You can not delete this. this is already visited");
                }
                var map = _context.ApplicationFileUserMaps.Where(x => x.ApplicationFileMasterID == data.ApplicationFileMasterID).ToList();
                if (map.Count > 0)
                {
                    throw new Exception("You can not delete this. there is an officer");
                }
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
        public async Task<ResponseMessage> MigrateData(List<ApplicationMigration> lstData)
        {
            ResponseMessage responseMessage = new ResponseMessage();
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var data = lstData.Where(x => x.ApplicationFileMasterID == 0).ToList();
            _context.Database.BeginTransaction();
            try
            {

                if (data.Count > 0)
                {
                    IEnumerable<string> dataIDs = data.Select(x => x.ApplicationMigrationID.ToString()).ToList();
                    var dbList_Migration = _context.ApplicationMigrations.Where(x => dataIDs.Contains(x.ApplicationMigrationID.ToString()) && x.ApplicationFileMasterID == 0).ToList();

                    foreach (ApplicationMigration oItem in dbList_Migration)
                    {
                        var duplicateDate = _context.ApplicationFileMasters.SingleOrDefault(x => x.RefNo.Trim() == oItem.RefNo);
                        if (duplicateDate != null)
                        {
                            responseMessage.StatusCode = ReturnStatus.Duplicate;
                            responseMessage.Message = "Ref No already exists";
                            return responseMessage;
                        }
                        ApplicationFileMaster saveData = new ApplicationFileMaster();
                        saveData.RefNo = oItem.RefNo;
                        saveData.ApplicantName = oItem.ApplicantName;
                        saveData.ApprovalDate = oItem.ApprovalDate;
                        saveData.ApplicationType = oItem.ApplicationType;
                        saveData.RSNo = oItem.RSNo;
                        saveData.BSNo = oItem.BSNo;
                        saveData.ThanaID = oItem.ThanaID;
                        saveData.MouzaID = oItem.MouzaID;
                        saveData.DPZID = 0;
                        saveData.Road = oItem.Road;
                        saveData.IsVisited = false;
                        saveData.CreatedBy = userId;
                        saveData.CreatedDate = DateTime.Now;
                        saveData.UpdatedBy = userId;
                        saveData.UpdatedDate = DateTime.Now;
                        _context.ApplicationFileMasters.Add(saveData);
                        _context.SaveChanges();

                        oItem.ApplicationFileMasterID = saveData.ApplicationFileMasterID;
                        _context.ApplicationMigrations.Attach(oItem);
                        _context.Entry(oItem).State = EntityState.Modified;
                    }
                    _context.SaveChanges();
                    _context.Database.CurrentTransaction.Commit();
                    responseMessage.StatusCode = ReturnStatus.Success;
                    responseMessage.ResponseObj = dbList_Migration;
                }
                else
                {
                    responseMessage.Message = "no data";
                }
            }
            catch (Exception ex)
            {
                _context.Database.CurrentTransaction.Rollback();
                responseMessage.StatusCode = ReturnStatus.Failed;
                responseMessage.Message = ex.InnerException.Message;
            }
            return responseMessage;
        }
        public async Task<ResponseMessage> GetListByAssingedPerson(QueryObject oQueryObject)
        {
            ResponseMessage responseMessage = new ResponseMessage();
            List<ApplicationFileMaster> lstApplicationFileMaster = new List<ApplicationFileMaster>();
            List<ApplicationFileUserMap> lstApplicationFileUserMap = new List<ApplicationFileUserMap>();
            List<Mouza> lstMouza = _context.Mouzas.ToList();
            List<Thana> lstThana = _context.Thanas.ToList();
            List<DPZ> lstDPZ = _context.DPZs.ToList();
            List<Users> lstUser = _context.Users.ToList();

            if (oQueryObject.UserID > 0)
            {
                lstApplicationFileUserMap = _context.ApplicationFileUserMaps.Where(x => x.UserID == oQueryObject.UserID).ToList();
                IEnumerable<string> mapIDs = lstApplicationFileUserMap.Select(x => x.ApplicationFileMasterID.ToString()).ToList();
                if (oQueryObject.StatusID == 0)
                {
                    lstApplicationFileMaster = _context.ApplicationFileMasters.Where(x => mapIDs.Contains(x.ApplicationFileMasterID.ToString()) && x.IsVisited == false).ToList();
                }
                if (oQueryObject.StatusID == 1)
                {
                    lstApplicationFileMaster = _context.ApplicationFileMasters.Where(x => mapIDs.Contains(x.ApplicationFileMasterID.ToString()) && x.IsVisited == true).ToList();
                }
                if (oQueryObject.StatusID == 2)
                {
                    lstApplicationFileMaster = _context.ApplicationFileMasters.Where(x => mapIDs.Contains(x.ApplicationFileMasterID.ToString())).ToList();
                }


                var query =
                    from file in lstApplicationFileMaster
                    join map in lstApplicationFileUserMap on file.ApplicationFileMasterID equals map.ApplicationFileMasterID
                    join mouza in lstMouza on file.MouzaID equals mouza.MouzaID
                    join thana in lstThana on file.ThanaID equals thana.ThanaID
                    //join dpz in lstDPZ on file.DPZID equals dpz.DPZID
                    join user in lstUser on map.AssignBy equals user.UserAutoID

                    select new VMApplicationFileMaster
                    {
                        ApplicationFileMasterID = file.ApplicationFileMasterID,
                        RefNo = file.RefNo,
                        ApplicantName = file.ApplicantName,
                        ApprovalDate = file.ApprovalDate,
                        ApplicationType = file.ApplicationType,
                        RSNo = file.RSNo,
                        BSNo = file.BSNo,
                        ThanaID = file.ThanaID,
                        MouzaID = file.MouzaID,
                        DPZID = file.DPZID,
                        Road = file.Road,
                        IsVisited = file.IsVisited,
                        Latitude = file.Latitude,
                        Longitude = file.Longitude,
                        ThanaName = thana.ThanaName,
                        ThanaNameBangla = thana.ThanaNameBangla,
                        MouzaName = mouza.MouzaName,
                        MouzaNameBangla = mouza.MouzaNameBangla,
                        //DPZName = dpz.DPZName,
                        //DPZNameBangla = dpz.DPZNameBangla,
                        //AssignBy = map.AssignBy,
                        //AssignByName = user.UserFullName,
                        //AssignByNameBangla = user.UserFullNameBangla,
                        AssignDate = map.AssignDate,
                    };
                responseMessage.StatusCode = ReturnStatus.Success;
                responseMessage.ResponseObj = query.ToList();
            }
            return responseMessage;
        }
        public async Task<ResponseMessage> UpdateApplicationFileGeofence(QueryObject oQueryObject)
        {
            ResponseMessage responseMessage = new ResponseMessage();
            try
            {
                var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
                var data = _context.ApplicationFileMasters.SingleOrDefault(x => x.ApplicationFileMasterID == oQueryObject.ApplicationFileMasterID);
                if (data == null)
                {
                    throw new Exception("No Data");
                }
                data.IsVisited = true;
                data.Latitude = oQueryObject.Latitude;
                data.Longitude = oQueryObject.Longitude;
                data.UpdatedBy = userId;
                data.UpdatedDate = DateTime.Now;

                var vmData = await GetByID(oQueryObject.ApplicationFileMasterID);
                _context.ApplicationFileMasters.Attach(data);
                _context.Entry(data).State = EntityState.Modified;
                _context.SaveChanges();

                responseMessage.StatusCode = ReturnStatus.Success;
                responseMessage.ResponseObj = data;
            }
            catch (Exception ex)
            {
                responseMessage.Message = ex.Message;
                responseMessage.StatusCode = ReturnStatus.Failed;
            }
            return responseMessage;
        }
        public async Task<ResponseMessage> SaveApplicationFileAttachments(IFormCollection request, int applicationFileMasterID, int attachmentType)
        {
            ResponseMessage responseMessage = new ResponseMessage();
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var fileSavePath = _configuration.GetSection("FilePath:fileSavePath");
            var fileGetPath = _configuration.GetSection("FilePath:fileGetPath");
            _context.Database.BeginTransaction();
            try
            {
                var files = request.Files;
                var fileMaster = _context.ApplicationFileMasters.SingleOrDefault(x => x.ApplicationFileMasterID == applicationFileMasterID);
                if (fileMaster == null)
                {
                    throw new Exception("There is no master file");
                }

                foreach (var file in files)
                {
                    string fileName = DateTime.Now.ToString("ddMMyyyyHHmmssfff") + "_" + applicationFileMasterID.ToString().PadLeft(10, '0').ToString() + "." + file.FileName.Split(".")[1];
                    string sFileSavePath = fileGetPath.Value.ToString() + "\\" + fileMaster.CreatedDate.Year.ToString();

                    var filePath = Path.Combine(sFileSavePath, fileName.ToString());
                    using (FileStream fs = System.IO.File.Create(filePath))
                    {
                        file.CopyTo(fs);
                    }
                    Attachments oAttachment = new Attachments();
                    oAttachment.ReferenceType = 0;
                    oAttachment.ReferenceID = applicationFileMasterID;
                    oAttachment.FileFormat = Path.GetExtension(file.FileName);
                    oAttachment.AttachementTypeID = attachmentType;
                    oAttachment.AttachmentName = fileName;
                    oAttachment.AttachmentLink = fileMaster.CreatedDate.Year.ToString();
                    oAttachment.FileContent = null;
                    oAttachment.Notes = null;
                    oAttachment.Status = 0;
                    oAttachment.CreatedBy = userId;
                    oAttachment.CreatedDate = DateTime.Now;
                    oAttachment.UpdatedBy = userId;
                    oAttachment.UpdatedDate = DateTime.Now;

                    _context.Attachments.Add(oAttachment);
                }
                _context.SaveChanges();
                _context.Database.CurrentTransaction.Commit();

                var attachmentList = _context.Attachments.Where(x => x.ReferenceID == applicationFileMasterID).ToList();
                responseMessage.StatusCode = ReturnStatus.Success;
                responseMessage.ResponseObj = attachmentList;
            }
            catch (Exception ex)
            {
                _context.Database.CurrentTransaction.Rollback();
                responseMessage.Message = ex.Message;
                responseMessage.StatusCode = ReturnStatus.Failed;
            }
            return responseMessage;
        }
        //private AttachmentType GetAttachmentTypeByFileFormat(string fileFormat, int docType)
        //{
        //    if (fileFormat.ToLower().Contains("wav")
        //        || fileFormat.ToLower().Contains("mp3"))
        //    {
        //        return AttachmentType.Audio;
        //    }
        //    if (fileFormat.ToLower().Contains("jpg")
        //        || fileFormat.ToLower().Contains("jpeg")
        //        || fileFormat.ToLower().Contains("png")
        //        || fileFormat.ToLower().Contains("gif"))
        //    {
        //        return AttachmentType.Image;
        //    }
        //    if (fileFormat.ToLower().Contains("3gp")
        //        || fileFormat.ToLower().Contains("mp4")
        //        || fileFormat.ToLower().Contains("mkv"))
        //    {
        //        return AttachmentType.Video;
        //    }
        //    if (fileFormat.ToLower().Contains("map"))
        //    {
        //        return AttachmentType.Map;
        //    }
        //    return AttachmentType.File;
        //}
    }
}

