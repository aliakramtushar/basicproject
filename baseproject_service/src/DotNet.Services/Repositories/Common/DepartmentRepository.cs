﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using DotNet.ApplicationCore.Entities;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;

namespace DotNet.Services.Repositories.Common
{
    //IGenericRepository<Department>,
    public class DepartmentRepository : IDepartmentRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        public DepartmentRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
        }
        public async Task<IEnumerable<Department>> GetAll()
        {
            var Departments = _context.Departments.OrderBy(x=>x.OrderNo).ToList();
            return await Task.FromResult(Departments);
        }
        public async Task<Department> GetByID(int id)
        {
            var result = _context.Departments.SingleOrDefault(x => x.DepartmentID == id);
            return await Task.FromResult(result);
        }
        public async Task<Department> Add(Department Department)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            _context.Departments.Add(Department);
            _context.SaveChanges();

            return await GetByID(Department.DepartmentID);
        }
        public async Task<Department> Update(Department Department)
        {
            var data = await GetByID(Department.DepartmentID);
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();

            if (data == null)
            {
                throw new Exception();
            }
            data.DepartmentName = Department.DepartmentName;
            data.DepartmentNameBangla = Department.DepartmentNameBangla;
            data.ShortName = Department.ShortName;
            data.IsActive = Department.IsActive;
            data.IsActive = Department.IsActive;
            data.ParentDepartmentID = Department.ParentDepartmentID;
            data.OrganizationID = Department.OrganizationID;
            data.OrderNo = Department.OrderNo;

            _context.Departments.Attach(data);
            _context.Entry(data).State = EntityState.Modified;
            _context.SaveChanges();
            return data;
        }
        public async Task<bool> Delete(int DepartmentID)
        {
            var data = await GetByID(DepartmentID);
            if (data != null)
            {
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
        public async Task<IEnumerable<Department>> UpdateOrder(List<Department> oList)
        {
            var dbList = await GetAll();
            foreach (Department Department in dbList)
            {
                var obj = oList.SingleOrDefault(x => x.DepartmentID == Department.DepartmentID);
                if (obj != null)
                {
                    Department.OrderNo = obj.OrderNo;
                    _context.Departments.Attach(Department);
                    _context.Entry(Department).State = EntityState.Modified;
                }
            }
            _context.SaveChanges();
            var dbList_updated = await GetAll();
            return dbList_updated;
        }
<<<<<<< HEAD
        public async Task<IEnumerable<Department>> GetListByOrganizationID(int id)
        {
            var Departments = _context.Departments.Where(x=>x.OrganizationID == id).OrderBy(x => x.OrderNo).ToList();
            return await Task.FromResult(Departments);
        }
=======
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
    }
}

