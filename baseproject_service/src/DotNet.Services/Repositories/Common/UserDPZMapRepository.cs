﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using System.Diagnostics.Metrics;
using DotNet.ApplicationCore.DTOs.VM;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Runtime.Intrinsics.Arm;

namespace DotNet.Services.Repositories.Common
{
    public class UserDPZMapRepository : IUserDPZMapRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        public UserDPZMapRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
        }

        public async Task<IEnumerable<VMUserDPZMap>> GetAll()
        {
            var query =
                from obj in _context.UserDPZMaps.ToList()
                join user in _context.Users.ToList() on obj.UserID equals user.UserAutoID
                join dpz in _context.DPZs.ToList() on obj.DPZID equals dpz.DPZID
<<<<<<< HEAD
                join department in _context.Departments.ToList() on user.DepartmentID equals department.DepartmentID
=======
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa

                select new VMUserDPZMap
                {
                    UserDPZMapID = obj.UserDPZMapID,
                    UserID = obj.UserID,
                    DPZID = obj.DPZID,
                    IsActive = obj.IsActive,
                    FromDate = obj.FromDate,
                    ToDate = obj.ToDate,
                    UserName = user.UserFullName,
                    DPZName = dpz.DPZName,
                    DPZNameBangla = dpz.DPZNameBangla,
<<<<<<< HEAD
                    DepartmentID = department.DepartmentID,
                    DepartmentName = department.DepartmentName,
=======
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
                };

            var retDataList = query.OrderByDescending(x => x.FromDate).ToList();
            return await Task.FromResult(retDataList);
        }
        public async Task<VMUserDPZMap> GetByID(int id)
        {
            var result = _context.UserDPZMaps.SingleOrDefault(x => x.UserDPZMapID == id);
            VMUserDPZMap retObj = new VMUserDPZMap();

            if (result != null)
            {
                retObj.UserDPZMapID = result.UserDPZMapID;
                retObj.UserID = result.UserID;
                retObj.DPZID = result.DPZID;
                retObj.IsActive = result.IsActive;
                retObj.FromDate = result.FromDate;
                retObj.ToDate = result.ToDate;

                var user = _context.Users.SingleOrDefault(x => x.UserAutoID == result.UserID);
                if (user != null)
                {
                    retObj.UserName = user.UserFullName;
                }
                var dpz = _context.DPZs.SingleOrDefault(x => x.DPZID == result.DPZID);
                if (dpz != null)
                {
                    retObj.DPZName = dpz.DPZName;
                    retObj.DPZNameBangla = dpz.DPZNameBangla;
                }
            }
            return await Task.FromResult(retObj);
        }
        public async Task<VMUserDPZMap> Add(VMUserDPZMap userDPZMap)
        {
            var userID = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            if (userDPZMap == null || userDPZMap.UserDPZMapID > 0)
            {
                throw new Exception("This is not in entry state ");
            }

            _context.Database.BeginTransaction();
            UserDPZMap saveUserDPZMap = new UserDPZMap();
            saveUserDPZMap.UserID = userDPZMap.UserID;
            saveUserDPZMap.DPZID = userDPZMap.DPZID;
            saveUserDPZMap.IsActive = userDPZMap.IsActive;
            saveUserDPZMap.FromDate = userDPZMap.FromDate;
            saveUserDPZMap.ToDate = userDPZMap.ToDate;
            saveUserDPZMap.CreatedBy = userID;
            saveUserDPZMap.CreatedDate = DateTime.Now;
            saveUserDPZMap.UpdatedBy = userID;
            saveUserDPZMap.UpdatedDate = DateTime.Now;
            _context.UserDPZMaps.Add(saveUserDPZMap);
            _context.SaveChanges();

            _context.Database.CommitTransaction();
            return await GetByID(saveUserDPZMap.UserDPZMapID);
        }
        public async Task<VMUserDPZMap> Update(VMUserDPZMap userDPZMap)
        {
            var userID = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();

            var updateUserDPZMap = _context.UserDPZMaps.SingleOrDefault(x => x.UserDPZMapID == userDPZMap.UserDPZMapID);
            if (updateUserDPZMap == null)
            {
                throw new Exception("There is no data");
            }

            _context.Database.BeginTransaction();
            //updateUserDPZMap.UserID = userDPZMap.UserID;
            //updateUserDPZMap.DPZID = userDPZMap.DPZID;
            updateUserDPZMap.IsActive = userDPZMap.IsActive;
            updateUserDPZMap.FromDate = userDPZMap.FromDate;
            updateUserDPZMap.ToDate = userDPZMap.ToDate;
            updateUserDPZMap.UpdatedBy = userID;
            updateUserDPZMap.UpdatedDate = DateTime.Now;

            _context.UserDPZMaps.Attach(updateUserDPZMap);
            _context.Entry(updateUserDPZMap).State = EntityState.Modified;
            _context.SaveChanges();

            _context.Database.CommitTransaction();
            return await GetByID(updateUserDPZMap.UserDPZMapID);
        }
        public async Task<bool> Delete(int UserDPZMapID)
        {
            var data = _context.UserDPZMaps.SingleOrDefault(x => x.UserDPZMapID == UserDPZMapID);
            if (data != null)
            {
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}

