﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using DotNet.ApplicationCore.Entities;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;

namespace DotNet.Services.Repositories.Common
{
    public class UserRepository : IUserRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        public UserRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;

        }
        public AuthUser UserAuthentication(AuthUser user)
        {
            string password = EncryptionHelper.Encrypt(user.Password);
            Users dbUser = _context.Users.SingleOrDefault(x => string.Equals(x.UserID, user.UserID) && string.Equals(x.Password, password));
            AuthUser authUser = new AuthUser();
            if (dbUser != null)
            {
                authUser.UserAutoID = dbUser.UserAutoID;
                authUser.UserID = dbUser.UserID;
                authUser.UserTypeID = dbUser.UserTypeID;
                authUser.OrganizationID = dbUser.OrganizationID;
                authUser.UserFullName = dbUser.UserFullName;
                authUser.UserRoleID = dbUser.UserRoleID;
                authUser.UserImage = dbUser.UserImage;
            }
            return authUser;
        }
        public async Task<IEnumerable<Users>> GetAll()
        {
            int organizationID = await _httpContextAccessor.HttpContext.User.GetOrginzationIdFromClaimIdentity();
            int userAutoId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var users = _context.Users.Where(x => x.OrganizationID == organizationID).ToList();
            return await Task.FromResult(users);
        }
        public async Task<Users> GetByID(int id)
        {
            var result = _context.Users.SingleOrDefault(x => x.UserAutoID == id);
            return await Task.FromResult(result);
        }
        public async Task<Users> Add(Users user)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            user.Password = EncryptionHelper.Encrypt(user.Password);
            user.PasswordExpiryDate = DateTime.Now;
            user.CreatedBy = Convert.ToInt32(userId);
            user.CreatedDate = DateTime.Now;
            user.UpdatedBy = Convert.ToInt32(userId);
            user.UpdatedDate = DateTime.Now;
            _context.Users.Add(user);
            _context.SaveChanges();

            return await GetByID(user.UserAutoID);
        }
        public async Task<Users> Update(Users user)
        {
            var data = await GetByID(user.UserAutoID);
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();

            if (data == null)
            {
                throw new Exception();
            }
            data.UserID = user.UserID;
            data.UserTypeID = user.UserTypeID;
            data.UserRoleID = user.UserRoleID;
            data.OrganizationID = user.OrganizationID;
            data.DepartmentID = user.DepartmentID;
            data.DesignationID = user.DesignationID;
            data.UserFullName = user.UserFullName;
            data.UserFullNameBangla = user.UserFullNameBangla;
            data.MobileNo = user.MobileNo;
            data.Address = user.Address;
            data.PasswordExpiryDate = user.PasswordExpiryDate;
            data.Status = user.Status;
            data.Email = user.Email;
            data.UserImage = user.UserImage;
            data.Signature = user.Signature;
            data.LastLatitude = user.LastLatitude;
            data.LastLongitude = user.LastLongitude;
            data.Is2FAauthenticationEnabled = user.Is2FAauthenticationEnabled;
            data.NID = user.NID;
            data.CanChangeOwnPassword = user.CanChangeOwnPassword;
            data.MobileVerification = user.MobileVerification;
            data.UpdatedBy = Convert.ToInt32(userId);
            data.UpdatedDate = DateTime.Now;

            _context.Users.Attach(data);
            _context.Entry(data).State = EntityState.Modified;
            _context.SaveChanges();

            return data;
        }
        public async Task<bool> Delete(int userAutoID)
        {
            var data = await GetByID(userAutoID);
            if (data != null)
            {
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;   
            }
            return false;
        }
        public async Task<IEnumerable<VMUsers>> GetAllByOrganizationID()
        {
            int organizationID = await _httpContextAccessor.HttpContext.User.GetOrginzationIdFromClaimIdentity();
<<<<<<< HEAD
            int userAutoId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var loginUser = _context.Users.SingleOrDefault(x => x.UserAutoID == userAutoId);
            var lstUser = _context.Users.ToList();
            if (loginUser != null && loginUser.UserRoleID > 1)
            {
                lstUser = lstUser.Where(x => x.OrganizationID == organizationID).ToList();
            }

            var query =
                from user in lstUser

                join department in _context.Departments on user.DepartmentID equals department.DepartmentID into departmentMap
                from departmentFiltered in departmentMap.DefaultIfEmpty()

                join designation in _context.Designations on user.DesignationID equals designation.DesignationID into designationMap
                from designationFiltered in designationMap.DefaultIfEmpty()

                select new VMUsers
                {
                    UserAutoID = user.UserAutoID,
                    UserID = user.UserID,
                    UserTypeID = user.UserTypeID,
                    UserRoleID = user.UserRoleID,
                    OrganizationID = user.OrganizationID,
                    DepartmentID = user.DepartmentID,
                    DesignationID = user.DesignationID,
                    UserFullName = user.UserFullName,
                    UserFullNameBangla = user.UserFullNameBangla,
                    MobileNo = user.MobileNo,
                    Address = user.Address,
                    PasswordExpiryDate = user.PasswordExpiryDate,
                    Status = user.Status,
                    Email = user.Email,
                    UserImage = user.UserImage,
                    Signature = user.Signature,
                    LastLatitude = user.LastLatitude,
                    LastLongitude = user.LastLongitude,
                    Is2FAauthenticationEnabled = user.Is2FAauthenticationEnabled,
                    NID = user.NID,
                    CanChangeOwnPassword = user.CanChangeOwnPassword,
                    MobileVerification = user.MobileVerification,
                    CreatedDate = user.CreatedDate,
                    CreatedBy = user.CreatedBy,
                    UpdatedDate = user.UpdatedDate,
                    UpdatedBy = user.UpdatedBy,
                    DepartmentName = (departmentFiltered == null ? "" : departmentFiltered.DepartmentName),
                    DepartmentNameBangla = (departmentFiltered == null ? "" : departmentFiltered.DepartmentNameBangla),
                    DesignationName = (designationFiltered == null ? "" : designationFiltered.DesignationName),
                    DesignationNameBangla = (designationFiltered == null ? "" : designationFiltered.DesignationNameBangla),
                };
            var retDataList = query.OrderBy(x => x.UserFullName).ThenBy(x => x.CreatedDate).ToList();
            return await Task.FromResult(retDataList);
        }

        public async Task<IEnumerable<VMUsers>> GetAllByLoginUserDepartmentID()
        {
            int organizationID = await _httpContextAccessor.HttpContext.User.GetOrginzationIdFromClaimIdentity();
            int userAutoId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var loginUser = _context.Users.SingleOrDefault(x => x.UserAutoID == userAutoId);

            var datalist = GetUserList(organizationID, loginUser.DepartmentID.Value);
            return await Task.FromResult(datalist);
        }
        public async Task<IEnumerable<VMUsers>> GetAllByDepartmentID(int id)
        {
            int organizationID = await _httpContextAccessor.HttpContext.User.GetOrginzationIdFromClaimIdentity();
            var users = _context.Users.Where(x => x.OrganizationID == organizationID).ToList();

            var datalist = GetUserList(organizationID, id);
            return await Task.FromResult(datalist);
        }
        public IEnumerable<VMUsers> GetUserList(int organizationID, int departmentID)
        {

            var users = _context.Users.Where(x => x.OrganizationID == organizationID).ToList();

            var query =
                from user in _context.Users.Where(x => x.OrganizationID == organizationID).ToList()

=======
            //int userAutoId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var users = _context.Users.Where(x => x.OrganizationID == organizationID).ToList();
            var query =
                from user in _context.Users.Where(x => x.OrganizationID == organizationID).ToList()
                
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
                join department in _context.Departments on user.DepartmentID equals department.DepartmentID into departmentMap
                from departmentFiltered in departmentMap.DefaultIfEmpty()

                join designation in _context.Designations on user.DesignationID equals designation.DesignationID into designationMap
                from designationFiltered in designationMap.DefaultIfEmpty()

<<<<<<< HEAD
                where departmentID == user.DepartmentID

=======
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
                select new VMUsers
                {
                    UserAutoID = user.UserAutoID,
                    UserID = user.UserID,
                    UserTypeID = user.UserTypeID,
                    UserRoleID = user.UserRoleID,
                    OrganizationID = user.OrganizationID,
                    DepartmentID = user.DepartmentID,
                    DesignationID = user.DesignationID,
                    UserFullName = user.UserFullName,
                    UserFullNameBangla = user.UserFullNameBangla,
                    MobileNo = user.MobileNo,
                    Address = user.Address,
                    PasswordExpiryDate = user.PasswordExpiryDate,
                    Status = user.Status,
                    Email = user.Email,
                    UserImage = user.UserImage,
                    Signature = user.Signature,
                    LastLatitude = user.LastLatitude,
                    LastLongitude = user.LastLongitude,
                    Is2FAauthenticationEnabled = user.Is2FAauthenticationEnabled,
                    NID = user.NID,
                    CanChangeOwnPassword = user.CanChangeOwnPassword,
                    MobileVerification = user.MobileVerification,
                    CreatedDate = user.CreatedDate,
                    CreatedBy = user.CreatedBy,
                    UpdatedDate = user.UpdatedDate,
                    UpdatedBy = user.UpdatedBy,
<<<<<<< HEAD
                    DepartmentName = (departmentFiltered == null ? "" : departmentFiltered.DepartmentName),
=======
                    DepartmentName = (departmentFiltered == null? "" : departmentFiltered.DepartmentName),
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
                    DepartmentNameBangla = (departmentFiltered == null ? "" : departmentFiltered.DepartmentNameBangla),
                    DesignationName = (designationFiltered == null ? "" : designationFiltered.DesignationName),
                    DesignationNameBangla = (designationFiltered == null ? "" : designationFiltered.DesignationNameBangla),
                };
            var retDataList = query.OrderBy(x => x.UserFullName).ThenBy(x => x.CreatedDate).ToList();
<<<<<<< HEAD
            return retDataList;
=======
            return await Task.FromResult(retDataList);
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
        }
    }
}

