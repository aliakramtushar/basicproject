﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using DotNet.ApplicationCore.Entities;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;

namespace DotNet.Services.Repositories.Common
{
    public class DesignationRepository : IDesignationRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        public DesignationRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
        }
        public async Task<IEnumerable<Designation>> GetAll()
        {
            var Designations = _context.Designations.ToList();
            return await Task.FromResult(Designations);
        }
        public async Task<Designation> GetByID(int id)
        {
            var result = _context.Designations.SingleOrDefault(x => x.DesignationID == id);
            return await Task.FromResult(result);
        }
        public async Task<Designation> Add(Designation Designation)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            _context.Designations.Add(Designation);
            _context.SaveChanges();

            return await GetByID(Designation.DesignationID);
        }
        public async Task<Designation> Update(Designation Designation)
        {
            var data = await GetByID(Designation.DesignationID);
            if (data == null)
            {
                throw new Exception();
            }
            data.DesignationName = Designation.DesignationName;
            data.DesignationNameBangla = Designation.DesignationNameBangla;
            data.IsActive = Designation.IsActive;
            data.OrderNo = Designation.OrderNo;

            _context.Designations.Attach(data);
            _context.Entry(data).State = EntityState.Modified;
            _context.SaveChanges();
            return data;
        }
        public async Task<bool> Delete(int DesignationID)
        {
            var data = await GetByID(DesignationID);
            if (data != null)
            {
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}

