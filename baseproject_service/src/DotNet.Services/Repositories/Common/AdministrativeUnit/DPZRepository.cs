﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using System.Diagnostics.Metrics;
using DotNet.ApplicationCore.DTOs.VM;

namespace DotNet.Services.Repositories.Common.AdministrativeUnit
{
    public class DPZRepository : IDPZRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        public DPZRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
        }

        public async Task<IEnumerable<VMDPZ>> GetAll()
        {
            var query =
                from obj in _context.DPZs.ToList()
<<<<<<< HEAD
                select new VMDPZ
                {
                    DPZID = obj.DPZID,
=======
                join thana in _context.Thanas.ToList() on obj.ThanaID equals thana.ThanaID
                join mouza in _context.Mouzas.ToList() on obj.MouzaID equals mouza.MouzaID

                select new VMDPZ
                {
                    DPZID = obj.DPZID,
                    ThanaID = obj.ThanaID,
                    MouzaID = obj.MouzaID,
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
                    DPZName = obj.DPZName,
                    DPZNameBangla = obj.DPZNameBangla,
                    SLNo = obj.SLNo,
                    IsActive = obj.IsActive,
                    ClientDPZID = obj.ClientDPZID,
<<<<<<< HEAD
=======
                    ThanaName = thana.ThanaName,
                    ThanaNameBangla = thana.ThanaNameBangla,
                    MouzaName = mouza.MouzaName,
                    MouzaNameBangla = mouza.MouzaNameBangla,
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
                };

            var retDataList = query.OrderBy(x => x.SLNo).ThenBy(x => x.DPZName).ToList();
            return await Task.FromResult(retDataList);
        }
        public async Task<VMDPZ> GetByID(int id)
        {
            var result = _context.DPZs.SingleOrDefault(x => x.DPZID == id);
            VMDPZ retObj = new VMDPZ();

            if (result != null)
            {
                retObj.DPZID = result.DPZID;
<<<<<<< HEAD
=======
                retObj.ThanaID = result.ThanaID;
                retObj.MouzaID = result.MouzaID;
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
                retObj.DPZName = result.DPZName;
                retObj.DPZNameBangla = result.DPZNameBangla;
                retObj.SLNo = result.SLNo;
                retObj.IsActive = result.IsActive;
                retObj.ClientDPZID = result.ClientDPZID;
<<<<<<< HEAD
=======

                var thana = _context.Thanas.SingleOrDefault(x => x.ThanaID == result.ThanaID);
                if (thana != null)
                {
                    retObj.ThanaName = thana.ThanaName;
                    retObj.ThanaNameBangla = thana.ThanaNameBangla;
                }
                var mouza = _context.Mouzas.SingleOrDefault(x => x.MouzaID == result.MouzaID);
                if (mouza != null)
                {
                    retObj.MouzaName = mouza.MouzaName;
                    retObj.MouzaNameBangla = mouza.MouzaNameBangla;
                }
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            }
            return await Task.FromResult(retObj);
        }
        public async Task<VMDPZ> Add(VMDPZ dpz)
        {
            var userID = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var existsData = _context.DPZs.SingleOrDefault(x => x.DPZName == dpz.DPZName);
            if (existsData != null)
            {
                throw new Exception("Data Exists with this name !");
            }

            _context.Database.BeginTransaction();
            DPZ saveDPZ = new DPZ();
            saveDPZ.DPZName = dpz.DPZName;
<<<<<<< HEAD
=======
            saveDPZ.ThanaID = dpz.ThanaID;
            saveDPZ.MouzaID = dpz.MouzaID;
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            saveDPZ.DPZName = dpz.DPZName;
            saveDPZ.DPZNameBangla = dpz.DPZNameBangla;
            saveDPZ.SLNo = dpz.SLNo;
            saveDPZ.IsActive = dpz.IsActive;
            saveDPZ.ClientDPZID = dpz.ClientDPZID;
            _context.DPZs.Add(saveDPZ);
            _context.SaveChanges();

            _context.Database.CommitTransaction();
            return await GetByID(saveDPZ.DPZID);
        }
        public async Task<VMDPZ> Update(VMDPZ DPZ)
        {
            var userID = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            int organizationID = await _httpContextAccessor.HttpContext.User.GetOrginzationIdFromClaimIdentity();

            var updateDPZ = _context.DPZs.SingleOrDefault(x => x.DPZID == DPZ.DPZID);
            if (updateDPZ == null)
            {
                throw new Exception();
            }
            var duplicateData = _context.DPZs.SingleOrDefault(x => x.DPZName == DPZ.DPZName && x.DPZID != DPZ.DPZID);
            if (duplicateData != null)
            {
                throw new Exception("Data Exists with this name !");
            }

            _context.Database.BeginTransaction();
<<<<<<< HEAD
=======
            updateDPZ.ThanaID = DPZ.ThanaID;
            updateDPZ.MouzaID = DPZ.MouzaID;
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            updateDPZ.DPZName = DPZ.DPZName;
            updateDPZ.DPZNameBangla = DPZ.DPZNameBangla;
            updateDPZ.SLNo = DPZ.SLNo;
            updateDPZ.IsActive = DPZ.IsActive;
            updateDPZ.ClientDPZID = DPZ.ClientDPZID;
            _context.DPZs.Attach(updateDPZ);
            _context.Entry(updateDPZ).State = EntityState.Modified;
            _context.SaveChanges();

            _context.Database.CommitTransaction();
            return await GetByID(updateDPZ.DPZID);
        }
        public async Task<bool> Delete(int DPZID)
        {
            var data = _context.DPZs.SingleOrDefault(x => x.DPZID == DPZID);
            if (data != null)
            {
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
        public async Task<IEnumerable<VMDPZ>> Search(QueryObject queryObject)
        {
            //var mapList = _context.DPZs.Where(x=>x.DPZName.con) Contains() ToList();
            //if (queryObject.UpazilaCityCorporationID > 0)
            //{
            //    mapList = mapList.Where(x => x.UpazilaCityCorporationID == queryObject.UpazilaCityCorporationID).ToList();
            //}

            //var query =
            //    from map in mapList
            //    join DPZ in _context.DPZs on map.DPZID equals DPZ.DPZID
            //    join UpazilaCityCorporation in _context.UpazilaCityCorporations on map.UpazilaCityCorporationID equals UpazilaCityCorporation.UpazilaCityCorporationID

            //    select new VMDPZ
            //    {
            //        DPZID = DPZ.DPZID,
            //        DPZCode = DPZ.DPZCode,
            //        DPZName = DPZ.DPZName,
            //        DPZNameBangla = DPZ.DPZNameBangla,
            //        GeoFenceID = 0,
            //        UpazilaCityCorporationDPZMap = map,
            //        UpazilaCityCorporationName = UpazilaCityCorporation.UpazilaCityCorporationName,
            //    };

            //var retDataList = query.OrderBy(x => x.OrderNo).ToList();
            var retDataList = new List<VMDPZ>();
            return await Task.FromResult(retDataList);
        }
        //public async Task<IEnumerable<VMUserDPZMap>> UserDpzMap(List<VMUserDPZMap> objList)
        //{
        //    //var mapList = _context.DPZs.Where(x=>x.DPZName.con) Contains() ToList();
        //    //if (queryObject.UpazilaCityCorporationID > 0)
        //    //{
        //    //    mapList = mapList.Where(x => x.UpazilaCityCorporationID == queryObject.UpazilaCityCorporationID).ToList();
        //    //}

        //    //var query =
        //    //    from map in mapList
        //    //    join DPZ in _context.DPZs on map.DPZID equals DPZ.DPZID
        //    //    join UpazilaCityCorporation in _context.UpazilaCityCorporations on map.UpazilaCityCorporationID equals UpazilaCityCorporation.UpazilaCityCorporationID

        //    //    select new VMDPZ
        //    //    {
        //    //        DPZID = DPZ.DPZID,
        //    //        DPZCode = DPZ.DPZCode,
        //    //        DPZName = DPZ.DPZName,
        //    //        DPZNameBangla = DPZ.DPZNameBangla,
        //    //        GeoFenceID = 0,
        //    //        UpazilaCityCorporationDPZMap = map,
        //    //        UpazilaCityCorporationName = UpazilaCityCorporation.UpazilaCityCorporationName,
        //    //    };

        //    //var retDataList = query.OrderBy(x => x.OrderNo).ToList();
        //    var retDataList = new List<VMUserDPZMap>();
        //    return await Task.FromResult(retDataList);
        //}
    }
}

