﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using System.Diagnostics.Metrics;

namespace DotNet.Services.Repositories.Common.AdministrativeUnit
{
    public class MouzaRepository : IMouzaRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        public MouzaRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
        }

        public async Task<IEnumerable<VMMouza>> GetAll()
        {
            var query =
                from obj in _context.Mouzas.ToList()
                join thana in _context.Thanas.ToList() on obj.ThanaID equals thana.ThanaID
<<<<<<< HEAD
                join dpz in _context.DPZs.ToList() on obj.DPZID equals dpz.DPZID
=======
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa

                select new VMMouza
                {
                    MouzaID = obj.MouzaID,
                    MouzaName = obj.MouzaName,
                    MouzaNameBangla = obj.MouzaNameBangla,
                    SLNo = obj.SLNo,
                    IsActive = obj.IsActive,
                    MouzaDetails = obj.MouzaDetails,
<<<<<<< HEAD
                    ThanaID = obj.ThanaID,
                    ThanaName = thana.ThanaName,
                    ThanaNameBangla = thana.ThanaNameBangla,
                    DPZID = obj.DPZID,
                    DPZName = dpz.DPZName,
                    DPZNameBangla = dpz.DPZNameBangla,
=======
                    ThanaID = thana.ThanaID,
                    ThanaName = thana.ThanaName,
                    ThanaNameBangla = thana.ThanaNameBangla,
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
                    ClientThanaID = obj.ClientThanaID,
                    ClientMouzeID = obj.ClientMouzeID,
                };

            var retDataList = query.OrderBy(x => x.SLNo).ThenBy(x => x.MouzaName).ToList();
            return await Task.FromResult(retDataList);
        }
        public async Task<VMMouza> GetByID(int id)
        {
            var result = _context.Mouzas.SingleOrDefault(x => x.MouzaID == id);
            VMMouza retObj = new VMMouza();

            if (result != null)
            {
                retObj.MouzaID = result.MouzaID;
                retObj.MouzaName = result.MouzaName;
                retObj.MouzaNameBangla = result.MouzaNameBangla;
                retObj.SLNo = result.SLNo;
                retObj.IsActive = result.IsActive;
                retObj.MouzaDetails = result.MouzaDetails;
                retObj.ThanaID = retObj.ThanaID;
                retObj.ClientThanaID = result.ClientThanaID;
                retObj.ClientMouzeID = result.ClientMouzeID;

                var thana = _context.Thanas.SingleOrDefault(x => x.ThanaID == result.ThanaID);
                if (thana != null)
                {
                    retObj.ThanaName = thana.ThanaName;
                    retObj.ThanaNameBangla = thana.ThanaNameBangla;
                }
            }
            return await Task.FromResult(retObj);
        }
        public async Task<VMMouza> Add(VMMouza mouza)
        {
            var userID = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
<<<<<<< HEAD
            var existsData = _context.Mouzas.SingleOrDefault(x => x.MouzaName.Trim() == mouza.MouzaName.Trim());
=======
            var existsData = _context.Mouzas.SingleOrDefault(x => x.MouzaName == mouza.MouzaName);
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            if (existsData != null)
            {
                throw new Exception("Data Exists with this name !");
            }

            _context.Database.BeginTransaction();
            Mouza saveMouza = new Mouza();
<<<<<<< HEAD
            saveMouza.MouzaName = mouza.MouzaName.Trim();
            saveMouza.MouzaNameBangla = mouza.MouzaNameBangla.Trim();
            saveMouza.ThanaID = mouza.ThanaID;
            saveMouza.DPZID = mouza.DPZID;
            saveMouza.SLNo = mouza.SLNo;
            saveMouza.IsActive = mouza.IsActive;
            saveMouza.MouzaDetails = mouza.MouzaDetails;
=======
            saveMouza.MouzaName = mouza.MouzaName;
            saveMouza.MouzaNameBangla = mouza.MouzaNameBangla;
            saveMouza.MouzaNameBangla = mouza.MouzaNameBangla;
            saveMouza.SLNo = mouza.SLNo;
            saveMouza.IsActive = mouza.IsActive;
            saveMouza.MouzaDetails = mouza.MouzaDetails;
            saveMouza.ThanaID = mouza.ThanaID;
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            saveMouza.ClientThanaID = mouza.ClientThanaID;
            saveMouza.ClientMouzeID = mouza.ClientMouzeID;
            _context.Mouzas.Add(saveMouza);
            _context.SaveChanges();

            _context.Database.CommitTransaction();
            return await GetByID(saveMouza.MouzaID);
        }
        public async Task<VMMouza> Update(VMMouza Mouza)
        {
            var userID = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            int organizationID = await _httpContextAccessor.HttpContext.User.GetOrginzationIdFromClaimIdentity();

            var updateMouza = _context.Mouzas.SingleOrDefault(x => x.MouzaID == Mouza.MouzaID);
            if (updateMouza == null)
            {
                throw new Exception();
            }
            var duplicateData = _context.Mouzas.SingleOrDefault(x => x.MouzaName == Mouza.MouzaName && x.MouzaID != Mouza.MouzaID);
            if (duplicateData != null)
            {
                throw new Exception("Data Exists with this name !");
            }

            _context.Database.BeginTransaction();
<<<<<<< HEAD
            updateMouza.MouzaName = Mouza.MouzaName.Trim();
            updateMouza.MouzaNameBangla = Mouza.MouzaNameBangla.Trim();
=======
            updateMouza.MouzaName = Mouza.MouzaName;
            updateMouza.MouzaNameBangla = Mouza.MouzaNameBangla;
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            updateMouza.SLNo = Mouza.SLNo;
            updateMouza.IsActive = Mouza.IsActive;
            updateMouza.MouzaDetails = Mouza.MouzaDetails;
            updateMouza.ThanaID = Mouza.ThanaID;
            updateMouza.ClientThanaID = Mouza.ClientThanaID;
            updateMouza.MouzaID = Mouza.MouzaID;
            _context.Mouzas.Attach(updateMouza);
            _context.Entry(updateMouza).State = EntityState.Modified;
            _context.SaveChanges();

            _context.Database.CommitTransaction();
            return await GetByID(updateMouza.MouzaID);
        }
        public async Task<bool> Delete(int MouzaID)
        {
            var data = _context.Mouzas.SingleOrDefault(x => x.MouzaID == MouzaID);
            if (data != null)
            {
<<<<<<< HEAD
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;
=======
                var lstDPZ = _context.DPZs.Where(x => x.MouzaID == data.MouzaID).ToList();
                if(lstDPZ.Count == 0)
                {
                    _context.Entry(data).State = EntityState.Deleted;
                    _context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            }
            return false;
        }
        public async Task<IEnumerable<VMMouza>> Search(QueryObject queryObject)
        {
            //var mapList = _context.Mouzas.Where(x=>x.MouzaName.con) Contains() ToList();
            //if (queryObject.UpazilaCityCorporationID > 0)
            //{
            //    mapList = mapList.Where(x => x.UpazilaCityCorporationID == queryObject.UpazilaCityCorporationID).ToList();
            //}

            //var query =
            //    from map in mapList
            //    join Mouza in _context.Mouzas on map.MouzaID equals Mouza.MouzaID
            //    join UpazilaCityCorporation in _context.UpazilaCityCorporations on map.UpazilaCityCorporationID equals UpazilaCityCorporation.UpazilaCityCorporationID

            //    select new VMMouza
            //    {
            //        MouzaID = Mouza.MouzaID,
            //        MouzaCode = Mouza.MouzaCode,
            //        MouzaName = Mouza.MouzaName,
            //        MouzaNameBangla = Mouza.MouzaNameBangla,
            //        GeoFenceID = 0,
            //        UpazilaCityCorporationMouzaMap = map,
            //        UpazilaCityCorporationName = UpazilaCityCorporation.UpazilaCityCorporationName,
            //    };

            //var retDataList = query.OrderBy(x => x.OrderNo).ToList();
            var retDataList = new List<VMMouza>();
            return await Task.FromResult(retDataList);
        }
    }
}

