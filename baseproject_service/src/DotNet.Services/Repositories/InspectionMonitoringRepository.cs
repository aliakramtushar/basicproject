﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using DotNet.ApplicationCore.Entities;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using DotNet.ApplicationCore.Entities.CDA;

namespace DotNet.Services.Repositories.Common
{
    //IGenericRepository<organization>,
    public class InspectionMonitoringRepository : IInspectionMonitoringRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        public InspectionMonitoringRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
        }


        public async Task<IEnumerable<VMApplicationFileMaster>> Search(QueryObject queryObject)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            int organizationID = await _httpContextAccessor.HttpContext.User.GetOrginzationIdFromClaimIdentity();

            List<VMApplicationFileMaster> lstApplicationFileMaster = new List<VMApplicationFileMaster>();
            List<Thana> lstThana = _context.Thanas.ToList();
            List<Mouza> lstMouza = _context.Mouzas.ToList();
            List<DPZ> lstDPZ = _context.DPZs.ToList();
            List<Users> lstUsers = _context.Users.Where(x => x.OrganizationID == organizationID).ToList();
            List<ApplicationFileMaster> lsAppFileMaster = _context.ApplicationFileMasters.ToList();

            var array = Enum.GetValues(typeof(ApplicationType)).Cast<ApplicationType>();
            var lstApplicationTypes = array.Select(a => new
            {
                ApplicationTypeID = Convert.ToInt32(a),
                ApplicationTypeName = a.ToString()
            }).ToList();

            if (!string.IsNullOrEmpty(queryObject.ReferenceNo))
            {
                lsAppFileMaster = lsAppFileMaster.Where(x => x.RefNo.Trim() == queryObject.ReferenceNo.Trim()).ToList();
            }
            else 
            {
                if (queryObject.FromDate != null || queryObject.ToDate != null)
                {
                    string fromDate = queryObject.FromDate.ToString("yyyy/MM/dd") + " 00:00:00";
                    string toDate = queryObject.ToDate.ToString("yyyy/MM/dd") + " 23:59:59";

                    lsAppFileMaster = lsAppFileMaster.Where(x => x.ApprovalDate >= Convert.ToDateTime(fromDate) && x.ApprovalDate <= Convert.ToDateTime(toDate)).ToList();
                }
                
                if (queryObject.DepartmentID > 0)
                {
                    lstUsers = lstUsers.Where(x => x.DepartmentID == queryObject.DepartmentID).ToList();
                }
                if (queryObject.ThanaID > 0)
                {
                    lstThana = lstThana.Where(x => x.ThanaID == queryObject.ThanaID).ToList();
                }
                if (queryObject.MouzaID > 0)
                {
                    lstMouza = lstMouza.Where(x => x.MouzaID == queryObject.MouzaID).ToList();
                }
                if (queryObject.DPZID > 0)
                {
                    lstDPZ = lstDPZ.Where(x => x.DPZID == queryObject.DPZID).ToList();
                }
                if (queryObject.ApplicationType > 0)
                {
                    lstApplicationTypes = lstApplicationTypes.Where(x => x.ApplicationTypeID == queryObject.ApplicationType).ToList();
                }
                if (queryObject.UserID > 0)
                {
                    lstUsers = lstUsers.Where(x => x.UserAutoID == queryObject.UserID).ToList();
                }

                if (queryObject.VisitType == 0)
                {
                    lsAppFileMaster = lsAppFileMaster.Where(x => x.IsVisited == false).ToList();
                }
                else if (queryObject.VisitType == 1)
                {
                    lsAppFileMaster = lsAppFileMaster.Where(x => x.IsVisited == true).ToList();
                }
            }




            var query =
               from fileMaster in lsAppFileMaster
               join thana in lstThana on fileMaster.ThanaID equals thana.ThanaID
               join mouza in lstMouza on fileMaster.MouzaID equals mouza.MouzaID
               join dpz in lstDPZ on fileMaster.DPZID equals dpz.DPZID
               //join appType in lstApplicationTypes on fileMaster.ApplicationType equals appType.ApplicationTypeID
               join fileUserMaps in _context.ApplicationFileUserMaps on fileMaster.ApplicationFileMasterID equals fileUserMaps.ApplicationFileMasterID
               join user in lstUsers on fileUserMaps.UserID equals user.UserAutoID

               select new VMApplicationFileMaster
               {
                   ApplicationFileMasterID = fileMaster.ApplicationFileMasterID,
                   RefNo = fileMaster.RefNo,
                   ApplicantName = fileMaster.ApplicantName,
                   ApplicationType = fileMaster.ApplicationType,
                   ApprovalDate = fileMaster.ApprovalDate,
                   RSNo = fileMaster.RSNo,
                   BSNo = fileMaster.BSNo,
                   ThanaID = fileMaster.ThanaID,
                   MouzaID = fileMaster.MouzaID,
                   DPZID = fileMaster.DPZID,
                   Road = fileMaster.Road,
                   IsVisited = fileMaster.IsVisited,
                   Latitude = fileMaster.Latitude,
                   Longitude = fileMaster.Longitude,
                   //CreatedBy = fileMaster.CreatedBy,
                   //UpdatedBy = fileMaster.UpdatedBy,
                   //CreatedDate = fileMaster.CreatedDate,
                   //UpdatedDate = fileMaster.UpdatedDate,
                   MouzaName = mouza.MouzaName,
                   DPZName = dpz.DPZName,
                   ThanaName = thana.ThanaName
               };
            lstApplicationFileMaster = query.ToList();
            return await Task.FromResult(lstApplicationFileMaster);
        }
    }
}

