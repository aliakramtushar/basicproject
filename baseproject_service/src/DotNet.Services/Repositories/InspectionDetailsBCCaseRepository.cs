﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using System.Net.Mail;
using OfficeOpenXml;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using System.Globalization;
using Microsoft.AspNetCore.Http.Internal;
using System.Drawing;
using DotNet.ApplicationCore.DTOs.VM;
using Microsoft.Extensions.Configuration;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.ApplicationCore.Entities;

namespace DotNet.Services.Repositories
{
    public class InspectionDetailsBCCaseRepository : IInspectionDetailsBCCaseRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public InspectionDetailsBCCaseRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper,
            IConfiguration configuration
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
            _configuration = configuration;
        }
        public async Task<IEnumerable<VMInspectionDetailsBCCase>> GetAll()
        {
            var sql =
                    from appMigration in _context.InspectionDetailsBCCases.ToList()
                    select new VMInspectionDetailsBCCase
                    {
                        InspectionDetailsBCCaseID = appMigration.InspectionDetailsBCCaseID,
                        CreatedBy = appMigration.CreatedBy,
                        CreatedDate = appMigration.CreatedDate,
                        UpdatedBy = appMigration.UpdatedBy,
                        UpdatedDate = appMigration.UpdatedDate,
                    };
            List<VMInspectionDetailsBCCase> retData = sql.ToList();

            return await Task.FromResult(retData);
        }
        public async Task<InspectionDetailsBCCase> GetByID(int id)
        {
            var result = _context.InspectionDetailsBCCases.SingleOrDefault(x => x.InspectionDetailsBCCaseID == id);
            return await Task.FromResult(result);
        }
        public async Task<InspectionDetailsBCCase> GetByFileMasterID(int id)
        {
            var result = _context.InspectionDetailsBCCases.SingleOrDefault(x => x.ApplicationFileMasterID == id);
            return await Task.FromResult(result);
        }

        public async Task<ResponseMessage> Add(InspectionDetailsBCCase inspectionDetailsBCCase)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            ResponseMessage responseMessage = new ResponseMessage();
            try
            {
                inspectionDetailsBCCase.CreatedBy = userId;
                inspectionDetailsBCCase.CreatedDate = DateTime.Now;
                inspectionDetailsBCCase.UpdatedBy = userId;
                inspectionDetailsBCCase.UpdatedDate = DateTime.Now;

                _context.InspectionDetailsBCCases.Add(inspectionDetailsBCCase);
                _context.SaveChanges();
                responseMessage.StatusCode = ReturnStatus.Success;
                responseMessage.ResponseObj = await GetByID(inspectionDetailsBCCase.InspectionDetailsBCCaseID);
            }
            catch (Exception ex)
            {
                responseMessage.Message = ex.Message;
                responseMessage.StatusCode = ReturnStatus.Failed;
                responseMessage.ResponseObj = await GetByID(inspectionDetailsBCCase.InspectionDetailsBCCaseID);
            }
            return responseMessage;
        }
        public async Task<ResponseMessage> Update(InspectionDetailsBCCase inspectionDetailsBCCase)
        {
            ResponseMessage responseMessage = new ResponseMessage();
            var data = await GetByID(inspectionDetailsBCCase.InspectionDetailsBCCaseID);
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();

            try
            {
                if (data == null)
                {
                    throw new Exception();
                }

                data.LandOwnerName = inspectionDetailsBCCase.LandOwnerName;
                data.IsLandUnderLease = inspectionDetailsBCCase.IsLandUnderLease;
                data.LeaseYear = inspectionDetailsBCCase.LeaseYear;
                data.ActualProposedSizeNorth = inspectionDetailsBCCase.ActualProposedSizeNorth;
                data.ActualProposedSizeSouth = inspectionDetailsBCCase.ActualProposedSizeSouth;
                data.ActualProposedSizeEast = inspectionDetailsBCCase.ActualProposedSizeEast;
                data.ActualProposedSizeWest = inspectionDetailsBCCase.ActualProposedSizeWest;
                data.ActualProposedSizeLandQty = inspectionDetailsBCCase.ActualProposedSizeLandQty;
                data.IsApplicantLandOwner = inspectionDetailsBCCase.IsApplicantLandOwner;
                data.AsceticOfPlace = inspectionDetailsBCCase.AsceticOfPlace;
                data.RSKhotian = inspectionDetailsBCCase.RSKhotian;
                data.LayOutPlotNo = inspectionDetailsBCCase.LayOutPlotNo;
                data.LandArea = inspectionDetailsBCCase.LandArea;
                data.ProposedBuldingEnvelop1stFloor = inspectionDetailsBCCase.ProposedBuldingEnvelop1stFloor;
                data.ProposedBuldingEnvelopFrom2ndFloor = inspectionDetailsBCCase.ProposedBuldingEnvelopFrom2ndFloor;
                data.ProposedBuldingEnvelopTillFloor = inspectionDetailsBCCase.ProposedBuldingEnvelopTillFloor;
                data.ProposedBuldingEnvelopStair = inspectionDetailsBCCase.ProposedBuldingEnvelopStair;
                data.ProposedBuldingEnvelopTotal = inspectionDetailsBCCase.ProposedBuldingEnvelopTotal;
                data.ProposedBuldingEnvelopParkingSpace = inspectionDetailsBCCase.ProposedBuldingEnvelopParkingSpace;
                data.IsAnyExistingBuilding = inspectionDetailsBCCase.IsAnyExistingBuilding;
                data.ExistingBuildingNature = inspectionDetailsBCCase.ExistingBuildingNature;
                data.ExistingBuildingSize = inspectionDetailsBCCase.ExistingBuildingSize;
                data.StructureToProsture = inspectionDetailsBCCase.StructureToProsture;
                data.IsNearbyOpenSpaceOk = inspectionDetailsBCCase.IsNearbyOpenSpaceOk;
                data.ConnectedRoadName = inspectionDetailsBCCase.ConnectedRoadName;
                data.ConnectedRoadWidth = inspectionDetailsBCCase.ConnectedRoadWidth;
                data.ConnectedRoadDirection = inspectionDetailsBCCase.ConnectedRoadDirection;
                data.UpdatedBy = userId;
                data.UpdatedDate = DateTime.Now;

                _context.InspectionDetailsBCCases.Attach(data);
                _context.Entry(data).State = EntityState.Modified;
                _context.SaveChanges();
                responseMessage = new ResponseMessage();
                responseMessage.StatusCode = ReturnStatus.Success;
                responseMessage.ResponseObj = await GetByID(inspectionDetailsBCCase.InspectionDetailsBCCaseID);
            }
            catch (Exception ex)
            {
                responseMessage.Message = ex.Message;
                responseMessage.StatusCode = ReturnStatus.Failed;
                responseMessage.ResponseObj = await GetByID(inspectionDetailsBCCase.InspectionDetailsBCCaseID);
            }
            return responseMessage;
        }
        public async Task<bool> Delete(int InspectionDetailsBCCaseID)
        {
            var data = await GetByID(InspectionDetailsBCCaseID);
            if (data != null)
            {
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}

