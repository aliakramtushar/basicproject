﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using System.Net.Mail;
using OfficeOpenXml;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using System.Globalization;
using Microsoft.AspNetCore.Http.Internal;
using System.Drawing;
using DotNet.ApplicationCore.DTOs.VM;
using Microsoft.Extensions.Configuration;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.CDA;

namespace DotNet.Services.Repositories
{
    public class ApplicationFileUserMapRepository : IApplicationFileUserMapRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public ApplicationFileUserMapRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper,
            IConfiguration configuration
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
            _configuration = configuration;
        }
        public async Task<IEnumerable<VMApplicationFileUserMap>> GetAll()
        {
            List<ApplicationFileUserMap> dataList = new List<ApplicationFileUserMap>();

            dataList = _context.ApplicationFileUserMaps.ToList();
            var sql =
                    (from appMigration in dataList

                     select new VMApplicationFileUserMap
                     {
                         ApplicationFileUserMapID = appMigration.ApplicationFileUserMapID,
                         ApplicationFileMasterID = appMigration.ApplicationFileMasterID
                     });
            List<VMApplicationFileUserMap> retData = sql.ToList();
            return await Task.FromResult(retData);
        }
        public async Task<ApplicationFileUserMap> GetByID(int id)
        {
            var result = _context.ApplicationFileUserMaps.SingleOrDefault(x => x.ApplicationFileUserMapID == id);
            return await Task.FromResult(result);
        }
        public async Task<ApplicationFileUserMap> Add(ApplicationFileUserMap applicationFileUserMap)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var existingData = _context.ApplicationFileUserMaps.SingleOrDefault(x => x.UserID == applicationFileUserMap.UserID && x.ApplicationFileMasterID == applicationFileUserMap.ApplicationFileMasterID && x.IsActive == true);
            if(existingData == null)
            {
                if(applicationFileUserMap.ApplicationFileMasterID == 0)
                {
                    throw new Exception("Please select any file");
                }
                if (applicationFileUserMap.UserID == 0)
                {
                    throw new Exception("Please select any user");
                }
                applicationFileUserMap.CreatedDate = DateTime.UtcNow;
                applicationFileUserMap.AssignBy = userId;
                _context.ApplicationFileUserMaps.Add(applicationFileUserMap);
                _context.SaveChanges();
            }
            else
            {
                throw new Exception("Data exists");
            }
            return await GetByID(applicationFileUserMap.ApplicationFileUserMapID);
        }
        public async Task<ApplicationFileUserMap> Update(ApplicationFileUserMap applicationFileUserMap)
        {
            var userId = await _httpContextAccessor.HttpContext.User.GetUserAutoIdFromClaimIdentity();
            var data = await GetByID(applicationFileUserMap.ApplicationFileUserMapID);
            if (data == null)
            {
                throw new Exception();
            }
            if (data.ApplicationFileMasterID > 0)
            {
                throw new Exception("You cant edit this");
            }

            if (applicationFileUserMap.ApplicationFileMasterID == 0)
            {
                throw new Exception("Please select any file");
            }
            if (applicationFileUserMap.UserID == 0)
            {
                throw new Exception("Please select any user");
            }
            //data.UserID = ApplicationFileUserMap.UserID;
            data.IsActive = applicationFileUserMap.IsActive;
            data.AssignDate = applicationFileUserMap.AssignDate;
            data.TargetDate = applicationFileUserMap.TargetDate;

            _context.ApplicationFileUserMaps.Attach(data);
            _context.Entry(data).State = EntityState.Modified;
            _context.SaveChanges();
            return data;
        }
        public async Task<bool> Delete(int ApplicationFileUserMapID)
        {
            var data = await GetByID(ApplicationFileUserMapID);
            if (data != null)
            {
                _context.Entry(data).State = EntityState.Deleted;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
        public async Task<IEnumerable<VMApplicationFileUserMap>> GetAssingedUserListByMasterFileID(int fileID)
        {
            List<ApplicationFileUserMap> dataList = new List<ApplicationFileUserMap>();
            dataList = _context.ApplicationFileUserMaps.Where(x=>x.ApplicationFileMasterID == fileID).ToList();
            var sql =
                    from map in dataList
                    join user in _context.Users on map.UserID equals user.UserAutoID
                    join user_created in _context.Users on map.AssignBy equals user_created.UserAutoID

                    select new VMApplicationFileUserMap
                     {
                         ApplicationFileUserMapID = map.ApplicationFileUserMapID,
                         ApplicationFileMasterID = map.ApplicationFileMasterID,
                         UserID = map.UserID,
                         IsActive = map.IsActive,
                         AssignDate = map.AssignDate,
                         TargetDate = map.TargetDate,
                         AssignBy = map.AssignBy,
                         CreatedDate = map.CreatedDate,
                         UserName = user.UserFullName,
                         AssignByName = user_created.UserFullName,
                     };
            List<VMApplicationFileUserMap> retData = sql.OrderByDescending(x=>x.CreatedDate).ThenByDescending(x=>x.IsActive).ToList();
            return await Task.FromResult(retData);
        }
    }
}

