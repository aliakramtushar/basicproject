﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.Services.Repositories.Infrastructure;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;

namespace DotNet.Services.Repositories.Interfaces
{

    public interface IInspectionDetailsBCCaseRepository //: ICommonRepository<InspectionDetailsBCCase>
    {
        Task<IEnumerable<VMInspectionDetailsBCCase>> GetAll();
        Task<InspectionDetailsBCCase> GetByID(int id);
        Task<InspectionDetailsBCCase> GetByFileMasterID(int id);
        Task<ResponseMessage> Add(InspectionDetailsBCCase entity);
        Task<ResponseMessage> Update(InspectionDetailsBCCase entity);
        Task<bool> Delete(int id);
    }
}
