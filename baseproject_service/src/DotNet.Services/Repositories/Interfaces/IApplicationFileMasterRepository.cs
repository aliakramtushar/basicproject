﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.Services.Repositories.Infrastructure;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;

namespace DotNet.Services.Repositories.Interfaces
{

    public interface IApplicationFileMasterRepository //: ICommonRepository<ApplicationFileMaster>
    {
        Task<IEnumerable<VMApplicationFileMaster>> GetAll();
        Task<VMApplicationFileMaster> GetByID(int id);
        Task<ApplicationFileMaster> Add(ApplicationFileMaster entity);
        Task<ApplicationFileMaster> Update(ApplicationFileMaster entity);
        Task<bool> Delete(int id);
        Task<ResponseMessage> MigrateData(List<ApplicationMigration> lstData);
        Task<ResponseMessage> GetListByAssingedPerson(QueryObject oQueryObject);
        Task<ResponseMessage> UpdateApplicationFileGeofence(QueryObject oQueryObject);
        Task<ResponseMessage> SaveApplicationFileAttachments(IFormCollection request, int applicationFileMasterID, int attachmentType);
    }
}
