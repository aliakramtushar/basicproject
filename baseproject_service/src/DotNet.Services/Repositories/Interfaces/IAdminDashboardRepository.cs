﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.Services.Repositories.Infrastructure;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;

namespace DotNet.Services.Repositories.Interfaces
{

    public interface IAdminDashboardRepository //: ICommonRepository<AdminDashboard>
    {
        Task<VMAdminDashboard> GetAdminDashboard();
        Task<List<VMAdminDashboardFileUserWise>> GetFileListByFileType(int type);
        Task<List<VMAdminDashboardFileListUserWise>> GetFileListByTypeVisitedUser(QueryObject queryObject);
    }
}
