﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using DotNet.ApplicationCore.DTOs.VM;

namespace DotNet.Services.Repositories.Interfaces.Common
{

    public interface IUserDPZMapRepository
    {
        Task<IEnumerable<VMUserDPZMap>> GetAll();
        Task<VMUserDPZMap> GetByID(int id);
        Task<VMUserDPZMap> Add(VMUserDPZMap entity);
        Task<VMUserDPZMap> Update(VMUserDPZMap entity);
        Task<bool> Delete(int id);
    }
}
