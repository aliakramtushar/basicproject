﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Infrastructure;

namespace DotNet.Services.Repositories.Interfaces.Common
{

    public interface IDepartmentRepository //: ICommonRepository<Department>
    {
        Task<IEnumerable<Department>> GetAll();
        Task<Department> GetByID(int id);
        Task<Department> Add(Department entity);
        Task<Department> Update(Department entity);
        Task<bool> Delete(int id);
        Task<IEnumerable<Department>> UpdateOrder(List<Department> oList);
<<<<<<< HEAD
        Task<IEnumerable<Department>> GetListByOrganizationID(int id);
=======
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
    }
}
