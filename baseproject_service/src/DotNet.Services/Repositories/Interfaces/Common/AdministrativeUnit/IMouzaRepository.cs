﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;

namespace DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit
{

    public interface IMouzaRepository
    {
        Task<IEnumerable<VMMouza>> GetAll();
        Task<VMMouza> GetByID(int id);
        Task<VMMouza> Add(VMMouza entity);
        Task<VMMouza> Update(VMMouza entity);
        Task<bool> Delete(int id);
        Task<IEnumerable<VMMouza>> Search(QueryObject queryObject);
    }
}
