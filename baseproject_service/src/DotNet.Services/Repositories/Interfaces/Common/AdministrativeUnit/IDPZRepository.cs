﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using DotNet.ApplicationCore.DTOs.VM;

namespace DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit
{

    public interface IDPZRepository
    {
        Task<IEnumerable<VMDPZ>> GetAll();
        Task<VMDPZ> GetByID(int id);
        Task<VMDPZ> Add(VMDPZ entity);
        Task<VMDPZ> Update(VMDPZ entity);
        Task<bool> Delete(int id);
        Task<IEnumerable<VMDPZ>> Search(QueryObject queryObject);
        //Task<IEnumerable<VMUserDPZMap>> UserDpzMap(List<VMUserDPZMap> objList);
    }
}
