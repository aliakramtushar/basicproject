﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Infrastructure;

namespace DotNet.Services.Repositories.Interfaces.Common
{

    public interface IDesignationRepository //: ICommonRepository<Designation>
    {
        Task<IEnumerable<Designation>> GetAll();
        Task<Designation> GetByID(int id);
        Task<Designation> Add(Designation entity);
        Task<Designation> Update(Designation entity);
        Task<bool> Delete(int id);
    }
}
