﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.ApplicationCore.DTOs.VM;

namespace DotNet.Services.Repositories.Interfaces.Common
{

    //: IGenericRepository<Users>
    public interface IUserRepository //: ICommonRepository<Users>
    {
        AuthUser UserAuthentication(AuthUser userResponse);
        Task<IEnumerable<Users>> GetAll();
        Task<Users> GetByID(int id);
        Task<Users> Add(Users entity);
        Task<Users> Update(Users entity);
        Task<bool> Delete(int id);
        Task<IEnumerable<VMUsers>> GetAllByOrganizationID();
<<<<<<< HEAD
        Task<IEnumerable<VMUsers>> GetAllByDepartmentID(int id);
        Task<IEnumerable<VMUsers>> GetAllByLoginUserDepartmentID();

=======
        //Task<IEnumerable<Users>> GetDivisionListByOrganization(int id);
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
    }
}
