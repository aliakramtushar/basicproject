﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.Services.Repositories.Infrastructure;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;

namespace DotNet.Services.Repositories.Interfaces
{

    public interface IApplicationFileUserMapRepository //: ICommonRepository<ApplicationFileUserMap>
    {
        Task<IEnumerable<VMApplicationFileUserMap>> GetAll();
        Task<ApplicationFileUserMap> GetByID(int id);
        Task<ApplicationFileUserMap> Add(ApplicationFileUserMap entity);
        Task<ApplicationFileUserMap> Update(ApplicationFileUserMap entity);
        Task<bool> Delete(int id);
        Task<IEnumerable<VMApplicationFileUserMap>> GetAssingedUserListByMasterFileID(int fileID);

        
    }
}
