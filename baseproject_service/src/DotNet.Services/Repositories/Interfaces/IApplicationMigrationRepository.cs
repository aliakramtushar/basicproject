﻿using DotNet.ApplicationCore.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DotNet.Services.Repositories.Infrastructure;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;

namespace DotNet.Services.Repositories.Interfaces
{

    public interface IApplicationMigrationRepository //: ICommonRepository<ApplicationMigration>
    {
        Task<IEnumerable<VMApplicationMigration>> GetAll(int type);
        Task<ApplicationMigration> GetByID(int id);
        Task<ApplicationMigration> Add(ApplicationMigration entity);
        Task<ApplicationMigration> Update(ApplicationMigration entity);
        Task<bool> Delete(int id);
        Task<ResponseMessage> ExcelUpload(VMAttachments attachment);
    }
}
