﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;

namespace DotNet.Services.Repositories.Interfaces.Common
{

    public interface IInspectionMonitoringRepository //: ICommonRepository<UserRole>
    {
        Task<IEnumerable<VMApplicationFileMaster>> Search(QueryObject queryObject);
    }
}
