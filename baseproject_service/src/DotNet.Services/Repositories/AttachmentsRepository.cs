﻿using DotNet.ApplicationCore.DTOs;
using Microsoft.AspNetCore.Http;
using DotNet.ApplicationCore.Utils.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Infrastructure.Persistence.Contexts;
using AutoMapper;
using Microsoft.Extensions.Logging;
using DotNet.Services.Repositories.Interfaces.Common;
using DotNet.Services.Repositories.Infrastructure;
using DotNet.Services.Repositories.Interfaces;
using DotNet.ApplicationCore.Utils.Enum;
using Microsoft.EntityFrameworkCore;
using DotNet.ApplicationCore.Entities.AdministrativeUnit;
using System.Net.Mail;
using OfficeOpenXml;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;
using System.Globalization;
using Microsoft.AspNetCore.Http.Internal;
using System.Drawing;
using DotNet.ApplicationCore.DTOs.VM;
using Microsoft.Extensions.Configuration;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.Entities.CDA;
using System.Xml.Linq;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using DotNet.Services.Repositories.Interfaces.Common.AdministrativeUnit;
using DotNet.Services.Repositories.Common.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using DotNet.Infrastructure.Persistence;
using System.Data.SqlClient;
using System.Data.Entity;
using System.ComponentModel;


namespace DotNet.Services.Repositories
{
    public class AttachmentsRepository : IAttachmentsRepository
    {
        public DotNetContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;


        public AttachmentsRepository(
            DotNetContext context,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper,
            IConfiguration configuration
            )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
            _configuration = configuration;
        }
        public async Task<ResponseMessage> GetAttachmentListByFileID(int id)
        {
            ResponseMessage responseMessage = new ResponseMessage();
            var fileShowPath = _configuration.GetSection("FilePath:fileShowPath");

            List<Attachments> lstAttachments = _context.Attachments.Where(x => x.ReferenceID == id).ToList();
            foreach (Attachments attachment in lstAttachments)
            {
                attachment.AttachmentLink = fileShowPath.Value + "/" + attachment.AttachmentLink + "/" + attachment.AttachmentName;
            }
            responseMessage.ResponseObj = lstAttachments;
            responseMessage.StatusCode = ReturnStatus.Success;
            return await Task.FromResult(responseMessage);
        }
    }
}

