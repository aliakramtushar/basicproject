﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;

namespace DotNet.ApplicationCore.Entities
{
    [Table("Departments", Schema = "core")]
    public class Department
    {
        [Key]
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentNameBangla { get; set; }
        public string ShortName { get; set; }
        public bool IsActive { get; set; }
        public int ParentDepartmentID { get; set; }
        public int OrganizationID { get; set; }
        public int? OrderNo { get; set; }
    }
}
