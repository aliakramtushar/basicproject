﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Policy;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;

namespace DotNet.ApplicationCore.Entities.AdministrativeUnit
{
    [Table("Mouzas", Schema = "com")]
    public class Mouza
    {
        [Key]
        public int MouzaID { get; set; }
        public string MouzaName { get; set; }
        public string MouzaNameBangla { get; set; }
<<<<<<< HEAD
        public int ThanaID { get; set; }
        public int DPZID { get; set; }
        public int SLNo { get; set; }
        public bool IsActive { get; set; }
        public string MouzaDetails { get; set; }
=======
        public int SLNo { get; set; }
        public bool IsActive { get; set; }
        public string MouzaDetails { get; set; }
        public int ThanaID { get; set; }
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
        public int? ClientThanaID { get; set; }
        public int? ClientMouzeID { get; set; }
    }
}