﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Policy;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;

namespace DotNet.ApplicationCore.Entities.AdministrativeUnit
{
    [Table("DPZs", Schema = "com")]
    public class DPZ
    {
        [Key]
        public int DPZID { get; set; }
<<<<<<< HEAD
=======
        public int ThanaID { get; set; }
        public int MouzaID { get; set; }
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
        public string DPZName { get; set; }
        public string DPZNameBangla { get; set; }
        public int SLNo { get; set; }
        public bool IsActive { get; set; }
        public int? ClientDPZID { get; set; }
    }
}