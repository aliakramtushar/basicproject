﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;

namespace DotNet.ApplicationCore.Entities.CDA
{
    [Table("ApplicationFileUserMaps", Schema = "cda")]
    public class ApplicationFileUserMap
    {
        [Key]
        public int ApplicationFileUserMapID { get; set; }
        public int ApplicationFileMasterID { get; set; }
        public int UserID { get; set; }
        public bool IsActive { get; set; }
        public DateTime AssignDate { get; set; }
        public DateTime TargetDate { get; set; }
        public int AssignBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}