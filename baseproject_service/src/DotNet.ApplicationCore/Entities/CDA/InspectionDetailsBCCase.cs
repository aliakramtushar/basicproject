﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;

namespace DotNet.ApplicationCore.Entities.CDA
{
    [Table("InspectionDetailsBCCase", Schema = "cda")]
    public class InspectionDetailsBCCase
    {
        [Key]
        public int InspectionDetailsBCCaseID { get; set; }
        public int ApplicationFileMasterID { get; set; }
        public string LandOwnerName { get; set; }
        public bool IsLandUnderLease { get; set; }
        public int? LeaseYear { get; set; }
        public string ActualProposedSizeNorth { get; set; }
        public string ActualProposedSizeSouth { get; set; }
        public string ActualProposedSizeEast { get; set; }
        public string ActualProposedSizeWest { get; set; }
        public string ActualProposedSizeLandQty { get; set; }
        public bool IsApplicantLandOwner { get; set; }
        public string AsceticOfPlace { get; set; }
        public string RSKhotian { get; set; }
        public string LayOutPlotNo { get; set; }
        public string LandArea { get; set; }
        public string ProposedBuldingEnvelop1stFloor { get; set; }
        public string ProposedBuldingEnvelopFrom2ndFloor { get; set; }
        public string ProposedBuldingEnvelopTillFloor { get; set; }
        public string ProposedBuldingEnvelopStair { get; set; }
        public string ProposedBuldingEnvelopTotal { get; set; }
        public string ProposedBuldingEnvelopParkingSpace { get; set; }
        public bool IsAnyExistingBuilding { get; set; }
        public string ExistingBuildingNature { get; set; }
        public string ExistingBuildingSize { get; set; }
        public string StructureToProsture { get; set; }
        public bool IsNearbyOpenSpaceOk { get; set; }
        public string ConnectedRoadName { get; set; }
        public string ConnectedRoadWidth { get; set; }
        public string ConnectedRoadDirection { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
