﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNet.ApplicationCore.DTOs.Common
{
    public class FilePath
    {
        public string FileSavePath { get; set; }
        public string FileGetPath { get; set; }
    }
}
