﻿using System;
using System.Collections.Generic;
using System.Text;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;

namespace DotNet.ApplicationCore.DTOs.VM
{
    public class VMApplicationFileUserMap
    {
        public int ApplicationFileUserMapID { get; set; }
        public int ApplicationFileMasterID { get; set; }
        public int UserID { get; set; }
        public bool IsActive { get; set; }
        public DateTime AssignDate { get; set; }
        public DateTime TargetDate { get; set; }
        public int AssignBy { get; set; }
        public string UserName { get; set; }
        public string AssignByName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
