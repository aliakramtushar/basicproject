﻿using System;
using System.Collections.Generic;
using System.Text;
using static DotNet.ApplicationCore.Utils.Enum.GlobalEnum;

namespace DotNet.ApplicationCore.DTOs.VM
{
    public class VMApplicationMigration
    {
        public int ApplicationMigrationID { get; set; }
        public int ApplicationFileMasterID { get; set; }
        public string RefNo { get; set; }
        public string ApplicantName { get; set; }
        public DateTime ApprovalDate { get; set; }
        public ApplicationType ApplicationType { get; set; }
        public string RSNo { get; set; }
        public string BSNo { get; set; }
        public int ThanaID { get; set; }
        public int MouzaID { get; set; }
        public string Road { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ApprovalDateSt
        {
            get
            {
                return this.ApprovalDate.ToString("dd-MM-yyyy");
            }
        }
        public string ThanaName { get; set; }
        public string MouzaName { get; set; }
        public string ApplicationTypeSt 
        {
            get
            {
                return this.ApplicationType.ToString();
            }
        }
        public bool IsMigrated
        {
            get
            {
                return this.ApplicationFileMasterID > 0 ? true : false;
            }
        }
    }
}
