﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.Services.Services;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http.Description;


namespace DotNet.WebApi.Controllers.Common
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InspectionDetailsBCCaseController : ControllerBase
    {
        private readonly IInspectionDetailsBCCaseService _InspectionDetailsBCCaseService;
        private readonly ILogger<InspectionDetailsBCCaseController> _logger;

        public InspectionDetailsBCCaseController(IInspectionDetailsBCCaseService InspectionDetailsBCCaseService, ILogger<InspectionDetailsBCCaseController> logger)
        {
            _InspectionDetailsBCCaseService = InspectionDetailsBCCaseService;
            this._logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var response = await _InspectionDetailsBCCaseService.GetAll();
            return Ok(response);
        }

        [HttpGet]
        [Route("getByID/{id}")]
        public async Task<IActionResult> GetByID(int id)
        {
            var response = await _InspectionDetailsBCCaseService.GetByID(id);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestMessage rm)
        {
            var InspectionDetailsBCCase = JsonConvert.DeserializeObject<InspectionDetailsBCCase>(rm.RequestObj.ToString());
            var response = await _InspectionDetailsBCCaseService.Add(InspectionDetailsBCCase);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put(RequestMessage rm)
        {
            var InspectionDetailsBCCase = JsonConvert.DeserializeObject<InspectionDetailsBCCase>(rm.RequestObj.ToString());
            var response = await _InspectionDetailsBCCaseService.Update(InspectionDetailsBCCase);
            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _InspectionDetailsBCCaseService.Delete(id);
            return Ok(response);
        }
    }
}
