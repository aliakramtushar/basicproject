﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.Services.Services;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http.Description;


namespace DotNet.WebApi.Controllers.Common
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminDashboardController : ControllerBase
    {
        private readonly IAdminDashboardService _AdminDashboardService;
        private readonly ILogger<AdminDashboardController> _logger;

        public AdminDashboardController(IAdminDashboardService AdminDashboardService, ILogger<AdminDashboardController> logger)
        {
            _AdminDashboardService = AdminDashboardService;
            this._logger = logger;
        }

        [HttpGet]
        [Route("getAdminDashboard")]
        public async Task<IActionResult> GetAdminDashboard()
        {
            var response = await _AdminDashboardService.GetAdminDashboard();
            return Ok(response);
        }

        [HttpGet]
        [Route("getFileListByFileType/{type}")]
        public async Task<IActionResult> GetFileListByFileType(int type)
        {
            var response = await _AdminDashboardService.GetFileListByFileType(type);
            return Ok(response);
        }

        [HttpPost]
        [Route("getFileListByTypeVisitedUser")]
        public async Task<IActionResult> GetFileListByTypeVisitedUser(RequestMessage rm)
        {
            QueryObject queryObject = JsonConvert.DeserializeObject<QueryObject>(rm.RequestObj.ToString());
            var response = await _AdminDashboardService.GetFileListByTypeVisitedUser(queryObject);
            return Ok(response);
        }

    }
}
