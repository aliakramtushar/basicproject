﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.Services.Services;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http.Description;


namespace DotNet.WebApi.Controllers.Common
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationFileUserMapController : ControllerBase
    {
        private readonly IApplicationFileUserMapService _ApplicationFileUserMapService;
        private readonly ILogger<ApplicationFileUserMapController> _logger;

        public ApplicationFileUserMapController(IApplicationFileUserMapService ApplicationFileUserMapService, ILogger<ApplicationFileUserMapController> logger)
        {
            _ApplicationFileUserMapService = ApplicationFileUserMapService;
            this._logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var response = await _ApplicationFileUserMapService.GetAll();
            return Ok(response);
        }

        [HttpGet]
        [Route("getByID/{id}")]
        public async Task<IActionResult> GetByID(int id)
        {
            var response = await _ApplicationFileUserMapService.GetByID(id);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestMessage rm)
        {
            var ApplicationFileUserMap = JsonConvert.DeserializeObject<ApplicationFileUserMap>(rm.RequestObj.ToString());
            var response = await _ApplicationFileUserMapService.Add(ApplicationFileUserMap);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put(RequestMessage rm)
        {
            var ApplicationFileUserMap = JsonConvert.DeserializeObject<ApplicationFileUserMap>(rm.RequestObj.ToString());
            var response = await _ApplicationFileUserMapService.Update(ApplicationFileUserMap);
            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _ApplicationFileUserMapService.Delete(id);
            return Ok(response);
        }
        [HttpGet]
        [Route("getAssingedUserListByMasterFileID/{fileID}")]
        public async Task<IActionResult> GetAssingedUserListByMasterFileID(int fileID)
        {
            var response = await _ApplicationFileUserMapService.GetAssingedUserListByMasterFileID(fileID);
            return Ok(response);
        }
    }
}
