﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.Services.Services.Interfaces.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Threading.Tasks;


namespace DotNet.WebApi.Controllers.Common
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InspectionMonitoringController : ControllerBase
    {
        private readonly IInspectionMonitoringService _inspectionMonitoringService;
        private readonly ILogger<InspectionMonitoringController> _logger;

        public InspectionMonitoringController(IInspectionMonitoringService inspectionMonitoringService, ILogger<InspectionMonitoringController> logger)
        {
            _inspectionMonitoringService = inspectionMonitoringService;
            this._logger = logger;
        }

        [HttpPost("search")]
        //[HttpGet]
        //[Route("search")]
        //public async Task<IActionResult> Search()
        public async Task<IActionResult> Search(RequestMessage rm)
        {
            QueryObject queryObject = JsonConvert.DeserializeObject<QueryObject>(rm.RequestObj.ToString());
            var response = await _inspectionMonitoringService.Search(queryObject);
            return Ok(response);
        }

        
        [HttpGet]
        [Route("getInitialData")]
        public async Task<IActionResult> GetInitialData()
        {
            var response = await _inspectionMonitoringService.GetInitialData();
            return Ok(response);
        }

    }
}
