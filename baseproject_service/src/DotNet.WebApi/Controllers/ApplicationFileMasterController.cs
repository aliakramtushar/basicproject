﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.Services.Services;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http.Description;


namespace DotNet.WebApi.Controllers.Common
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationFileMasterController : ControllerBase
    {
        private readonly IApplicationFileMasterService _ApplicationFileMasterService;
        private readonly ILogger<ApplicationFileMasterController> _logger;

        public ApplicationFileMasterController(IApplicationFileMasterService ApplicationFileMasterService, ILogger<ApplicationFileMasterController> logger)
        {
            _ApplicationFileMasterService = ApplicationFileMasterService;
            this._logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var response = await _ApplicationFileMasterService.GetAll();
            return Ok(response);
        }

        [HttpGet]
        [Route("getByID/{id}")]
        public async Task<IActionResult> GetByID(int id)
        {
            var response = await _ApplicationFileMasterService.GetByID(id);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestMessage rm)
        {
            var applicationFileMaster = JsonConvert.DeserializeObject<ApplicationFileMaster>(rm.RequestObj.ToString());
            var response = await _ApplicationFileMasterService.Add(applicationFileMaster);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put(RequestMessage rm)
        {
            var applicationFileMaster = JsonConvert.DeserializeObject<ApplicationFileMaster>(rm.RequestObj.ToString());
            var response = await _ApplicationFileMasterService.Update(applicationFileMaster);
            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _ApplicationFileMasterService.Delete(id);
            return Ok(response);
        }
        [HttpGet]
        [Route("getInitialData/{id}")]
        public async Task<IActionResult> GetInitialData(int id)
        {
            var response = await _ApplicationFileMasterService.GetInitialData(id);
            return Ok(response);
        }
        [HttpPost]
        [Route("migrateData")]
        public async Task<IActionResult> MigrateData(RequestMessage rm)
        {
            List<ApplicationMigration> lstData = JsonConvert.DeserializeObject<List<ApplicationMigration>>(rm.RequestObj.ToString());
            var response = await _ApplicationFileMasterService.MigrateData(lstData);
            return Ok(response);
        }
        [HttpPost]
        [Route("getListByAssingedPerson")]
        public async Task<IActionResult> GetListByAssingedPerson(RequestMessage rm)
        {
            QueryObject oQueryObject = JsonConvert.DeserializeObject<QueryObject>(rm.RequestObj.ToString());
            var response = await _ApplicationFileMasterService.GetListByAssingedPerson(oQueryObject);
            return Ok(response);
        }
        [HttpGet]
        [Route("getFileByIDWithInspectionDetails/{id}")]
        public async Task<IActionResult> GetFileByIDWithInspectionDetails(int id)
        {
            var response = await _ApplicationFileMasterService.GetFileByIDWithInspectionDetails(id);
            return Ok(response);
        }
        [HttpPut]
        [Route("updateApplicationFileGeofence")]
        public async Task<IActionResult> UpdateApplicationFileGeofence(RequestMessage rm)
        {
            QueryObject oQueryObject = JsonConvert.DeserializeObject<QueryObject>(rm.RequestObj.ToString());
            var response = await _ApplicationFileMasterService.UpdateApplicationFileGeofence(oQueryObject);
            return Ok(response);
        }

        [HttpPost]
        [Route("saveApplicationFileAttachments")]
        public async Task<IActionResult> SaveApplicationFileAttachments(IFormCollection formData)
        {
            var applicationFileMasterID = formData["applicationFileMasterID"];
            var attachmentType = formData["attachmentType"];

            var response = await _ApplicationFileMasterService.SaveApplicationFileAttachments(formData, Convert.ToInt32(applicationFileMasterID), (Convert.ToInt32(attachmentType)));
            return Ok(response);
        }
    }
}
