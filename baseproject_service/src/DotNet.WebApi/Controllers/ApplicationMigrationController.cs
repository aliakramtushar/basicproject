﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.Entities.CDA;
using DotNet.Services.Services;
using DotNet.Services.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http.Description;


namespace DotNet.WebApi.Controllers.Common
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationMigrationController : ControllerBase
    {
        private readonly IApplicationMigrationService _ApplicationMigrationService;
        private readonly ILogger<ApplicationMigrationController> _logger;

        public ApplicationMigrationController(IApplicationMigrationService ApplicationMigrationService, ILogger<ApplicationMigrationController> logger)
        {
            _ApplicationMigrationService = ApplicationMigrationService;
            this._logger = logger;
        }

        [HttpGet]
        [Route("getAll/{type}")]
        public async Task<IActionResult> GetAll(int type)
        {
            var response = await _ApplicationMigrationService.GetAll(type);
            return Ok(response);
        }

        [HttpGet]
        [Route("getByID/{id}")]
        public async Task<IActionResult> GetByID(int id)
        {
            var response = await _ApplicationMigrationService.GetByID(id);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestMessage rm)
        {
            var ApplicationMigration = JsonConvert.DeserializeObject<ApplicationMigration>(rm.RequestObj.ToString());
            var response = await _ApplicationMigrationService.Add(ApplicationMigration);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put(RequestMessage rm)
        {
            var ApplicationMigration = JsonConvert.DeserializeObject<ApplicationMigration>(rm.RequestObj.ToString());
            var response = await _ApplicationMigrationService.Update(ApplicationMigration);
            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _ApplicationMigrationService.Delete(id);
            return Ok(response);
        }
        [HttpGet]
        [Route("getInitialData/{id}")]
        public async Task<IActionResult> GetInitialData(int id)
        {
            var response = await _ApplicationMigrationService.GetInitialData(id);
            return Ok(response);
        }
        [HttpPost]
        [Route("excelUpload")]
        public async Task<IActionResult> ExcelUpload(RequestMessage requestMessage)
        {
            VMAttachments attachment = JsonConvert.DeserializeObject<VMAttachments>(requestMessage.RequestObj.ToString());
            var response = await _ApplicationMigrationService.ExcelUpload(attachment);
            return Ok(response);
        }
    }
}
