﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Services.Common;
using DotNet.Services.Services.Common.AdministrativeUnit;
using DotNet.Services.Services.Interfaces.Common.AdministrativeUnit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace DotNet.WebApi.Controllers.Common.AdministrativeUnit
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MouzaController : ControllerBase
    {
<<<<<<< HEAD
        private readonly IMouzaService _mouzaService;
        private readonly ILogger<MouzaController> _logger;

        public MouzaController(IMouzaService mouzaService, ILogger<MouzaController> logger)
        {
            _mouzaService = mouzaService;
=======
        private readonly IMouzaService _MouzaService;
        private readonly ILogger<MouzaController> _logger;

        public MouzaController(IMouzaService districtService, ILogger<MouzaController> logger)
        {
            _MouzaService = districtService;
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
<<<<<<< HEAD
            var response = await _mouzaService.GetAll();
=======
            var response = await _MouzaService.GetAll();
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            return Ok(response);
        }

        [HttpGet]
        [Route("getByID/{id}")]
        public async Task<IActionResult> GetByID(int id)
        {
<<<<<<< HEAD
            var response = await _mouzaService.GetByID(id);
=======
            var response = await _MouzaService.GetByID(id);
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestMessage rm)
        {
<<<<<<< HEAD
            var mouza = JsonConvert.DeserializeObject<VMMouza>(rm.RequestObj.ToString());
            var response = await _mouzaService.Add(mouza);
=======
            var district = JsonConvert.DeserializeObject<VMMouza>(rm.RequestObj.ToString());
            var response = await _MouzaService.Add(district);
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put(RequestMessage rm)
        {
<<<<<<< HEAD
            var mouza = JsonConvert.DeserializeObject<VMMouza>(rm.RequestObj.ToString());
            var response = await _mouzaService.Update(mouza);
=======
            var district = JsonConvert.DeserializeObject<VMMouza>(rm.RequestObj.ToString());
            var response = await _MouzaService.Update(district);
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
<<<<<<< HEAD
            var response = await _mouzaService.Delete(id);
=======
            var response = await _MouzaService.Delete(id);
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            return Ok(response);
        }

        [HttpPost("search")]
        public async Task<IActionResult> Search(RequestMessage rm)
        {
            QueryObject queryObject = JsonConvert.DeserializeObject<QueryObject>(rm.RequestObj.ToString());
<<<<<<< HEAD
            var response = await _mouzaService.Search(queryObject);
=======
            var response = await _MouzaService.Search(queryObject);
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
            return Ok(response);
        }

    }
}
