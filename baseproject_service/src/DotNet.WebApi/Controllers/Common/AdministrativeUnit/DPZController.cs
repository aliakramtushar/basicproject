﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Services.Common;
using DotNet.Services.Services.Common.AdministrativeUnit;
using DotNet.Services.Services.Interfaces.Common.AdministrativeUnit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace DotNet.WebApi.Controllers.Common.AdministrativeUnit
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DPZController : ControllerBase
    {
        private readonly IDPZService _DPZService;
        private readonly ILogger<DPZController> _logger;

        public DPZController(IDPZService districtService, ILogger<DPZController> logger)
        {
            _DPZService = districtService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var response = await _DPZService.GetAll();
            return Ok(response);
        }

        [HttpGet]
        [Route("getByID/{id}")]
        public async Task<IActionResult> GetByID(int id)
        {
            var response = await _DPZService.GetByID(id);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestMessage rm)
        {
            var district = JsonConvert.DeserializeObject<VMDPZ>(rm.RequestObj.ToString());
            var response = await _DPZService.Add(district);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put(RequestMessage rm)
        {
            var district = JsonConvert.DeserializeObject<VMDPZ>(rm.RequestObj.ToString());
            var response = await _DPZService.Update(district);
            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _DPZService.Delete(id);
            return Ok(response);
        }

        [HttpPost("search")]
        public async Task<IActionResult> Search(RequestMessage rm)
        {
            QueryObject queryObject = JsonConvert.DeserializeObject<QueryObject>(rm.RequestObj.ToString());
            var response = await _DPZService.Search(queryObject);
            return Ok(response);
        }

        //[HttpPost("userDpzMap")]
        //public async Task<IActionResult> UserDpzMap(RequestMessage rm)
        //{
        //    List<VMUserDPZMap> objList = JsonConvert.DeserializeObject<List<VMUserDPZMap>>(rm.RequestObj.ToString());
        //    var response = await _DPZService.UserDpzMap(objList);
        //    return Ok(response);
        //}
    }
}
