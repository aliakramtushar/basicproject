﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Services.Common;
using DotNet.Services.Services.Interfaces.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Description;


namespace DotNet.WebApi.Controllers.Common
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentService _DepartmentService;
        private readonly ILogger<DepartmentController> _logger;

        public DepartmentController(IDepartmentService userService, ILogger<DepartmentController> logger)
        {
            _DepartmentService = userService;
            this._logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var response = await _DepartmentService.GetAll();
            return Ok(response);
        }

        [HttpGet]
        [Route("getByID/{id}")]
        public async Task<IActionResult> GetByID(int id)
        {
            var response = await _DepartmentService.GetByID(id);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestMessage rm)
        {
            var Department = JsonConvert.DeserializeObject<Department>(rm.RequestObj.ToString());
            var response = await _DepartmentService.Add(Department);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put(RequestMessage rm)
        {
            var Department = JsonConvert.DeserializeObject<Department>(rm.RequestObj.ToString());
            var response = await _DepartmentService.Update(Department);
            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _DepartmentService.Delete(id);
            return Ok(response);
        }
        [HttpPut("updateOrder")]
        public async Task<IActionResult> UpdateOrder(RequestMessage rm)
        {
            var Departments = JsonConvert.DeserializeObject<List<Department>>(rm.RequestObj.ToString());
            var response = await _DepartmentService.UpdateOrder(Departments);
            return Ok(response);
        }
<<<<<<< HEAD
        [HttpGet]
        [Route("getListByOrganizationID/{id}")]
        public async Task<IActionResult> GetListByOrganizationID(int id)
        {
            var response = await _DepartmentService.GetListByOrganizationID(id);
            return Ok(response);
        }
=======
>>>>>>> 80c7ea2c6727b668e85781e93223e5620376a6aa
    }
}
