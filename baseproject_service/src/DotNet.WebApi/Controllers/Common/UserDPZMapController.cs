﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Services.Common;
using DotNet.Services.Services.Common.AdministrativeUnit;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.Services.Services.Interfaces.Common.AdministrativeUnit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace DotNet.WebApi.Controllers.Common
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserDPZMapController : ControllerBase
    {
        private readonly IUserDPZMapService _UserDPZMapService;
        private readonly ILogger<UserDPZMapController> _logger;

        public UserDPZMapController(IUserDPZMapService districtService, ILogger<UserDPZMapController> logger)
        {
            _UserDPZMapService = districtService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var response = await _UserDPZMapService.GetAll();
            return Ok(response);
        }

        [HttpGet]
        [Route("getByID/{id}")]
        public async Task<IActionResult> GetByID(int id)
        {
            var response = await _UserDPZMapService.GetByID(id);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestMessage rm)
        {
            var userDPZMap = JsonConvert.DeserializeObject<VMUserDPZMap>(rm.RequestObj.ToString());
            var response = await _UserDPZMapService.Add(userDPZMap);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put(RequestMessage rm)
        {
            var district = JsonConvert.DeserializeObject<VMUserDPZMap>(rm.RequestObj.ToString());
            var response = await _UserDPZMapService.Update(district);
            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await _UserDPZMapService.Delete(id);
            return Ok(response);
        }
    }
}
