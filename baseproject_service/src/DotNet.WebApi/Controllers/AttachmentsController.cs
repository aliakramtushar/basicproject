﻿using DotNet.ApplicationCore.DTOs;
using DotNet.ApplicationCore.DTOs.Common;
using DotNet.ApplicationCore.DTOs.VM;
using DotNet.ApplicationCore.DTOs.VM.AdministrativeUnit;
using DotNet.ApplicationCore.Entities;
using DotNet.Services.Services;
using DotNet.Services.Services.Common;
using DotNet.Services.Services.Common.AdministrativeUnit;
using DotNet.Services.Services.Interfaces;
using DotNet.Services.Services.Interfaces.Common;
using DotNet.Services.Services.Interfaces.Common.AdministrativeUnit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace DotNet.WebApi.Controllers.Common
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AttachmentsController : ControllerBase
    {
        private readonly IAttachmentsService _attachmentsService;
        private readonly ILogger<AttachmentsController> _logger;

        public AttachmentsController(IAttachmentsService districtService, ILogger<AttachmentsController> logger)
        {
            _attachmentsService = districtService;
            _logger = logger;
        }

        //[HttpGet]
        //[Route("getAttachmentListByFileID/{id}")]
        //public async Task<IActionResult> GetAttachmentListByFileID(int id)
        //{
        //    var response = await _attachmentsService.GetAttachmentListByFileID(id);
        //    return Ok(response);
        //}


        [HttpGet]
        [Route("getAttachmentListByFileID/{id}")]
        public async Task<IActionResult> GetAttachmentListByFileID(int id)
        {
            var response = await _attachmentsService.GetAttachmentListByFileID(id);
            return Ok(response);
        }
    }
}
