export class ApplicationFileUserMap {
    applicationFileUserMapID: number;
    applicationFileMasterID: number;
    userID: number;
    isActive: boolean;
    assignDate: string;
    targetDate: string;
    assignBy: number;
    userName: string;
    assignByName: string;
}