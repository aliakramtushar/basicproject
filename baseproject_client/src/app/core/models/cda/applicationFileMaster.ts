import { ApplicationType } from "../../enums/globalEnum";

export class ApplicationFileMaster {
    applicationFileMasterID: number;
    refNo: string;
    applicantName: string;
    approvalDate: string;
    applicationType: ApplicationType;
    rsNo: string;
    bsNo: string;
    thanaID: number;
    mouzaID: number;
    dPZID: number;
    road: string;
    isVisited: boolean;
    latitude: number | null;
    longitude: number | null;
    createdBy: number;
    createdDate: string;
    updatedBy: number;
    updatedDate: string;
    approvalDateSt: string;
    thanaName: string;
    thanaNameBangla: string;
    mouzaName: string;
    mouzaNameBangla: string;
    dPZName: string;
    dPZNameBangla: string;
    userID: number;
    userFullName: string;
    assignBy: number;
    assignByName: string;
    assignByNameBangla: string;
    targetDate: string;
    assignDate: string;
    applicationTypeSt: string;

    isChecked : boolean;
    isFileDetails: boolean = false;
}