import { ApplicationType } from "../../enums/globalEnum";

export class ApplicationMigration {
    applicationMigrationID: number;
    applicationFileMasterID : number;
    
    refNo: string;
    applicantName: string;
    approvalDate: string;
    applicationType: ApplicationType;
    rsNo: string;
    bsNo: string;
    thanaID: number;
    mouzaID: number;
    road: string;
    createdBy: number;
    createdDate: string;
    updatedBy: number;
    updatedDate: string;
    thanaName: string;
    mouzaName: string;
    applicationTypeID: number;
    applicationTypeName: string;
    approvalDateSt: string;
    applicationTypeSt: string;
    isChecked : boolean;
}