import { ApplicationType } from "../../enums/globalEnum";

export class AdminDashboard {
    adminDashboardFileTypeWiseList: AdminDashboardFileTypeWise[];
    adminDashboardFileUserWiseList: AdminDashboardFileUserWise[];
    adminDashboardFileListUserWise: AdminDashboardFileListUserWise[];
}

export class AdminDashboardFileTypeWise {
    applicationType: ApplicationType;
    totalFile: number;
    visited: number;
    applicationTypeName: string;
}

export class AdminDashboardFileUserWise {
    userID: number;
    userFullName: string;
    departmentID: number;
    departmentName: string;
    totalFile: number;
    visited: number;
    isFileDetails : boolean;
}
export class AdminDashboardFileListUserWise {
    refNo: string;
    applicantName: string;
    thanaID: number;
    thanaName: string;
    mouzaID: number;
    mouzaName: string;
    isVisited: boolean;
    assignDate: Date;
    targetDate: Date;
    VisitDate: Date;
}