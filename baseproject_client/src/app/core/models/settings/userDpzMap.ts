export class UserDPZMap {
    userDPZMapID: number;
    userID: number;
    dpzid: number;
    departmentID: number;
    isActive: boolean;
    fromDate: string;
    toDate: string;
    createdBy: number;
    createdDate: string;
    updatedBy: number;
    updatedDate: string;
    userName: string;
    dpzName: string;
    departmentName: string;
}