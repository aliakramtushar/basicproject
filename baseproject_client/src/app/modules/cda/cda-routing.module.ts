import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApplicationMigrationComponent } from './application-migration/application-migration.component';
import { ApplicationFileMasterComponent } from './application-file-master/application-file-master.component';
import { InspectionMonitoringComponent } from './inspection-monitoring/inspection-monitoring.component';

const routes: Routes = [
  { path: 'application-migration', component: ApplicationMigrationComponent },
  { path: 'application-file-master', component: ApplicationFileMasterComponent },
  { path: 'inspection-monitoring', component: InspectionMonitoringComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CdaRoutingModule { }
