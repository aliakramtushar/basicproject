import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ColDef, GridApi, GridOptions, GridReadyEvent, RowNode } from 'ag-grid-community';
import { Observable, throwError } from 'rxjs';
import { RoutingHelper } from 'src/app/core/helpers/routing-helper';
import { SweetAlertEnum, SweetAlertService } from 'src/app/core/helpers/sweet-alert.service';
import { ApplicationMigration } from 'src/app/core/models/cda/applicationMigration';
import { Attachment } from 'src/app/core/models/common/attachment';
import { PageModel } from 'src/app/core/models/core/pageModel';
import { ResponseMessage } from 'src/app/core/models/responseMessage';
import { Mouza } from 'src/app/core/models/settings/mouza';
import { Thana } from 'src/app/core/models/settings/thana';
import { ApplicationMigrationService } from 'src/app/core/services/cda/application-migration.service';
import { CheckboxRendererComponent } from '../../renderer/checkbox-renderer/checkbox-renderer.component';
import { ApplicationFileMasterService } from 'src/app/core/services/cda/application-file-master.service';
import { ApplicationType, ReturnStatus } from 'src/app/core/enums/globalEnum';

@Component({
  selector: 'app-application-migration',
  templateUrl: './application-migration.component.html',
  styleUrls: ['./application-migration.component.scss']
})
export class ApplicationMigrationComponent implements OnInit {


  @ViewChild("modalApplicationMigration") modalApplicationMigration: TemplateRef<any>;

  lstApplicationMigration: ApplicationMigration[] = new Array<ApplicationMigration>();
  selectedApplicationMigration: ApplicationMigration = new ApplicationMigration();
  public pageModel: PageModel = new PageModel();
  responsData: any;
  frameworkComponents: any;

  lstThana: Thana[] = new Array<Thana>();
  lstMouza: Mouza[] = new Array<Mouza>();
  lstMouza_all: Mouza[] = new Array<Mouza>();
  lstApplicationType: ApplicationType[] = new Array<ApplicationType>();

  private gridApi;
  private gridColumnApi;
  columnDefs = dataColumnDefs;
  gridOptions: GridOptions = {
    pagination: true,
    rowSelection: 'single',
    suppressDragLeaveHidesColumns: true,
    suppressRowDrag: false,
    rowDragManaged: true,
    getRowHeight: (params) => 40,
    defaultColDef: dataDefaultColDef,
  }
  objDataAttachment: Attachment;
  selectedFile: any;
  profileImagePath: any;
  fileURL: string | ArrayBuffer;

  constructor(
    private applicationMigrationService: ApplicationMigrationService,
    private applicationFileMasterService: ApplicationFileMasterService,
    private swal: SweetAlertService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.frameworkComponents = {
      checkboxRenderer: CheckboxRendererComponent
    };
    this.checkedHandler = this.checkedHandler.bind(this);
  }

  ngOnInit() {
    this.selectedApplicationMigration.applicationTypeID = 0;
    // this.selectedApplicationMigration.isActive = true;
    this.pageModel.type = 0;
    this.getInitialData();
    this.getAll();
  }


  private params: any;

  agInit(params: any): void {
    this.params = params;
  }

  checkedHandler(event) {
    let checked = event.target.checked;
    let colId = this.params.column.colId;
    this.params.node.setDataValue(colId, checked);
  }

  checkAll(dataList, gridOption, isChecked) {
    dataList.forEach(x => {
      // x.isChecked = !isChecked;
      // if (x.isChecked == false){
      x.isChecked = !isChecked;
      // }

    })
    gridOption.api.redrawRows();

  }

  getInitialData() {
    let orgID = parseInt(localStorage.getItem("ORGANIZATION_ID"));
    this.applicationMigrationService.getInitialData(orgID).subscribe(
      (res: ResponseMessage) => {
        if (res) {
          this.responsData = res.responseObj;
          this.lstThana = this.responsData.lstThana;
          this.lstMouza_all = this.responsData.lstMouza;
          this.lstApplicationType = this.responsData.lstApplicationType;
        }
      }
    )
  }
  getAll() {
    this.applicationMigrationService.getAll(this.pageModel.type).subscribe(
      (res) => {
        if (res) {
          let data: ApplicationMigration[] = new Array<ApplicationMigration>();
          data = Object.assign(data, res);
          this.lstApplicationMigration = [...data];
          this.gridOptions.api.redrawRows();
        }
      }
    )
  }



  modalClose() {
    this.modalService.dismissAll(this.modalApplicationMigration);
  }

  onEdit() {
    // if (await this.swal.confirm_custom('Are you sure?', SweetAlertEnum.question, true, false)) {


    if (this.selectedApplicationMigration.thanaID > 0) {
      this.onChangeThana();
    }
    this.modalService.open(this.modalApplicationMigration, { size: 'lg' });
    // }
  }

  async process() {
    debugger;
    let data = this.lstApplicationMigration.filter(x => x.isChecked == true);
    if (data && data.length > 0) {
      if (await this.swal.confirm_custom('Are you sure?', SweetAlertEnum.question, true, false)) {
        this.applicationFileMasterService.migrateData(data).subscribe(
          (res: ResponseMessage) => {
            if (res.statusCode == ReturnStatus.Success) {
              this.swal.message("Data Saved", SweetAlertEnum.success);
              this.pageModel.type = 0;
              this.getAll();
            }
            else {
              this.swal.message(res.message, SweetAlertEnum.error)
            }
          },
          (error) => {
            this.swal.message(error, SweetAlertEnum.error);
          })
      }
    }
    else {
      this.swal.message("No data selected", SweetAlertEnum.error);
    }

  }

  async update() {
    if (await this.swal.confirm_custom('Are you sure?', SweetAlertEnum.question, true, false)) {
      this.applicationMigrationService.update(this.selectedApplicationMigration).subscribe(
        (res: ApplicationMigration) => {
          if (res && res.applicationMigrationID > 0) {
            this.swal.message('Data Updated Successfully', SweetAlertEnum.success);
            this.modalClose();
            this.getAll();
          }
        },
        (error) => {
          this.swal.message(error, SweetAlertEnum.error);
        })
    }
  }

  async onDelete() {
    if (await this.swal.confirm_custom('Are you sure?', SweetAlertEnum.question, true, false)) {
      this.applicationMigrationService.delete(this.selectedApplicationMigration.applicationMigrationID).subscribe(
        (res: ApplicationMigration) => {
          if (res && res.applicationMigrationID > 0) {
            this.swal.message('Data Delete Successfully', SweetAlertEnum.success);
            this.getAll();
          }
        },
        (error) => {
          this.swal.message(error, SweetAlertEnum.error);
        })
    }
  }


  onChangeFile(event: any) {
    this.selectedFile = event.target.files;
    const reader = new FileReader();
    this.profileImagePath = this.selectedFile;
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.fileURL = reader.result;
    }
  }

  async uploadFile() {
    if (this.selectedApplicationMigration.applicationTypeID == 0) {
      this.swal.message('Please select file type', SweetAlertEnum.error);
      return;
    }
    if (await this.swal.confirm_custom('Are you sure?', SweetAlertEnum.question, true, false)) {
      this.objDataAttachment = new Attachment();
      this.objDataAttachment.AttachmentName = 'Application_Migration';
      this.objDataAttachment.FileContent = this.fileURL.toString();
      this.objDataAttachment.ReferenceID = 0;
      this.objDataAttachment.AttachementTypeID = this.selectedApplicationMigration.applicationTypeID;
      this.objDataAttachment.UpdatedBy = parseInt(localStorage.getItem('userAutoID'));
      this.applicationMigrationService.excelUpload(this.objDataAttachment).subscribe(
        (res: ResponseMessage) => {
          if (res.statusCode == 1) {
            this.getAll();
     
          }
          else {
            this.swal.message(res.message, SweetAlertEnum.error);
          }

        });
    }
  }


  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    let nodes = this.gridApi.getRenderedNodes();
    if (nodes.length) {
      nodes[0].setSelected(true); //selects the first row in the rendered view
    }
  }
  onSelect() {
    const selectedRows = this.gridApi.getSelectedRows();
    if (selectedRows && selectedRows.length == 1) {
      this.selectedApplicationMigration = selectedRows[0];
    }
    else {
      this.selectedApplicationMigration = new ApplicationMigration();
    }
  }
  onChangeColName(colDef: ColDef) {
    const columns = this.gridOptions.columnApi.getAllColumns();
    const valueColumn = columns.filter(column => column.getColDef().headerName === colDef.headerName)[0];
    const newState = !valueColumn.isVisible();
    this.gridOptions.columnApi.setColumnVisible(valueColumn, newState);
    this.gridOptions.api.sizeColumnsToFit();
  }
  onBtnExport() {
    this.gridApi.exportDataAsCsv();
  }
  onChangeThana() {
    this.lstMouza = [];
    if (this.selectedApplicationMigration.thanaID > 0) {
      this.lstMouza = this.lstMouza_all.filter(x => x.thanaID == this.selectedApplicationMigration.thanaID);
    }
  }
}

const dataDefaultColDef: ColDef = {
  // flex: 1,
  // width: 300,
  resizable: true,
  sortable: true,
  suppressMovable: false,
  filter: true,
  cellClass: 'suppress-movable-col',
  // floatingFilter: true,
};

const dataColumnDefs = [
  { isVisible: true, field: "isChecked", cellRenderer: "checkboxRenderer", width: 50, lockPosition: true, pinned: 'left', suppressMovable: true, },
  { isVisible: true, field: 'slNo', headerName: 'SL', lockPosition: true, pinned: 'left', suppressMovable: true, valueGetter: "node.rowIndex + 1", resizable: false, width: 80 },
  { isVisible: true, field: "refNo", headerName: 'Approval Number' },
  { isVisible: true, field: "applicantName", headerName: 'Applicant Name' },
  { isVisible: true, field: "approvalDateSt", headerName: 'Approval Date', width: 150 },
  { isVisible: true, field: "applicationTypeSt", headerName: 'Application Type' },
  { isVisible: true, field: "rsNo", headerName: 'RS No' },
  { isVisible: true, field: "bsNo", headerName: 'BS No' },
  { isVisible: true, field: "thanaName", headerName: 'Thana Name' },
  { isVisible: true, field: "mouzaName", headerName: 'Mouza Name' },
  { isVisible: true, field: "road", headerName: 'Road' },
  { isVisible: true, field: "isMigrated", headerName: 'is Migrated' },
];
