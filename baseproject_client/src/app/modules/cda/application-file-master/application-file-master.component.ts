import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ColDef, GridApi, GridOptions, GridReadyEvent, RowNode } from 'ag-grid-community';
import { Observable, throwError } from 'rxjs';
import { RoutingHelper } from 'src/app/core/helpers/routing-helper';
import { SweetAlertEnum, SweetAlertService } from 'src/app/core/helpers/sweet-alert.service';
import { Attachment } from 'src/app/core/models/common/attachment';
import { PageModel } from 'src/app/core/models/core/pageModel';
import { ResponseMessage } from 'src/app/core/models/responseMessage';
import { Mouza } from 'src/app/core/models/settings/mouza';
import { Thana } from 'src/app/core/models/settings/thana';
import { CheckboxRendererComponent } from '../../renderer/checkbox-renderer/checkbox-renderer.component';
import { ApplicationFileMasterService } from 'src/app/core/services/cda/application-file-master.service';
import { ApplicationFileMaster } from 'src/app/core/models/cda/applicationFileMaster';
import { Users } from 'src/app/core/models/settings/users';
import { ApplicationFileUserMap } from 'src/app/core/models/cda/applicationFileUserMap';
import { ApplicationFileUserMapService } from 'src/app/core/services/cda/application-file-user-map.service';
import { QueryObject } from 'src/app/core/models/core/queryObject';
import { ApplicationType, ReturnStatus } from 'src/app/core/enums/globalEnum';

@Component({
  selector: 'app-application-file-master',
  templateUrl: './application-file-master.component.html',
  styleUrls: ['./application-file-master.component.scss']
})
export class ApplicationFileMasterComponent implements OnInit {


  @ViewChild("modalApplicationFileMaster") modalApplicationFileMaster: TemplateRef<any>;
  @ViewChild("modalUserAssign") modalUserAssign: TemplateRef<any>;
  lstApplicationFileMaster: ApplicationFileMaster[] = new Array<ApplicationFileMaster>();
  selectedApplicationFileMaster: ApplicationFileMaster = new ApplicationFileMaster();
  selectedApplicationFileUserMap: ApplicationFileUserMap = new ApplicationFileUserMap();
  selectedUser: Users = new Users();
  public pageModel: PageModel = new PageModel();
  responsData: any;
  frameworkComponents: any;

  userImage: string;
  lstApplicationType = [];

  lstThana: Thana[] = new Array<Thana>();
  lstMouza: Mouza[] = new Array<Mouza>();
  lstMouza_all: Mouza[] = new Array<Mouza>();
  lstUser: Users[] = new Array<Users>();

  private gridApi;
  private gridColumnApi;
  columnDefs = dataColumnDefs;
  gridOptions: GridOptions = {
    pagination: true,
    rowSelection: 'single',
    suppressDragLeaveHidesColumns: true,
    suppressRowDrag: false,
    rowDragManaged: true,
    getRowHeight: (params) => 40,
    defaultColDef: dataDefaultColDef,
  }
  objDataAttachment: Attachment;
  selectedFile: any;
  profileImagePath: any;
  fileURL: string | ArrayBuffer;

  constructor(
    private applicationFileMasterService: ApplicationFileMasterService,
    private swal: SweetAlertService,
    private router: Router,
    private modalService: NgbModal,
    private applicationFileUserMapService: ApplicationFileUserMapService
  ) {
    this.frameworkComponents = {
      checkboxRenderer: CheckboxRendererComponent
    };
    this.checkedHandler = this.checkedHandler.bind(this);
  }

  ngOnInit() {
    this.selectedApplicationFileUserMap.userID = 0;
    // this.selectedApplicationFileMaster.isActive = true;
    this.getInitialData();
    this.getAll();
  }


  private params: any;

  agInit(params: any): void {
    this.params = params;
  }

  checkedHandler(event) {
    let checked = event.target.checked;
    let colId = this.params.column.colId;
    this.params.node.setDataValue(colId, checked);
  }

  checkAll(dataList, gridOption, isChecked) {
    dataList.forEach(x => {
      // x.isChecked = !isChecked;
      // if (x.isChecked == false){
      x.isChecked = !isChecked;
      // }

    })
    gridOption.api.redrawRows();

  }

  getInitialData() {
    let orgID = parseInt(localStorage.getItem("ORGANIZATION_ID"));
    this.applicationFileMasterService.getInitialData(orgID).subscribe(
      (res: ResponseMessage) => {
        if (res) {
          this.responsData = res.responseObj;
          this.lstThana = this.responsData.lstThana;
          this.lstMouza_all = this.responsData.lstMouza;
          this.lstUser = this.responsData.lstUser;
          this.lstApplicationType = this.responsData.lstApplicationType
        }
      }
    )
  }
  getAll() {
    this.applicationFileMasterService.getAll().subscribe(
      (res) => {
        if (res) {
          let data = Object.assign(this.lstApplicationFileMaster, res);
          this.lstApplicationFileMaster = [...data];
          this.gridOptions.api.redrawRows();
        }
      }
    )
  }
  userChange() {
    // if (this.selectedApplicationFileUserMap.applicationFileUserMapID > 0) {
    //   this.swal.message("Can not change user in this state", SweetAlertEnum.error);
    //   return;
    // }
    if (this.selectedApplicationFileUserMap.userID > 0) {
      let selectedUser = this.lstUser.find(x => x.userAutoID == this.selectedApplicationFileUserMap.userID);
      if (selectedUser.userAutoID > 0) {
        this.selectedApplicationFileUserMap.applicationFileUserMapID = 0;
        this.userImage = selectedUser.userImage;
        this.userImage = "data:image/png;base64," + this.userImage;
      }
    }
    else {
      this.selectedApplicationFileUserMap.userID = 0;
      this.userImage = '';
    }

  }
  userAssignModal() {
    if (this.selectedApplicationFileMaster == null || this.selectedApplicationFileMaster.applicationFileMasterID > 0) {
      this.selectedApplicationFileUserMap.applicationFileMasterID = this.selectedApplicationFileMaster.applicationFileMasterID;
      this.getAssingedUserListByFileID(this.selectedApplicationFileUserMap.applicationFileMasterID);
      this.modalService.open(this.modalUserAssign, { size: 'md' });
    }
    else {
      this.swal.message('Please select any file', SweetAlertEnum.info);
    }
  }
  getAssingedUserListByFileID(fileID: number) {
    this.userImage = "";
    this.selectedApplicationFileUserMap = new ApplicationFileUserMap();
    this.selectedApplicationFileUserMap.applicationFileMasterID = this.selectedApplicationFileMaster.applicationFileMasterID;
    this.applicationFileUserMapService.getAssingedUserListByMasterFileID(fileID).subscribe(
      (res: ResponseMessage) => {
        debugger;

        var data = res;
        if (data != null && data[0]) {
          this.selectedApplicationFileUserMap = data[0];
          this.userChange();
        }
      }
    )
  }

  async submitUserAssign() {
    if (await this.swal.confirm_custom('Are you sure', SweetAlertEnum.question, true, false)) {
      // if (this.selectedApplicationFileMaster == null || this.selectedApplicationFileMaster.applicationFileMasterID > 0) {
      if (this.selectedApplicationFileUserMap.applicationFileMasterID > 0 && this.selectedApplicationFileUserMap.userID > 0) {
        if (this.selectedApplicationFileUserMap.applicationFileUserMapID > 0) {
          this.applicationFileUserMapService.update(this.selectedApplicationFileUserMap).subscribe(
            (res: ApplicationFileUserMap) => {
              if (res && res.applicationFileMasterID > 0) {
                this.swal.message('Data update successful', SweetAlertEnum.success);
                this.modalCloseUserAssign();
              }
            },
            (error) => {
              this.swal.message(error, SweetAlertEnum.error);
            })
        }
        else {
          this.applicationFileUserMapService.save(this.selectedApplicationFileUserMap).subscribe(
            (res: ApplicationFileUserMap) => {
              if (res && res.applicationFileMasterID > 0) {
                this.swal.message('Data saved successful', SweetAlertEnum.success);
                this.modalCloseUserAssign();
              }
            },
            (error) => {
              this.swal.message(error, SweetAlertEnum.error);
            })
        }

      }
      else {
        this.swal.message('please select user & file', SweetAlertEnum.warning);
      }
    }
  }

  modalClose() {
    this.modalService.dismissAll(this.modalApplicationFileMaster);
  }
  modalCloseUserAssign() {
    this.modalService.dismissAll(this.modalUserAssign);
  }

  onEdit() {
    // if (await this.swal.confirm_custom('Are you sure?', SweetAlertEnum.question, true, false)) {
    this.modalService.open(this.modalApplicationFileMaster, { size: 'lg' });
    // }
  }

  async process(dataList) {
    let data = dataList.filter(x => x.isChecked);
    if (await this.swal.confirm_custom('Are you sure?', SweetAlertEnum.question, true, false)) {
      this.applicationFileMasterService.migrateData(data).subscribe(
        (res) => {

        },
        (error) => {
          this.swal.message(error, SweetAlertEnum.error);
        })
    }
  }     

  async update() {
    if (await this.swal.confirm_custom('Are you sure?', SweetAlertEnum.question, true, false)) {
      this.applicationFileMasterService.update(this.selectedApplicationFileMaster).subscribe(
        (res: ApplicationFileMaster) => {
          if (res && res.applicationFileMasterID > 0) {
            this.swal.message('Data Updated Successfully', SweetAlertEnum.success);
            this.modalClose();
            this.getAll();
          }
        },
        (error) => {
          this.swal.message(error, SweetAlertEnum.error);
        })
    }
  }

  async onDelete() {
    if (await this.swal.confirm_custom('Are you sure?', SweetAlertEnum.question, true, false)) {
      this.applicationFileMasterService.delete(this.selectedApplicationFileMaster.applicationFileMasterID).subscribe(
        (res: ApplicationFileMaster) => {
          if (res && res.applicationFileMasterID > 0) {
            this.swal.message('Data Delete Successfully', SweetAlertEnum.success);
            this.getAll();
          }
        },
        (error) => {
          this.swal.message(error, SweetAlertEnum.error);
        })
    }
  }


  onChangeFile(event: any) {
    this.selectedFile = event.target.files;
    const reader = new FileReader();
    this.profileImagePath = this.selectedFile;
    reader.readAsDataURL(this.selectedFile[0]);
    reader.onload = (_event) => {
      this.fileURL = reader.result;
    }
  }
  onChangeThana() {
    this.lstMouza = [];
    if (this.selectedApplicationFileMaster.thanaID > 0) {
      this.lstMouza = this.lstMouza_all.filter(x => x.thanaID == this.selectedApplicationFileMaster.thanaID);
    }
  }


  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    let nodes = this.gridApi.getRenderedNodes();
    if (nodes.length) {
      nodes[0].setSelected(true); //selects the first row in the rendered view
    }
  }
  onSelect() {
    const selectedRows = this.gridApi.getSelectedRows();
    if (selectedRows && selectedRows.length == 1) {
      this.selectedApplicationFileMaster = selectedRows[0];
    }
    else {
      this.selectedApplicationFileMaster = new ApplicationFileMaster();
    }
  }
  onChangeColName(colDef: ColDef) {
    const columns = this.gridOptions.columnApi.getAllColumns();
    const valueColumn = columns.filter(column => column.getColDef().headerName === colDef.headerName)[0];
    const newState = !valueColumn.isVisible();
    this.gridOptions.columnApi.setColumnVisible(valueColumn, newState);
    this.gridOptions.api.sizeColumnsToFit();
  }
  onBtnExport() {
    this.gridApi.exportDataAsCsv();
  }
}

const dataDefaultColDef: ColDef = {
  // flex: 1,
  // width: 300,
  resizable: true,
  sortable: true,
  suppressMovable: false,
  filter: true,
  cellClass: 'suppress-movable-col',
  // floatingFilter: true,
};

const dataColumnDefs = [
  // { isVisible: true, field: "isChecked", cellRenderer: "checkboxRenderer", width: 50, lockPosition: true, pinned: 'left', suppressMovable: true, },
  { isVisible: true, field: 'slNo', headerName: 'SL', lockPosition: true, pinned: 'left', suppressMovable: true, valueGetter: "node.rowIndex + 1", resizable: false, width: 80 },
  { isVisible: true, field: "refNo", headerName: 'Approval Number', pinned: 'left' },
  { isVisible: true, field: "applicantName", headerName: 'Applicant Name', pinned: 'left' },
  { isVisible: true, field: "approvalDateSt", headerName: 'Approval Date', width: 150 },
  { isVisible: true, field: "applicationTypeSt", headerName: 'Application Type' },
  { isVisible: true, field: "rsNo", headerName: 'RS No' },
  { isVisible: true, field: "bsNo", headerName: 'BS No' },
  { isVisible: true, field: "thanaName", headerName: 'Thana Name' },
  { isVisible: true, field: "mouzaName", headerName: 'Mouza Name' },
  { isVisible: true, field: "road", headerName: 'Road' },
  { isVisible: true, field: "userFullName", headerName: 'Office Name' },
];
