import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { CdaRoutingModule } from './cda-routing.module';
import { ApplicationMigrationComponent } from './application-migration/application-migration.component';
import { AgGridModule } from 'ag-grid-angular';
import { UIModule } from "../../shared/ui/ui.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbCollapseModule, NgbDropdownModule, NgbModalModule, NgbNavModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';
import { HttpClientModule } from '@angular/common/http';
import { FullCalendarModule } from '@fullcalendar/angular';
import { SimplebarAngularModule } from 'simplebar-angular';
import { LightboxModule } from 'ngx-lightbox';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ApplicationMigrationService } from 'src/app/core/services/cda/application-migration.service';
import { ApplicationFileMasterComponent } from './application-file-master/application-file-master.component';
import { ApplicationFileMasterService } from 'src/app/core/services/cda/application-file-master.service';
import { ApplicationFileUserMapService } from 'src/app/core/services/cda/application-file-user-map.service';
import { InspectionMonitoringComponent } from './inspection-monitoring/inspection-monitoring.component';
import { AgmCoreModule } from '@agm/core';
import { environment } from 'src/environments/environment';
import { AdminDashboardService } from 'src/app/core/services/cda/admin-dashboard.service';
import { InspectionMonitoringService } from 'src/app/core/services/cda/inspection-monitoring.service';
import { AttachmentsService } from 'src/app/core/services/cda/attachments.service';


@NgModule({
  declarations: [
    ApplicationMigrationComponent,
    ApplicationFileMasterComponent,
    InspectionMonitoringComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    NgbDropdownModule,
    NgbModalModule,
    NgApexchartsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FullCalendarModule,
    NgbNavModule,
    NgbTooltipModule,
    NgbCollapseModule,
    SimplebarAngularModule,
    LightboxModule,
    UIModule,
    Ng2SmartTableModule,
    AgGridModule,
    CdaRoutingModule,
    AgmCoreModule.forRoot({
      // apiKey: 'AIzaSyBiols4lFvOc7_rGeOZVI6l-YE617w7xR0',
      apiKey: environment.MAP_API_KEY,
      libraries: ['places', 'drawing', 'geometry']
    }),
  ],
  providers: [
    ApplicationMigrationService,
    ApplicationFileMasterService,
    ApplicationFileUserMapService,
    AdminDashboardService,
    InspectionMonitoringService,
    AttachmentsService,
    DatePipe
  ]
})
export class CdaModule { }
