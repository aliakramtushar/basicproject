import { MapsAPILoader } from '@agm/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, PLATFORM_ID, ViewChild } from '@angular/core';
import { ApplicationType } from 'src/app/core/enums/globalEnum';
import { ApplicationFileMaster } from 'src/app/core/models/cda/applicationFileMaster';
import { QueryObject } from 'src/app/core/models/core/queryObject';
import { Organization } from 'src/app/core/models/data/organization';
import { ResponseMessage } from 'src/app/core/models/responseMessage';
import { Department } from 'src/app/core/models/settings/department';
import { DPZ } from 'src/app/core/models/settings/dpz';
import { Mouza } from 'src/app/core/models/settings/mouza';
import { Thana } from 'src/app/core/models/settings/thana';
import { Users } from 'src/app/core/models/settings/users';
import { AttachmentsService } from 'src/app/core/services/cda/attachments.service';
import { InspectionMonitoringService } from 'src/app/core/services/cda/inspection-monitoring.service';



declare const google: any;
declare var zoomifyc: any;
declare var $: any;


@Component({
  selector: 'app-inspection-monitoring',
  templateUrl: './inspection-monitoring.component.html',
  styleUrls: ['./inspection-monitoring.component.scss'],
  animations: [
    trigger('agmInfoSlide', [
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate(250, style({ transform: 'translateX(0)' }))
      ]),
      transition(':leave', [
        animate(250, style({ transform: 'translateX(-100%)' }))
      ])
    ])
  ]

})


export class InspectionMonitoringComponent implements OnInit {

  pickup_latitude: number;
  pickup_longitude: number;
  showMarker = false;

  mapTypeControlOptions = false;

  queryObject: QueryObject = new QueryObject();
  fileOrRefNo: string;
  lstDepartment: Department[] = new Array<Department>();
  lstApplicationType: ApplicationType[] = new Array<ApplicationType>();
  lstDPZ: DPZ[] = new Array<DPZ>();
  lstThana: Thana[] = new Array<Thana>();
  lstMouza: Mouza[] = new Array<Mouza>();
  lstUser: Users[] = new Array<Users>();
  lstApplicationFileMaster: ApplicationFileMaster[] = new Array<ApplicationFileMaster>();

  lstAttachmentAllType: any;
  lstAttachmentSingleType: any;
  responsData: any;
  lstMonitoringHistory: any;

  agmInfoSlideToggler: boolean = false;



  changePickupMarkerLocation($event) {

    // this.inputVar = $event.coords.lat;
    // this.inputVar2 = $event.coords.lng;

    this.pickup_latitude = $event.coords.lat;
    this.pickup_longitude = $event.coords.lng;

    if (this.showMarker == false) {
      this.showMarker = true;
    } else {
      this.showMarker = false;
    }

  }
  zoomPosition: any;
  onMapReady(mapInstance) {

    let trafficLayer = new google.maps.TrafficLayer();
    trafficLayer.setMap(mapInstance);
    mapInstance.setOptions({
      mapTypeControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.TOP_RIGHT
      },
      fullscreenControlOptions: {
        //style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.TOP_RIGHT
      },
      //streetViewControl:'false',
      streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      },
      //zoomControl: 'false',
      zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      }
    });

  }

  mapType = 'roadmap';
  annotation: any;
  lat: number = 23.8750074;
  lng: number = 90.3382872;

  plat: number = 23.785064;
  plng: number = 90.397394;

  zoom: number = 9;
  // streetViewControl: true;
  // streetViewControlOptions: {
  //   position: google.maps.ControlPosition.TOP_RIGHT
  // }

  color: string = 'navyblue';
  opacity: number = 0.0;
  poSearch: any;
  strokeColor: string = 'red';
  strokeOpacity: number = 0.6;

  @ViewChild('streetviewMap', { static: true }) streetviewMap: any;
  @ViewChild('streetviewPano', { static: true }) streetviewPano: any;

  // bread crumb items
  constructor(
    private datePipe: DatePipe,
    private attachmentsService: AttachmentsService,
    private inspectionMonitoringService: InspectionMonitoringService
  ) { }


  ngOnInit(): void {
    // debugger
    // this.lstMonitoringHistory = [
    //   {
    //     "POID": 137941,
    //     "Latitude": 23.7850726,
    //     "Longitude": 90.3979967,
    //     "tLatitude": 24.898176,
    //     "tLongitude": 90.888219,

    //   },
    //   {
    //     "POID": 137942,
    //     "Latitude": 23.7850156,
    //     "Longitude": 90.3979767,
    //     "tLatitude": 24.898176,
    //     "tLongitude": 90.888219,

    //   },
    // ]

    this.getInitialData();
    this.initialSetValue();
  }

  initialSetValue() {
    this.queryObject.fromDate = this.datePipe.transform(new Date(), "yyyy-MM-dd");
    this.queryObject.toDate = this.datePipe.transform(new Date(), "yyyy-MM-dd");
    this.queryObject.departmentID = 0;
    this.queryObject.organizationID = 0;
    this.queryObject.applicationType = 0;
  }

  getInitialData() {
    this.inspectionMonitoringService.getInitialData().subscribe(
      (res: ResponseMessage) => {
        if (res) {
          this.lstDepartment = res.responseObj.lstDepartments;
          this.lstApplicationType = res.responseObj.lstApplicationTypes;
          this.lstDPZ = res.responseObj.lstDPZs;
          this.lstThana = res.responseObj.lstThanas;
          this.lstMouza = res.responseObj.lstMouzas;
          this.lstUser = res.responseObj.lstUsers;
        }
      })
  }

  search() {
    this.queryObject.referenceNo = "";
    this.inspectionMonitoringService.search(this.queryObject).subscribe(
      (res: ResponseMessage) => {
        if (res) {
          this.responsData = res;
          this.lstMonitoringHistory = this.responsData;
        }
      })
  }

  //==============Search by Only File File /Ref no 

  searchByFileNo() {
    if (this.queryObject.referenceNo.length > 5 && this.queryObject.referenceNo) {
      this.fileOrRefNo = this.queryObject.referenceNo;
      this.queryObject = new QueryObject();
      this.queryObject.referenceNo = this.fileOrRefNo;
      this.initialSetValue();
      this.inspectionMonitoringService.search(this.queryObject).subscribe(
        (res: ResponseMessage) => {
          if (res) {
            this.responsData = res;
            this.lstMonitoringHistory = this.responsData;


          }
        })
    }
  }


  markerClicked(fileMasterID: number) {
    if (fileMasterID) {
      this.attachmentsService.getAttachmentListByFileID(fileMasterID).subscribe(
        (res: ResponseMessage) => {
          if (res) {
            this.lstAttachmentAllType = res.responseObj;
          }
        })
    }

  }

  attachmentTabLink(id: number) {
    if (id) {
      this.lstAttachmentSingleType = this.lstAttachmentAllType.filter(x => x.attachementTypeID == id);
    }
  }

}
