import { Component, OnInit, ViewChild } from '@angular/core';
import { emailSentBarChart, monthlyEarningChart } from './data';
import { ChartType } from './dashboard.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfigService } from 'src/app/core/services/config.service';
import { EventService } from 'src/app/core/services/event.service';
import { AdminDashboardService } from 'src/app/core/services/cda/admin-dashboard.service';
import { ResponseMessage } from 'src/app/core/models/responseMessage';
import { ApplicationType, ReturnStatus } from 'src/app/core/enums/globalEnum';
import { SweetAlertEnum, SweetAlertService } from 'src/app/core/helpers/sweet-alert.service';
import { AdminDashboard, AdminDashboardFileListUserWise, AdminDashboardFileUserWise } from 'src/app/core/models/cda/adminDashboard';
import { QueryObject } from 'src/app/core/models/core/queryObject';

declare var $;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  private swal: SweetAlertService
  adminDashboard: AdminDashboard = new AdminDashboard()

  isVisible: string;

  uiStatusChange: number = 0;
  fileDetailsListToggle: boolean;

  transactions: Array<[]>;
  statData: Array<[]>;

  isActive: string;

  // @ViewChild('content') content;
  constructor(private modalService: NgbModal, private configService: ConfigService, private eventService: EventService, private adminDashboardService: AdminDashboardService) {
  }

  ngOnInit() {
    this.getAdminDashboard();
    this.loadData(0);
  }

  getAdminDashboard() {
    this.adminDashboardService.getAdminDashboard().subscribe(
      (res: AdminDashboard) => {
        if (res) {
          let resData = res;
          if (resData.adminDashboardFileTypeWiseList) {
            this.adminDashboard.adminDashboardFileTypeWiseList = resData.adminDashboardFileTypeWiseList;
            // if (this.adminDashboard.adminDashboardFileTypeWiseList != null && this.adminDashboard.adminDashboardFileTypeWiseList.length > 0) {
            //   this.loadData(this.adminDashboard.adminDashboardFileTypeWiseList[0].applicationType);
            // }
          }
        }
      },
      (error) => {
        this.swal.message(error, SweetAlertEnum.error);
      })
  }
  selectedApplicationType: ApplicationType;
  loadData(id: number) {
    debugger;
    this.selectedApplicationType = id;
    this.uiStatusChange = id;
    this.adminDashboardService.getFileListByFileType(id).subscribe(
      (res: AdminDashboardFileUserWise[]) => {
        if (res) {
          let resData = res;
          if (resData) {
            this.adminDashboard.adminDashboardFileUserWiseList = resData;
          }
        }
      },
      (error) => {
        this.swal.message(error, SweetAlertEnum.error);
      })
  }

  getListByUser(item: AdminDashboardFileUserWise, isVisited: boolean) {

    this.adminDashboard.adminDashboardFileListUserWise = [];
    if (isVisited == true && item.visited > 0) {
      item.isFileDetails = !item.isFileDetails;
    }
    if (isVisited == false && (item.totalFile - item.visited) > 0) {
      item.isFileDetails = !item.isFileDetails;
    }

    this.fileDetailsListToggle = isVisited;

    let queryObject = new QueryObject();
    queryObject.isVisited = isVisited;
    queryObject.applicationType = this.selectedApplicationType;
    queryObject.userID = item.userID;
    debugger;

    this.adminDashboardService.getFileListByTypeVisitedUser(queryObject).subscribe(
      (res: AdminDashboardFileListUserWise[]) => {
        if (res) {
          let resData = res;
          if (resData) {
            this.adminDashboard.adminDashboardFileListUserWise = resData;
          }
        }

      },
      (error) => {
        this.swal.message(error, SweetAlertEnum.error);
      })
  }



}
